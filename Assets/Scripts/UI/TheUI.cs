﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class TheUI : MonoBehaviour
    {
        public Canvas game_canvas;
        public Canvas top_canvas;

        public Text game_code;
        public Text observing;
        public Text help_txt;

        public UIPanel assign_filter;

        [Header("Menu")]
        public UIPanel menu_panel;

        private float selector_timer = 0f;

        private static TheUI _instance;

        void Awake()
        {
            _instance = this;
            observing.gameObject.SetActive(false);
            game_canvas.worldCamera = Camera.main;
            top_canvas.worldCamera = Camera.main;
        }

        private void Start()
        {
            game_code.text = GameClient.game_uid;
            game_code.enabled = GameClient.online_mode;
            help_txt.text = "";
            GameClient.Get().onGameStart += OnGameStart;
            GameClient.Get().onNewTurn += OnNextTurn;
            LoadPanel.Get().Show(true);
            BlackPanel.Get().Show(true);
            BlackPanel.Get().Hide();

        }

        void Update()
        {
            Game data = GameClient.Get().GetGameData();
            if (data == null)
                return;

			bool is_connecting = data.current_state == GameState.Connecting;
            bool connection_lost = !is_connecting && !GameClient.Get().IsConnected();
            ConnectionPanel.Get().SetVisible(connection_lost);

            //Menu
            if (Input.GetKeyDown(KeyCode.Escape))
                menu_panel.Toggle();

            if (!GameClient.Get().IsConnected())
                return;

            bool yourturn = GameClient.Get().IsYourTurn();
            int player_id = GameClient.Get().GetPlayerID();
            Player player = data.GetPlayer(player_id);

            LoadPanel.Get().SetVisible(is_connecting && !data.AreAllPlayersConnected());
            observing.gameObject.SetActive(GameClient.Get().IsObserveMode());

            HideHelperText();

            bool show_msg = data.current_state == GameState.Selector && data.selector == SelectorType.SelectCardTarget && data.selector_player == player_id;
            CardTargetUI.Get().SetVisible(show_msg);
            if (show_msg)
            {
                Card caster = data.GetCard(data.selector_caster_uid);
                AbilityData iability = AbilityData.Get(data.selector_ability_id);

                CardData icaster = caster != null ? CardData.Get(caster.card_id) : null;
                string msg = "Select a target";
                Sprite img = icaster != null ? icaster.GetFullImage(caster.variant) : null;
                CardTargetUI.Get().ShowMsg(msg, img);
            }

            bool show_slot = data.current_state == GameState.Selector && data.selector == SelectorType.SelectSlot && data.selector_player == player_id;
            SlotTargetUI.Get().SetVisible(show_slot);

            if (!CardSelector.Get().IsVisible())
                selector_timer += Time.deltaTime;

            bool show_selector = data.current_state == GameState.Selector && data.selector == SelectorType.SelectCardSelector && data.selector_player == player_id && selector_timer > 2f;
            if (show_selector && !CardSelector.Get().IsVisible())
            {
                AbilityData iability = AbilityData.Get(data.selector_ability_id);
                Player oplayer = data.GetOpponentPlayer(player_id);

                List<Card> card_list = player.cards_discard;
                if (data.selector_source == AbilityTarget.SelectInHand)
                    card_list = player.cards_hand;
                if (data.selector_source == AbilityTarget.SelectInOpponentHand)
                    card_list = oplayer.cards_hand;
                if (data.selector_source == AbilityTarget.SelectInDeck)
                    card_list = player.cards_deck;

                bool show_all = false;
                selector_timer = 0f;

                if (iability != null)
                    CardSelector.Get().Show(card_list, iability, show_all);
            }

            //Choice selector
            bool show_choice_selector = data.current_state == GameState.Selector && data.selector == SelectorType.SelectChoice && data.selector_player == player_id;
            if (show_choice_selector && !ChoiceSelector.Get().IsVisible() && selector_timer > 2f)
            {
                AbilityData iability = AbilityData.Get(data.selector_ability_id);
                ChoiceSelector.Get().Show(iability);
                selector_timer = 0f;
            }

            //Hide
            if (!yourturn && CardSelector.Get().IsVisible())
                CardSelector.Get().Hide();
            if (!yourturn && ChoiceSelector.Get().IsVisible())
                ChoiceSelector.Get().Hide();

        }

        private void OnGameStart()
        {
            Game data = GameClient.Get().GetGameData();
            int player_id = GameClient.Get().GetPlayerID();
            Player player = data.GetPlayer(player_id);
        }

        private void OnNextTurn(int player_id)
        {
            CardSelector.Get().Hide();
            CardTargetUI.Get().Hide();
        }

        public void ShowHelperText(string text)
        {
            help_txt.text = text;
        }

        public void HideHelperText()
        {
            help_txt.text = "";
        }

        public void OnClickDeck()
        {
            //GameClient.Get().DrawCard();
        }

        public void OnClickRestart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void OnClickMenu()
        {
            menu_panel.Show();
        }

        public void OnClickBack()
        {
            menu_panel.Hide();
        }

        public void OnClickQuit()
        {
            if (TheGame.settings.play_mode == PlayMode.Adventure)
            {
                StartCoroutine(QuitRoutine("Adventure"));
            }
            else
            {
                GameClient.Get().Resign();
                StartCoroutine(QuitRoutine("Menu"));
            }
        }

        private IEnumerator QuitRoutine(string scene)
        {
            BlackPanel.Get().Show();
            AudioSystem.Get().FadeOutMusic("music");
            AudioSystem.Get().FadeOutSFX("ambience");
            AudioSystem.Get().FadeOutSFX("ending_sfx");

            yield return new WaitForSeconds(1f);
            SceneNav.GoTo(scene);
        }

        public void OnClickSwapObserve()
        {
            int other = GameClient.Get().GetPlayerID() == 0 ? 1 : 0;
            GameClient.Get().SetObserveMode(other);
        }

        public static bool IsUIOpened()
        {
            return CardSelector.Get().IsVisible() || EndGamePanel.Get().IsVisible();
        }

        public static bool IsOverUI()
        {
            //return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public static bool IsOverUILayer(int layer)
        {
            //return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            int count = 0;
            foreach (RaycastResult result in results)
            {
                if (result.sortingLayer == layer)
                    count++;
            }
            return count > 0;
        }

        public static Vector2 ScreenToRectPos(Canvas canvas, RectTransform rect, Vector2 screen_pos)
        {
            if (canvas.renderMode != RenderMode.ScreenSpaceOverlay && canvas.worldCamera != null)
            {
                Vector2 anchor_pos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screen_pos, canvas.worldCamera, out anchor_pos);
                return anchor_pos;
            }
            else
            {
                Vector2 anchor_pos = screen_pos - new Vector2(rect.position.x, rect.position.y);
                anchor_pos = new Vector2(anchor_pos.x / rect.lossyScale.x, anchor_pos.y / rect.lossyScale.y);
                return anchor_pos;
            }
        }

        public static string FormatCredits(int value)
        {
            return string.Format("{0:#,0}", value);
        }

        public static TheUI Get()
        {
            return _instance;
        }
    }
}
