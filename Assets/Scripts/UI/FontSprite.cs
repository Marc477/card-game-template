﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGameTemplate.UI
{
    public class FontSprite : MonoBehaviour
    {
        public int value;
        public FontSpriteData font;
        public TextAlignment align = TextAlignment.Center;
        public bool auto_refresh = true;

        //public bool show_sign;
        //public bool align_center;
        public Image[] images;

        private Vector2[] start_pos;
        private RectTransform rect;
        private FontSpriteData current_sprite;
        private float spacing = 1f;
        private int prev_value = -1;

        void Awake()
        {
            rect = GetComponent<RectTransform>();
            start_pos = new Vector2[images.Length];

            for (int i = 0; i < images.Length; i++)
                start_pos[i] = images[i].rectTransform.anchoredPosition;

            if (images.Length >= 2)
            {
                spacing = (images[0].GetComponent<RectTransform>().anchoredPosition - images[1].GetComponent<RectTransform>().anchoredPosition).magnitude;
            }
        }

        void Update()
        {
            if (auto_refresh)
            {
                if (prev_value != value)
                    Refresh();
            }
        }

        public void Refresh()
        {
            string val = value.ToString();
            prev_value = value;

            foreach (Image img in images)
                img.enabled = false;

            //bool signed = false; bool iconed = false;
            if (align == TextAlignment.Right || align == TextAlignment.Center)
            {
                int img_index = images.Length - 1;
                for (int i = 0; i < val.Length; i++)
                {
                    if (img_index >= 0 && img_index < images.Length)
                    {
                        int val_index = val.Length - 1 - i;
                        string number = val.Substring(val_index, 1);
                        SetIndexValue(img_index, number);
                    }
                    img_index--;
                }
            }
            if (align == TextAlignment.Left)
            {
                int img_index = 0;
                for (int i = 0; i < val.Length; i++)
                {
                    if (img_index >= 0 && img_index < images.Length)
                    {
                        int val_index = i;
                        string number = val.Substring(val_index, 1);
                        SetIndexValue(img_index, number);
                    }
                    img_index++;
                }
            }

            if (align == TextAlignment.Center && images.Length >= 2)
            {
                int val_length = val.Length;
                int nb_offset = Mathf.Max(images.Length - val_length, 0);

                for (int i = 0; i < images.Length; i++)
                    images[i].rectTransform.anchoredPosition = start_pos[i] + Vector2.left * nb_offset * spacing * 0.5f;
            }
        }

        private void SetIndexValue(int img_index, string value)
        {
            images[img_index].sprite = font.GetSprite(value);
            images[img_index].enabled = images[img_index].sprite != null;

            /*if (show_sign && !signed && !number_img[img_index].enabled && value != 0)
            {
                number_img[img_index].sprite = value > 0 ? font.plus : font.minus;
                number_img[img_index].enabled = number_img[img_index].sprite != null;
                signed = true;
            }*/
        }

        public void SetOpacity(float opacity)
        {
            foreach (Image img in images)
                img.color = new Color(img.color.r, img.color.g, img.color.b, opacity);
        }

        public void SetMat(Material mat)
        {
            foreach (Image img in images)
                img.material = mat;
        }

    }
}
