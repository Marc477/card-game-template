﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class PlayerUI : MonoBehaviour
    {
        public bool is_opponent;
        public Text pname;
        public Text plevel;
        public AvatarIcon avatar;
        public CanvasGroup chat_field_area;
        public InputField chat_field;
        public AudioClip chat_audio;
        public ChatBubble[] chat_bubbles;

        private UserData player_data = null;
        private bool loading_player = false;

        private Queue<string> chat_msg = new Queue<string>();
        private float chat_timer = 0f;

        private static List<PlayerUI> ui_list = new List<PlayerUI>();

        private void Awake()
        {
            ui_list.Add(this);
        }

        private void OnDestroy()
        {
            ui_list.Remove(this);
        }

        void Start()
        {
            pname.text = "";
            plevel.text = "";

            if (chat_field_area != null)
                chat_field_area.alpha = 0;

            GameClient.Get().onChatMsg += OnChat;
            RefreshChat();
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            int player_id = is_opponent ? GameClient.Get().GetOpponentPlayerID() : GameClient.Get().GetPlayerID();
            Game data = GameClient.Get().GetGameData();
            Player player = data.GetPlayer(player_id);

            if (player != null)
            {

                pname.text = player.username;

                if (!loading_player && player_data == null && !player.is_ai)
                    LoadPlayerData(player);

                if (player_data != null)
                {
                    plevel.text = "Lvl" + " " + GameplayData.Get().GetPlayerLevel(player_data.xp);
                    if (avatar != null)
                        avatar.SetAvatar(AvatarData.Get(player_data.avatar));
                }

                if (player.is_ai)
                {
                    DeckData deck = DeckData.Get(player.deck);
                    if (deck != null && avatar != null)
                        avatar.SetAvatar(deck.avatar);
                    plevel.text = player.ai_level.ToString();
                }

                //Chat
                if (chat_field_area != null && !is_opponent && GameClient.online_mode && Input.GetKeyDown(KeyCode.Return))
                {
                    if (chat_field_area.alpha > 0.5f)
                    {
                        if (!string.IsNullOrWhiteSpace(chat_field.text))
                            GameClient.Get().SendChatMsg(chat_field.text);
                        chat_field.text = "";
                        chat_field_area.alpha = 0f;
                        GUI.FocusControl(null);
                    }
                    else
                    {
                        chat_field_area.alpha = 1f;
                    }

                    chat_field.ActivateInputField();
                    chat_field.Select();
                }

                //Chat remove
                chat_timer += Time.deltaTime;
                if (chat_timer > 5f)
                    chat_msg.Clear();
            }
        }

        private void RefreshChat()
        {
            foreach (ChatBubble bubble in chat_bubbles)
                bubble.Hide();

            int index = 0;
            foreach (string msg in chat_msg)
            {
                if (index < chat_bubbles.Length)
                {
                    ChatBubble line = chat_bubbles[index];
                    line.SetLine(msg, 5f);
                    index++;
                }
            }
        }

        private void LoadPlayerData(Player player)
        {
            if (!ApiManager.Get().IsLoggedIn())
                return;

            loading_player = true;
            ApiManager.Get().SendGetUserData(player.username, (bool success, UserData udata) =>
            {
                loading_player = false;
                if (success)
                {
                    player_data = udata;
                }
            });
        }

        private void OnChat(int chat_player_id, string msg)
        {
            int player_id = is_opponent ? GameClient.Get().GetOpponentPlayerID() : GameClient.Get().GetPlayerID();
            if (player_id == chat_player_id)
            {
                chat_msg.Enqueue(msg);
                chat_timer = 0f;
                if (chat_msg.Count > chat_bubbles.Length)
                    chat_msg.Dequeue();
                AudioSystem.Get().PlaySFX("chat", chat_audio);
                RefreshChat();
            }
        }

        public static PlayerUI Get(bool opponent)
        {
            foreach (PlayerUI ui in ui_list)
            {
                if (ui.is_opponent == opponent)
                    return ui;
            }
            return null;
        }

    }
}