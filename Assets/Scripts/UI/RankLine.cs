﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class RankLine : MonoBehaviour
    {
        public Text ranking;
        public Text player;
        public Text rank;
        //public Text matches_count;
        //public Text win_count;
        //public Text win_percent;
        //public Image highlight;

        public UnityAction<string> onClick;

        private string username;

        void Start()
        {
            //highlight.enabled = false;
        }

        public void SetLine(string username, int elo, int ranking, bool highlight)
        {
            this.username = username;
            this.ranking.text = ranking.ToString();
            this.player.text = username;
            this.rank.text = elo.ToString();
            //this.matches_count.text = user.matches.ToString();
            //this.win_count.text = user.victories.ToString();
            //this.highlight.enabled = highlight;

            //int win_rate = Mathf.RoundToInt(user.victories * 100f / Mathf.Max(user.matches, 1));
            //this.win_percent.text = win_rate.ToString() + "%";

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public string GetUsername()
        {
            return username;
        }

        public void OnClick()
        {
            onClick?.Invoke(username);
        }
    }
}
