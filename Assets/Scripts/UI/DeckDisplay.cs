﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class DeckDisplay : MonoBehaviour
    {
        public CardUI[] cards;

        private string deck_id;

        void Start()
        {

        }

        void Update()
        {

        }

        public void Clear()
        {
            foreach (CardUI card in cards)
                card.Hide();
        }

        public void SetDeck(UserDeckData deck)
        {
            Clear();

            if (deck != null)
            {
                deck_id = deck.tid;

                int index = 0;
                foreach (string tid in deck.cards)
                {
                    if (index < cards.Length)
                    {
                        CardUI card_ui = cards[index];
                        string card_id = UserCardData.GetCardId(tid);
                        CardVariant variant = UserCardData.GetCardVariant(tid);
                        CardData icard = CardData.Get(card_id);
                        card_ui.SetCard(icard, variant);
                        card_ui.Show();
                        index++;
                    }
                }
            }
        }

        public void SetDeck(DeckData deck)
        {
            Clear();

            if (deck != null)
            {
                deck_id = deck.id;

                List<CardData> dcards = new List<CardData>();
                dcards.AddRange(deck.cards);

                int index = 0;
                foreach (CardData card in dcards)
                {
                    if (index < cards.Length)
                    {
                        CardUI card_ui = cards[index];
                        card_ui.SetCard(card, CardVariant.Normal);
                        card_ui.Show();
                        index++;
                    }
                }
            }
        }

        public string GetDeck()
        {
            return deck_id;
        }
    }
}
