﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate.UI
{
    public class LoadPanel : UIPanel
    {

        private static LoadPanel instance;

        protected override void Awake()
        {
            base.Awake();
            instance = this;
        }

        public static LoadPanel Get()
        {
            return instance;
        }
    }
}
