﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class TurnArea : MonoBehaviour
    {
        public Text turn_count;
        public Text turn_timer;
        public Image turn_timer_icon;

        void Start()
        {
            turn_timer.enabled = false;
            turn_timer_icon.enabled = false;
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            Game data = GameClient.Get().GetGameData();
            turn_count.text = data.turn_count.ToString();
            turn_timer.enabled = data.turn_timer > 0f;
            turn_timer.text = Mathf.RoundToInt(data.turn_timer).ToString();
            turn_timer.enabled = data.turn_timer < 999f;
            turn_timer_icon.enabled = data.turn_timer < 999f;

            //Simulate timer
            if (GameClient.online_mode && data.turn_timer > 0f)
                data.turn_timer -= Time.deltaTime;
        }
    }
}
