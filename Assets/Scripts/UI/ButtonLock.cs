﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGameTemplate.UI
{
    public class ButtonLock : MonoBehaviour
    {
        public Button button;

        private Image lock_img;


        void Start()
        {
            lock_img = GetComponent<Image>();
        }

        void Update()
        {
            if (button != null)
            {
                lock_img.enabled = !(button.enabled && button.interactable);
            }
        }
    }
}
