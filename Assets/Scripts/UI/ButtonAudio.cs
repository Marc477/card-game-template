﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGameTemplate.UI
{
    public class ButtonAudio : MonoBehaviour
    {
        public AudioClip click_audio;

        void Start()
        {
            Button button = GetComponent<Button>();
            if (button != null)
                button.onClick.AddListener(OnClick);
        }

        void OnClick()
        {
            AudioSystem.Get().PlaySFX("ui", click_audio);
        }
    }
}
