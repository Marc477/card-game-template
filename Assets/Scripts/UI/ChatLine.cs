﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class ChatLine : MonoBehaviour
    {
        public Text msg_txt;

        private const string color_tag1 = "<color=#ffc500>";
        private const string color_tag2 = "</color>";

        void Start()
        {

        }

        public void SetLine(string username, string msg)
        {
            msg_txt.text = color_tag1 + username + color_tag2 + ": " + msg;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}