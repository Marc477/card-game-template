﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;

namespace CardGameTemplate.UI
{

    public class ChoiceSelector : UIPanel
    {
        public ChoiceSelectorChoice[] choices;

        private AbilityData ability;

        private static ChoiceSelector _instance;

        protected override void Awake()
        {
            base.Awake();
            _instance = this;
            Hide();

            foreach (ChoiceSelectorChoice choice in choices)
                choice.Hide();
            foreach (ChoiceSelectorChoice choice in choices)
                choice.onClick += OnClickChoice;
        }

        protected override void Update()
        {
            base.Update();

        }

        private void RefreshSelector()
        {
            foreach (ChoiceSelectorChoice choice in choices)
                choice.Hide();

            if (ability != null)
            {
                int index = 0;
                foreach (AbilityData choice in ability.choices)
                {
                    if (index < choices.Length)
                    {
                        ChoiceSelectorChoice achoice = choices[index];
                        achoice.SetChoice(index, choice.title, choice.subtitle);
                        index++;
                    }
                }
            }
        }

        public void OnClickChoice(int index)
        {
            Game data = GameClient.Get().GetGameData();
            if (data.selector == SelectorType.SelectChoice)
            {
                GameClient.Get().SelectChoice(index);
                Hide();
            }
            else
            {
                Hide();
            }
        }

        public void OnClickCancel()
        {
            GameClient.Get().CancelSelection();
            Hide();
        }

        public void Show(AbilityData iability)
        {
            ability = iability;
            Show();
            RefreshSelector();
        }

        public static ChoiceSelector Get()
        {
            return _instance;
        }
    }
}
