﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class TabButton : MonoBehaviour
    {
        public TabPanel tab;
        public GameObject highlight;
        public string param;

        private Button button;

        public static UnityAction<TabButton> onClickButton;

        private static List<TabButton> button_list = new List<TabButton>();

        void Awake()
        {
            button_list.Add(this);
            button = GetComponent<Button>();
            button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            button_list.Remove(this);
        }

        void Start()
        {

        }

        void Update()
        {
            if (highlight != null)
            {
                bool valid_param = string.IsNullOrEmpty(param) || tab.GetParam() == param;
                highlight.SetActive(tab.IsVisible() && valid_param);

            }
        }

        void OnClick()
        {
            if (tab != null)
            {
                TabPanel.HideGroup(tab.tab_group);
                tab.ShowTab(param);
                onClickButton?.Invoke(this);
            }
        }
    }
}
