﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class ChatBubble : MonoBehaviour
    {
        public Text msg_txt_short;
        public Text msg_txt_long;
        public Image bubble_short;
        public Image bubble_long;
        public CanvasGroup group;
        public int msg_length_long = 15;

        private float timer = 0f;

        void Start()
        {

        }

        private void Update()
        {
            timer -= Time.deltaTime;
            group.alpha = timer;

            if (timer < 0f)
                Hide();
        }

        public void SetLine(string msg, float duration)
        {
            msg_txt_short.text = msg;
            msg_txt_long.text = msg;
            timer = duration;
            bubble_short.enabled = msg.Length <= msg_length_long;
            bubble_long.enabled = msg.Length > msg_length_long;
            msg_txt_short.enabled = msg.Length <= msg_length_long;
            msg_txt_long.enabled = msg.Length > msg_length_long;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}