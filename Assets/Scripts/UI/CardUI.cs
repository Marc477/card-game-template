﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class CardUI : MonoBehaviour
    {
        public Text attack;
        public Text power;
        public Text cost;
        public Image image;
        public IconValue rarity_icon;

        public Text card_title;
        public Text card_type;
        public Text card_effect;
        public Text card_quote;
        public Text card_quantity;

        public Image text_bg;
        public Text gameplay_text;
        public GameObject status_group;
        public Text status_text;

        public Material disabled_mat;

        private CardData card;
        private Animator animator;
        private bool visible = true;
        private Material default_mat;

        void Awake()
        {
            animator = GetComponent<Animator>();
            default_mat = image.material;
        }

        public void SetCard(CardData card, CardVariant variant, Card acard = null, int quantity = 1)
        {
            this.card = card;

            image.sprite = card.GetFullImage(variant);
            attack.text = card.attack.ToString();
            power.text = card.hp.ToString();
            cost.text = card.cost.ToString();

            card_title.text = card.title.ToUpper();
            card_effect.text = card.card_text;
            card_quote.text = card.card_quote;
            card_type.text = card.GetTypeText();

            RarityData rdata = RarityData.Get(card.rarity);
            if (rarity_icon != null)
            {
                rarity_icon.value = rdata.rank - 1;
                rarity_icon.Refresh();
            }

            if (gameplay_text != null)
                gameplay_text.text = card.gameplay_desc;

            if (card_quantity != null)
            {
                card_quantity.text = "x" + quantity.ToString();
                card_quantity.enabled = quantity > 0;
            }

            if (acard != null && status_text != null)
            {
                status_text.text = "";

                foreach (CardStatus astatus in acard.GetAllStatus())
                {
                    StatusData istats = StatusData.Get(astatus.effect);
                    if (istats != null && !string.IsNullOrEmpty(istats.title))
                        status_text.text += istats.title + ", ";
                }

                if (status_text.text.Length > 2)
                    status_text.text = status_text.text.Substring(0, status_text.text.Length - 2);
            }

            if (disabled_mat != null)
            {
                Material mat = quantity > 0 ? default_mat : disabled_mat;
                image.material = mat;
                if (rarity_icon != null)
                    rarity_icon.SetMat(mat);

                if (quantity == 0)
                    card_effect.text = card_effect.text.Replace("#ffc500", "#ffffff");
            }

            if (!visible)
                gameObject.SetActive(false);
            if (visible && animator != null)
                animator.Rebind();
        }

        public void RemoveCard()
        {
            this.card = null;
        }

        public void Show()
        {
            visible = true;
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            if (status_group != null)
                status_group.SetActive(status_text != null && status_text.text.Length > 0);
            if (text_bg != null)
                text_bg.enabled = true;
        }

        public void Hide()
        {
            visible = false;
            if (gameObject.activeSelf)
                gameObject.SetActive(false);
            if (status_group != null)
                status_group.SetActive(false);
            if (text_bg != null)
                text_bg.enabled = false;
        }

        public CardData GetCard()
        {
            return card;
        }
    }
}
