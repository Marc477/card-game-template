﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class CardTargetUI : UIPanel
    {
        public Text msg;
        public Image image;

        private static CardTargetUI _instance;

        protected override void Awake()
        {
            _instance = this;
            base.Awake();


        }

        protected override void Update()
        {
            base.Update();


        }

        public void ShowMsg(string msg, Sprite img)
        {
            this.msg.text = msg;
            this.image.sprite = img;
            this.image.enabled = img != null;
        }

        public void OnClickClose()
        {
            GameClient.Get().CancelSelection();
        }

        public static CardTargetUI Get()
        {
            return _instance;
        }
    }
}