﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class ConnectionPanel : UIPanel
    {

        private static ConnectionPanel instance;

        protected override void Awake()
        {
            base.Awake();
            instance = this;
        }

        public void OnClickQuit()
        {
            SceneNav.GoTo("LoginMenu");
        }

        public static ConnectionPanel Get()
        {
            return instance;
        }
    }
}
