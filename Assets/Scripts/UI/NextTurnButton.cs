﻿using CardGameTemplate.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class NextTurnButton : MonoBehaviour
    {
        public Button end_turn_button;

        public Image on_sprite;
        public float fade_speed = 2f;

        private bool auto_click = false;

        void Start()
        {

        }

        private void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            end_turn_button.interactable = !auto_click && GameClient.Get().IsYourTurn();

            float alpha = end_turn_button.interactable ? 1f : 0f;
            Color tcolor = new Color(on_sprite.color.r, on_sprite.color.g, on_sprite.color.b, alpha);
            on_sprite.color = Vector4.MoveTowards(on_sprite.color, tcolor, fade_speed * Time.deltaTime);

            //Cancel auto click
            if (!GameClient.Get().IsYourTurn() && auto_click)
                auto_click = false;

            //Auto click
            Game gdata = GameClient.Get().GetGameData();
            if (auto_click && gdata.current_state == GameState.Play)
            {
                GameClient.Get().NextStep();
                auto_click = true;
            }

            //if (Input.GetKeyDown(KeyCode.Space))
            //    OnClickNextTurn();
        }

        public void OnClickNextTurn()
        {
            Game gdata = GameClient.Get().GetGameData();
            if (GameClient.Get().IsYourTurn() && !TheUI.IsOverUILayer(5))
            {
                auto_click = gdata.current_state != GameState.Play;
                GameClient.Get().NextStep();
                PlayerControls.Get().UnselectAll();
            }
        }
    }
}
