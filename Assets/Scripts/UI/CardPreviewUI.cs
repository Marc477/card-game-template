﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class CardPreviewUI : MonoBehaviour
    {
        public UIPanel ui_panel;
        public CardUI card_left;
        public CardUI card_right;
        public float hover_delay = 1f;

        private float preview_timer = 0f;

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            card_left.Hide();
            card_right.Hide();

            HandCard card = HandCard.GetFocus();
            BoardCard bcard = BoardCard.GetFocus();
            PlayerControls controls = PlayerControls.Get();
            bool hover_type = !Input.GetMouseButton(0);

            bool should_show_preview = !HandCardArea.Get().IsDragging() && !TheUI.IsUIOpened() && (card != null || bcard != null) && hover_type;
            bool show_preview = should_show_preview && preview_timer >= hover_delay;

            if (should_show_preview)
                preview_timer += Time.deltaTime;
            else
                preview_timer = 0f;
            //preview_timer = Mathf.Clamp(preview_timer, 0f, hover_delay + 0.01f);

            ui_panel.SetVisible(show_preview);

            if (show_preview)
            {
                Card pcard = card != null ? card.GetCard() : bcard.GetCard();
                string card_id = pcard.card_id;

                CardData icard = CardData.Get(card_id);
                card_left.SetCard(icard, pcard.variant, pcard);
                card_right.SetCard(icard, pcard.variant, pcard);

                Vector3 world_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (card)
                {
                    card_right.Show();
                }
                else if (world_pos.x >= 0f)
                {
                    card_left.Show();
                }
                else if (world_pos.x < 0f)
                {
                    card_right.Show();
                }

            }

        }
    }
}
