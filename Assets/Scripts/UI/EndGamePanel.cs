﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;

namespace CardGameTemplate.UI
{
    public class EndGamePanel : UIPanel
    {
        public Text winner_text;
        public Text credits_text;
        public Text xp_text;

        //public CardUI[] card_rewards;

        //public Text name_player;
        //public Text name_other;

        private bool reward_loaded = false;
        private float timer = 0f;

        private int target_credits = 0;
        private int target_xp = 0;
        private float credits = 0;
        private float xp = 0;

        private static EndGamePanel _instance;

        protected override void Awake()
        {
            base.Awake();
            _instance = this;
        }

        protected override void Start()
        {
            base.Start();

            credits_text.text = "";
            xp_text.text = "";

            //foreach (CardUI card in card_rewards)
            //    card.Hide();
        }

        protected override void Update()
        {
            base.Update();

            if (!reward_loaded && IsVisible())
            {
                timer += Time.deltaTime;
                if (timer > 1f)
                {
                    timer = 0f;
                    RefreshRewards();
                }
            }

            if (reward_loaded)
            {
                credits = Mathf.MoveTowards(credits, target_credits, 2000f * Time.deltaTime);
                xp = Mathf.MoveTowards(xp, target_xp, 500f * Time.deltaTime);

                credits_text.text = "+ " + Mathf.RoundToInt(credits) + " credits";
                xp_text.text = "+ " + Mathf.RoundToInt(xp) + " xp";

                if (Mathf.RoundToInt(credits) == 0)
                    credits_text.text = "";
                if (Mathf.RoundToInt(xp) == 0)
                    xp_text.text = "";
            }
        }

        private void RefreshPanel(int winner)
        {
            Game data = GameClient.Get().GetGameData();
            Player pwinner = data.GetPlayer(winner);
            Player player = GameClient.Get().GetPlayer();
            Player oplayer = GameClient.Get().GetOpponentPlayer();
            //name_player.text = player.username;
            //name_other.text = oplayer.username;

            if (pwinner != null && pwinner == player)
                winner_text.text = "Victory";
            else if (pwinner != null)
                winner_text.text = "Defeat";
            else
                winner_text.text = "Tie";


        }

        private void RefreshRewards()
        {
            //Online rewards
            if (GameClient.online_mode)
            {
                string url = ApiManager.server_url + "/matches/" + GameClient.game_uid;
                ApiManager.Get().SendGetRequest(url, (WebResponse res) =>
                {
                    if (res.success)
                    {
                        reward_loaded = true;
                        MatchResponse match = ApiTool.JsonToObject<MatchResponse>(res.body);
                        string username = ApiManager.Get().GetUsername().ToLower();
                        foreach (MatchDataResponse data in match.udata)
                        {
                            if (data.username.ToLower() == username)
                            {
                                target_credits = data.reward.credits;
                                target_xp = data.reward.xp;
                            }
                        }
                    }
                });
            }
        }

        /*private void RefreshCardRewards(RewardResponse reward)
        {
            foreach (CardUI card in card_rewards)
                card.Hide();

            if (reward.cards != null && reward.cards.Length > 0)
            {
                int index = 0;
                foreach (string card_tid in reward.cards)
                {
                    if (index < card_rewards.Length)
                    {
                        CardUI card_ui = card_rewards[index];
                        CardData icard = CardData.Get(UserCardData.GetCardId(card_tid));
                        CardVariant variant = UserCardData.GetCardVariant(card_tid);
                        card_ui.SetCard(icard, variant);
                        card_ui.Show();
                        index++;
                    }
                }
            }
        }*/

        public void ShowWinner(int winner)
        {
            reward_loaded = false;
            RefreshPanel(winner);
            RefreshRewards();
            Show();
        }

        public void OnClickQuit()
        {
            TheUI.Get().OnClickQuit();
        }

        public static EndGamePanel Get()
        {
            return _instance;
        }
    }
}
