﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class DeckSelector : MonoBehaviour
    {
        public DropdownValue deck_dropdown;
        public Image artefact;
        public CardUI[] cards;

        public UnityAction<string> onChange;

        private string selected_deck;
        private bool data_mode = false;

        void Start()
        {
            deck_dropdown.onValueChanged += OnChange;
        }

        void Update()
        {

        }

        public void RefreshDecks()
        {
            selected_deck = "";
            data_mode = false;
            deck_dropdown.ClearOptions();
            artefact.enabled = false;
            foreach (CardUI card in cards)
                card.Hide();

            UserData udata = ApiManager.Get().GetUserData();
            if (udata != null)
            {
                foreach (UserDeckData deck in udata.decks)
                {
                    deck_dropdown.AddOption(deck.tid, deck.title);
                }

                if (udata.decks.Length > 0)
                    SelectDeck(udata.decks[0]);
            }
        }

        public void RefreshDecks(DeckData[] decks)
        {
            selected_deck = "";
            data_mode = true;
            deck_dropdown.ClearOptions();
            artefact.enabled = false;
            foreach (CardUI card in cards)
                card.Hide();

            deck_dropdown.AddOption("", "Random");

            foreach (DeckData deck in decks)
            {
                if (deck.IsValid())
                {
                    deck_dropdown.AddOption(deck.id, deck.title);
                }
            }

            DeckData random_deck = null;
            SelectDeck(random_deck);
        }

        private void SelectDeck(UserDeckData deck)
        {
            selected_deck = "";
            artefact.enabled = false;

            foreach (CardUI card in cards)
                card.Hide();

            if (deck != null)
            {
                selected_deck = deck.tid;

                UserData udata = ApiManager.Get().GetUserData();
                int index = 0;
                foreach (string tid in deck.cards)
                {
                    if (index < cards.Length)
                    {
                        CardUI card_ui = cards[index];
                        string card_id = UserCardData.GetCardId(tid);
                        CardVariant variant = UserCardData.GetCardVariant(tid);
                        CardData icard = CardData.Get(card_id);
                        int quantity = udata.GetCardQuantity(tid);
                        card_ui.SetCard(icard, variant, null, quantity);
                        card_ui.Show();
                        index++;
                    }
                }
            }
        }

        private void SelectDeck(DeckData deck)
        {
            selected_deck = "";
            artefact.enabled = false;

            foreach (CardUI card in cards)
                card.Hide();

            if (deck != null)
            {
                selected_deck = deck.id;

                List<CardData> dcards = new List<CardData>();
                dcards.AddRange(deck.cards);

                int index = 0;
                foreach (CardData card in dcards)
                {
                    if (index < cards.Length)
                    {
                        CardUI card_ui = cards[index];
                        card_ui.SetCard(card, CardVariant.Normal);
                        card_ui.Show();
                        index++;
                    }
                }
            }
        }

        public void SelectDeck(string deck)
        {
            UserData udata = ApiManager.Get().GetUserData();
            if (udata != null)
            {
                foreach (UserDeckData adeck in udata.decks)
                {
                    if (adeck.tid == deck)
                    {
                        deck_dropdown.SetValue(deck);
                        SelectDeck(adeck);
                    }
                }
            }
        }

        public void Lock()
        {
            deck_dropdown.interactable = false;
        }

        public void Unlock()
        {
            deck_dropdown.interactable = true;
        }

        public void SetLocked(bool locked)
        {
            deck_dropdown.interactable = !locked;
        }

        private void OnChange(int i, string val)
        {
            string value = deck_dropdown.GetSelectedValue();

            if (data_mode)
            {
                DeckData deck = DeckData.Get(value);
                SelectDeck(deck);
                onChange?.Invoke(deck.id);
            }
            else
            {
                UserData udata = ApiManager.Get().GetUserData();
                UserDeckData deck = udata?.GetDeck(value);
                SelectDeck(deck);
                onChange?.Invoke(deck.tid);
            }
        }

        public string GetDeck()
        {
            return selected_deck;
        }
    }
}