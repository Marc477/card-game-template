﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGameTemplate.UI
{
    public class LiveText : MonoBehaviour
    {
        public Text text;
        public float duration = 5f;

        void Start()
        {
            Destroy(gameObject, duration);
        }

        void Update()
        {

        }

        public static LiveText Create(string text, Vector3 pos)
        {
            if (AssetData.Get().live_text_fx != null)
            {
                GameObject fx = Instantiate(AssetData.Get().live_text_fx, pos, Quaternion.identity);
                LiveText ltxt = fx.GetComponent<LiveText>();
                ltxt.text.text = text;
                return ltxt;
            }
            return null;
        }
    }
}