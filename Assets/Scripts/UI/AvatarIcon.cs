﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class AvatarIcon : MonoBehaviour
    {
        public bool large_icon;

        public UnityAction<AvatarData> onClick;

        private Image avatar_img;
        private Button avatar_button;
        private Sprite default_icon;

        private AvatarData avatar;

        void Awake()
        {
            avatar_img = GetComponent<Image>();
            avatar_button = GetComponent<Button>();
            default_icon = avatar_img.sprite;

            if (avatar_button != null)
                avatar_button.onClick.AddListener(OnClick);
        }

        public void SetAvatar(AvatarData avatar)
        {
            this.avatar = avatar;
            avatar_img.enabled = true;
            avatar_img.sprite = default_icon;

            if (avatar != null)
            {
                if (large_icon)
                    avatar_img.sprite = avatar.avatar_large;
                else
                    avatar_img.sprite = avatar.avatar;
            }
        }

        public void RemoveAvatar()
        {
            this.avatar = null;
            avatar_img.enabled = true;
            avatar_img.sprite = default_icon;
        }

        public void HideAvatar()
        {
            this.avatar = null;
            avatar_img.enabled = false;
        }

        public AvatarData GetAvatar()
        {
            return avatar;
        }

        private void OnClick()
        {
            if (avatar != null)
                onClick?.Invoke(avatar);
        }
    }
}