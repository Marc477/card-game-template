﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class TabPanel : UIPanel
    {
        public string tab_group;
        public bool visible_at_start;

        public UnityAction<string> onShowTab;

        private string param;

        private static List<TabPanel> tab_list = new List<TabPanel>();

        protected override void Awake()
        {
            base.Awake();
            tab_list.Add(this);
        }

        void OnDestroy()
        {
            tab_list.Add(this);
        }

        protected override void Start()
        {
            base.Start();

            if (visible_at_start)
                Show(true);
        }

        protected override void Update()
        {
            base.Update();

        }

        public void ShowTab(string param = "")
        {
            HideGroup(tab_group);
            this.param = param;
            Show();

            if (onShowTab != null)
                onShowTab.Invoke(param);
        }

        public string GetParam()
        {
            return param;
        }

        public override void Hide(bool instant = false)
        {
            base.Hide(instant);
            param = "";
        }

        public static void HideAll()
        {
            foreach (TabPanel tab in tab_list)
            {
                tab.Hide();
            }
        }

        public static void HideGroup(string group)
        {
            foreach (TabPanel tab in tab_list)
            {
                if (tab.tab_group == group)
                    tab.Hide();
            }
        }

        public static List<TabPanel> GetAll(string group)
        {
            List<TabPanel> group_list = new List<TabPanel>();
            foreach (TabPanel tab in tab_list)
            {
                if (tab.tab_group == group)
                    group_list.Add(tab);
            }
            return group_list;
        }
    }
}
