﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate.UI;

namespace CardGameTemplate
{

    public class TheGame : MonoBehaviour
    {

        public static GameSettings settings = GameSettings.Default;
        public static string selected_deck = "";
        public static string selected_deck_ai = "";
        public static int selected_ai_level = 10;
        public static string observe_user = "";

        private float timer = 0f;
        private bool game_ended = false;

        private static TheGame _instance;

        void Awake()
        {
            _instance = this;
            PlayerData.LoadLast();
        }

        private void Start()
        {
            GameClient.Get().onConnected += AfterConnected;

        }

        private void AfterConnected()
        {
            GameClient client = GameClient.Get();
            string username = ApiManager.Get().GetUsername();
            string avatar = ApiManager.Get().GetAvatar();

            if (!GameClient.online_mode && !ApiManager.Get().IsLoggedIn())
            {
                //Test mode
                client.SendGameplaySettings(settings);
                client.SendAIPlayerSettings(1, "AI", GameplayData.Get().test_deck_ai, selected_ai_level);
                client.SendPlayerSettings(0, username, GameplayData.Get().test_deck);
            }
            else if (!GameClient.online_mode)
            {
                //Local mode
                client.SendGameplaySettings(settings);
                DeckData ai_deck = DeckData.Get(selected_deck_ai);
                ai_deck = ai_deck == null ? GameplayData.Get().test_deck_ai : ai_deck;
                client.SendAIPlayerSettings(1, "AI", ai_deck, selected_ai_level);
                client.SendPlayerSettings(username, selected_deck);
            }
            else
            {
                //Online mode
                client.SendGameplaySettings(settings);
                client.SendPlayerSettings(username, selected_deck);
            }
        }

        void Update()
        {

            if (!GameClient.Get().IsConnected())
                return;

            Game data = GameClient.Get().GetGameData();
            timer += Time.deltaTime;

            //Game end
            if (!game_ended && data.current_state == GameState.GameEnded)
            {
                game_ended = true;
                EndGame();
                StartCoroutine(EndGameRun());
            }

            //Exit
            Game gdata = GameClient.Get().GetGameData();
            if (gdata.current_state == GameState.Connecting || gdata.current_state == GameState.Starting)
            {
                if (timer > 10f)
                {
                    SceneNav.GoTo("Menu");
                }
            }
        }

        private void EndGame()
        {

        }

        private IEnumerator EndGameRun()
        {
            Game data = GameClient.Get().GetGameData();
            Player pwinner = data.GetPlayer(data.current_player);
            Player player = GameClient.Get().GetPlayer();
            bool win = pwinner != null && player.player_id == pwinner.player_id;
            bool tied = pwinner == null;

            TheCamera.Get().Shake();
            AudioSystem.Get().FadeOutMusic("music");

            yield return new WaitForSeconds(1f);

            if (win && AssetData.Get().win_fx != null)
                Instantiate(AssetData.Get().win_fx, Vector3.zero, Quaternion.identity);
            else if (tied && AssetData.Get().tied_fx != null)
                Instantiate(AssetData.Get().tied_fx, Vector3.zero, Quaternion.identity);
            else if (tied && AssetData.Get().lose_fx != null)
                Instantiate(AssetData.Get().lose_fx, Vector3.zero, Quaternion.identity);

            if (win)
                AudioSystem.Get().PlaySFX("ending_sfx", AssetData.Get().win_audio);
            else
                AudioSystem.Get().PlaySFX("ending_sfx", AssetData.Get().defeat_audio);

            if (win)
                AudioSystem.Get().PlayMusic("music", AssetData.Get().win_music, 0.4f, false);
            else
                AudioSystem.Get().PlayMusic("music", AssetData.Get().defeat_music, 0.4f, false);

            yield return new WaitForSeconds(3f);


            EndGamePanel.Get().ShowWinner(data.current_player);
        }

        private void OnApplicationQuit()
        {
            GameClient.Get().Resign();
        }

        public static TheGame Get()
        {
            return _instance;
        }
    }
}