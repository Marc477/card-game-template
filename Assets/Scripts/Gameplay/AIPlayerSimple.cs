﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate.Gameplay;

namespace CardGameTemplate.AI
{
    public class AIPlayerSimple
    {
        public int player_id;

        private GameplayLogic gameplay;
        private bool is_playing = false;
        private bool is_selecting = false;

        public AIPlayerSimple(GameplayLogic gameplay, int id)
        {
            this.gameplay = gameplay;
            player_id = id;
        }

        public void Update()
        {
            Game game_data = gameplay.GetGameData();

            if (game_data.current_state == GameState.Play)
            {
                if (!is_playing && game_data.current_player == player_id)
                {
                    is_playing = true;
                    GameTool.StartCoroutine(AiTurn());
                }
            }

            if (game_data.current_state == GameState.Selector)
            {
                if (!is_selecting && game_data.selector == SelectorType.SelectCardTarget && game_data.selector_player == player_id)
                {
                    //AI select target
                    is_selecting = true;
                    GameTool.StartCoroutine(AiSelectTarget());
                }

                if (!is_selecting && game_data.selector == SelectorType.SelectCardSelector && game_data.selector_player == player_id)
                {
                    //AI select target
                    is_selecting = true;
                    GameTool.StartCoroutine(AiSelectCard());
                }

                if (!is_selecting && game_data.selector == SelectorType.SelectChoice && game_data.selector_player == player_id)
                {
                    //AI select target
                    is_selecting = true;
                    GameTool.StartCoroutine(AiSelectChoice());
                }
            }
        }

        private bool AICanPlay()
        {
            Game game_data = gameplay.GetGameData();
            if (player_id == game_data.current_player)
            {
                bool other_player_selecting = game_data.selector != SelectorType.None && game_data.selector_player != player_id;
                return game_data.current_state == GameState.Play && !other_player_selecting;
            }
            return false;
        }

        private IEnumerator AiTurn()
        {
            Game game_data = gameplay.GetGameData();
            Player player = game_data.GetPlayer(player_id);

            yield return new WaitForSeconds(0.7f);

            PlayCard();

            yield return new WaitForSeconds(0.7f);

            PlayCard();

            yield return new WaitForSeconds(0.7f);

            PlayCard();

            yield return new WaitForSeconds(0.5f);

            gameplay.NextStep(player_id);

            is_playing = false;
        }

        private IEnumerator AiSelectCard()
        {
            yield return new WaitForSeconds(0.5f);

            SelectCard();

            yield return new WaitForSeconds(0.5f);

            gameplay.CancelSelection(player_id);
            is_selecting = false;
        }

        private IEnumerator AiSelectTarget()
        {
            yield return new WaitForSeconds(0.5f);

            SelectTarget();

            yield return new WaitForSeconds(0.5f);

            gameplay.CancelSelection(player_id);
            is_selecting = false;
        }

        private IEnumerator AiSelectChoice()
        {
            yield return new WaitForSeconds(0.5f);

            SelectChoice();

            yield return new WaitForSeconds(0.5f);

            gameplay.CancelSelection(player_id);
            is_selecting = false;
        }

        //----------

        public void PlayCard()
        {
            Game game_data = gameplay.GetGameData();
            Player player = game_data.GetPlayer(player_id);
            if (player.cards_hand.Count > 0)
            {
                Card random = player.GetRandomCard(player.cards_hand);
                if (random != null)
                    gameplay.TryPlayCard(player_id, random.uid, 0, 0);
            }
        }

        public void SelectCard()
        {
            Game game_data = gameplay.GetGameData();
            Player player = game_data.GetPlayer(player_id);
            Player oplayer = game_data.GetOpponentPlayer(player_id);
            List<Card> card_list = player.cards_discard;
            if (game_data.selector_source == AbilityTarget.SelectInHand)
                card_list = player.cards_hand;
            if (game_data.selector_source == AbilityTarget.SelectInDeck)
                card_list = player.cards_deck;
            if (game_data.selector_source == AbilityTarget.SelectInOpponentHand)
                card_list = oplayer.cards_hand;
            // if (game_data.selector_source == AbilityTarget.SelectInVoid)
            //    card_list = player.cards_void;

            if (card_list.Count > 0)
            {
                Card card = card_list[Random.Range(0, card_list.Count)];
                gameplay.SelectCard(player_id, card.uid);
            }
        }

        public void SelectTarget()
        {
            Game game_data = gameplay.GetGameData();
            Player player = game_data.GetPlayer(player_id);
            Player oplayer = game_data.GetPlayer(player_id == 0 ? 1 : 0);

            if (game_data.selector != SelectorType.None)
            {
                int target_player = player_id;
                AbilityData ability = AbilityData.Get(game_data.selector_ability_id);
                if (ability != null && ability.target == AbilityTarget.SelectCard)
                    target_player = (player_id == 0 ? 1 : 0);

                Player tplayer = game_data.GetPlayer(target_player);
                if (tplayer.cards_board.Count > 0)
                {
                    Card random = tplayer.GetRandomCard(tplayer.cards_board);

                    if (random != null)
                        gameplay.SelectCard(player_id, random.uid);
                }
            }
        }

        public void SelectChoice()
        {
            Game game_data = gameplay.GetGameData();

            if (game_data.selector != SelectorType.None)
            {
                AbilityData ability = AbilityData.Get(game_data.selector_ability_id);
                if (ability != null && ability.choices.Length > 0)
                {
                    int choice = Random.Range(0, ability.choices.Length);
                    gameplay.SelectChoice(player_id, choice);
                }
            }
        }

    }

}