﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate.UI;

namespace CardGameTemplate
{

    public class BoardSlot : MonoBehaviour
    {
        public int index;
        public bool opponent;

        private SpriteRenderer render;
        private float current_alpha = 0f;

        private static List<BoardSlot> slot_list = new List<BoardSlot>();

        void Awake()
        {
            slot_list.Add(this);
            render = GetComponent<SpriteRenderer>();
            render.color = Color.clear;
        }

        private void OnDestroy()
        {
            slot_list.Remove(this);
        }

        private void Start()
        {

        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            //int player_id = opponent ? GameClient.Get().GetOpponentPlayerID() : GameClient.Get().GetPlayerID();
            PlayerSelectType select_type = PlayerControls.Get().GetSelectType();
            BoardCard slot_select_target = PlayerControls.Get().GetSelected();
            HandCard drag_card = HandCard.GetDrag();

            Game gdata = GameClient.Get().GetGameData();
            Player player = GameClient.Get().GetPlayer();
            Card slot_card = gdata.GetSlotCard(GetSlot());

            float target_alpha = 0f;
            if (drag_card != null && !opponent && slot_card == null && player.CanDoPoseAction())
            {
                target_alpha = 1f;
            }

            Card select_card = slot_select_target?.GetCard();
            bool can_do_move = select_card != null && select_type == PlayerSelectType.CardAction && slot_card == null && player.CanDoMoveAction(select_card) && select_card.CanMoveTo(GetSlot());
            bool can_do_attack = select_card != null && select_type == PlayerSelectType.CardAction && slot_card != null && player.CanDoAttackAction(select_card) && select_card.CanAttackTarget(slot_card);

            Card caster_card = gdata.GetCard(gdata.selector_caster_uid);
            if (can_do_attack || can_do_move)
            {
                target_alpha = 1f;
            }

            current_alpha = Mathf.MoveTowards(current_alpha, target_alpha, 2f * Time.deltaTime);
            render.color = new Color(1f, 1f, 1f, current_alpha);
        }

        public Slot GetSlot()
        {
            int p = opponent ? GameClient.Get().GetOpponentPlayerID() : GameClient.Get().GetPlayerID();
            return new Slot(index, p);
        }

        public void OnMouseDown()
        {
            if (TheUI.IsOverUI())
                return;

            Game gdata = GameClient.Get().GetGameData();
            int player_id = GameClient.Get().GetPlayerID();

            /*if (gdata.current_state == GameState.Play && player_id == gdata.current_player)
            {
                BoardCard card = PlayerControls.Get().GetSelected();
                PlayerSelectType type = PlayerControls.Get().GetSelectType();

                if (type == PlayerSelectType.CardAction && card != null)
                {
                    Card acard = card.GetCard();
                    Card tcard = gdata.GetSlotCard(GetSlot());

                    //Cant move to already occupied space
                    if (tcard == null)
                        GameClient.Get().Move(acard, GetSlot());

                    PlayerControls.Get().UnselectAll();
                }
            }*/

            if (gdata.current_state == GameState.Selector && gdata.selector == SelectorType.SelectSlot && player_id == gdata.selector_player)
            {
                GameClient.Get().SelectSlot(GetSlot());
            }
        }

        public static BoardSlot GetNearest(Vector3 pos, float range = 999f)
        {
            BoardSlot nearest = null;
            float min_dist = range;
            foreach (BoardSlot slot in GetAll())
            {
                float dist = (slot.transform.position - pos).magnitude;
                if (dist < min_dist)
                {
                    min_dist = dist;
                    nearest = slot;
                }
            }
            return nearest;
        }

        public static BoardSlot Get(Slot slot)
        {
            return Get(slot.x, slot.p);
        }

        public static BoardSlot Get(int index, int player_side)
        {
            int p = GameClient.Get().GetPlayerID();
            bool opponent = p != player_side;
            return Get(index, opponent);
        }

        public static BoardSlot Get(int index, bool opponent)
        {
            foreach (BoardSlot slot in GetAll())
            {
                if (slot.index == index && slot.opponent == opponent)
                    return slot;
            }
            return null;
        }

        public static List<BoardSlot> GetAll()
        {
            return slot_list;
        }
    }
}