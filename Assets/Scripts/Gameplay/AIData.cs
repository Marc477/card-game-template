﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate.AI
{
    public enum AIOrderType
    {
        None=0,
        Play=5,
        Move=10,
        Attack=15,
        CastAbility=30,
        SelectCard=40,
        SelectSlot=42,
        CancelSelect=45,
        EndTurn =50,
        Resign=60,
    }

    public class AIOrder
    {
        public AIOrderType type;
        public string card_id;
        public string target_id;
        public string card_uid;
        public string target_uid;
        public Slot slot = Slot.None;
        public string ability_id;
        public int value = 0;

        public AIOrder() { }
        public AIOrder(AIOrderType t) { type = t; }

        public string GetText()
        {
            string txt = type.ToString();
            if (card_id != null)
                txt += " c " + card_id;
            if (target_id != null)
                txt += " t " + target_id;
            if (slot != Slot.None)
                txt += " s " + slot.x + "-" + slot.p;
            if (ability_id != null)
                txt += " a " + ability_id;
            if (value > 0)
                txt += " v " + value;
            return txt;
        }

        public static AIOrder None { get { AIOrder a = new AIOrder(); a.type = AIOrderType.None; return a; } }
    }

    public class CardA
    {
        public string card_id;
        public string uid;
        public int player_id;

        public Slot slot;

        public int attack = 0;
        public int hp = 0;

        public int attack_bonus = 0;
        public int hp_bonus = 0;

        public int attack_ongoing_bonus = 0;
        public int hp_ongoing_bonus = 0;

        public List<CardStatus> ongoing_status = new List<CardStatus>();
        public List<CardStatus> status_effects = new List<CardStatus>();

        public CardData card_data;

        public CardA(Card acard)
        {
            card_id = acard.card_id;
            uid = acard.uid;
            player_id = acard.player_id;
            
            slot = acard.slot;

            attack = acard.attack;
            hp = acard.hp;

            attack_bonus = acard.attack_bonus;
            hp_bonus = acard.hp_bonus;

            attack_ongoing_bonus = acard.attack_ongoing_bonus;
            hp_ongoing_bonus = acard.hp_ongoing_bonus;

            status_effects = new List<CardStatus>();
            foreach (CardStatus astatus in acard.status_effects)
                status_effects.Add(AIData.Clone(astatus));
            ongoing_status = new List<CardStatus>();
            foreach (CardStatus astatus in acard.ongoing_status)
                ongoing_status.Add(AIData.Clone(astatus));

            card_data = CardData.Get(card_id);
        }

        public CardA(CardA acard)
        {
            card_id = acard.card_id;
            uid = acard.uid;
            player_id = acard.player_id;

            slot = acard.slot;

            attack = acard.attack;
            hp = acard.hp;

            attack_bonus = acard.attack_bonus;
            hp_bonus = acard.hp_bonus;

            attack_ongoing_bonus = acard.attack_ongoing_bonus;
            hp_ongoing_bonus = acard.hp_ongoing_bonus;

            status_effects = new List<CardStatus>();
            foreach (CardStatus astatus in acard.status_effects)
                status_effects.Add(AIData.Clone(astatus));
            ongoing_status = new List<CardStatus>();
            foreach (CardStatus astatus in acard.ongoing_status)
                ongoing_status.Add(AIData.Clone(astatus));

            card_data = CardData.Get(card_id);
        }

        public void Refresh() { }

        public void Cleanse()
        {
            attack = 0;
            status_effects.Clear();
            ongoing_status.Clear();
            CleanseOngoing();
        }

        public void CleanseOngoing()
        {
            ongoing_status.Clear();
            attack_ongoing_bonus = 0;
            hp_ongoing_bonus = 0;
        }

        public int GetAttack() { return attack + attack_bonus + attack_ongoing_bonus; }
        public int GetHP() { return hp + hp_bonus + hp_ongoing_bonus; }

        public CardStatus GetStatus(StatusEffect effect)
        {
            foreach (CardStatus status in status_effects)
            {
                if (status.effect == effect)
                    return status;
            }
            return null;
        }

        public CardStatus GetOngoingStatus(StatusEffect effect)
        {
            foreach (CardStatus status in ongoing_status)
            {
                if (status.effect == effect)
                    return status;
            }
            return null;
        }

        public bool HasStatusEffect(StatusEffect effect)
        {
            foreach (CardStatus status in status_effects)
            {
                if (status.effect == effect)
                    return true;
            }
            foreach (CardStatus status in ongoing_status)
            {
                if (status.effect == effect)
                    return true;
            }
            return false;
        }

        public void AddStatus(StatusData istatus, int value, int duration)
        {
            if (istatus != null)
            {
                CardStatus effect = GetStatus(istatus.effect);
                if (effect == null)
                {
                    effect = new CardStatus(istatus.effect, value, duration);
                    status_effects.Add(effect);
                }
                else
                {
                    effect.value += value;
                    effect.duration += duration;
                    effect.permanent = effect.permanent || duration == 0;
                }
            }
        }

        public void RemoveStatus(StatusEffect effect)
        {
            for (int i = status_effects.Count - 1; i >= 0; i--)
            {
                if (status_effects[i].effect == effect)
                    status_effects.RemoveAt(i);
            }
        }

        public AbilityData GetAbility(AbilityTrigger trigger, AbilityEffect effect)
        {
            foreach (AbilityData iability in card_data.abilities)
            {
                if (iability.trigger == trigger && iability.effect == effect)
                    return iability;
            }
            return null;
        }

        public bool HasAbility(AbilityTrigger trigger, AbilityEffect effect)
        {
            AbilityData iability = GetAbility(trigger, effect);
            if (iability != null)
                return true;
            return false;
        }

        public bool HasActiveAbility(AIData data, AbilityTrigger trigger, AbilityEffect effect)
        {
            AbilityData iability = GetAbility(trigger, effect);
            if (iability != null && CanDoAbilities() && AIData.AreConditionsMet(data, iability, this))
                return true;
            return false;
        }

        public bool CanDoActions()
        {
            if (HasStatusEffect(StatusEffect.Paralysed))
                return false;
            return true;
        }

        public bool CanDoAbilities()
        {
            if (HasStatusEffect(StatusEffect.Paralysed))
                return false;
            if (HasStatusEffect(StatusEffect.NoAbility))
                return false;

            return true;
        }

        public bool CanMoveTo(Slot aslot)
        {
            return this.slot.IsNextTo(aslot);
        }

        public bool CanAttackTarget(CardA target)
        {
            if (player_id == target.player_id)
                return false; //Cant attack same player

            if (target.HasStatusEffect(StatusEffect.Stealth))
                return false; //Stealth cant be attacked

            if (target.HasStatusEffect(StatusEffect.Protected) && !target.HasStatusEffect(StatusEffect.Protection))
                return false; //Protected by adjacent card

            return true;
        }

        public void Clear()
        {
            ongoing_status.Clear();
            status_effects.Clear();
        }
    }

    public class PlayerA
    {
        public int player_id;
        public bool is_ai = false;

        public int actions = 0;
        public int kill_count = 0;

        public List<CardA> cards_hand = new List<CardA>();
        public List<CardA> cards_board = new List<CardA>();
        public List<CardA> cards_discard = new List<CardA>();

        public PlayerA(Player aplayer)
        {
            player_id = aplayer.player_id;
            is_ai = aplayer.is_ai;
            actions = aplayer.actions;
            kill_count = aplayer.kill_count;

            cards_board = new List<CardA>();
            cards_hand = new List<CardA>();
            cards_discard = new List<CardA>();

            foreach (Card card in aplayer.cards_board)
                cards_board.Add(new CardA(card));
            foreach (Card card in aplayer.cards_hand)
                cards_hand.Add(new CardA(card));

            if (player_id == aplayer.player_id)
            {
                foreach (Card card in aplayer.cards_discard)
                    cards_discard.Add(new CardA(card));
            }
        }

        public PlayerA(PlayerA aplayer)
        {
            player_id = aplayer.player_id;
            is_ai = aplayer.is_ai;
            actions = aplayer.actions;
            kill_count = aplayer.kill_count;

            cards_board = new List<CardA>();
            cards_hand = new List<CardA>();
            cards_discard = new List<CardA>();

            foreach (CardA card in aplayer.cards_board)
                cards_board.Add(new CardA(card));
            foreach (CardA card in aplayer.cards_hand)
                cards_hand.Add(new CardA(card));

            if (player_id == aplayer.player_id)
            {
                foreach (CardA card in aplayer.cards_discard)
                    cards_discard.Add(new CardA(card));
            }
        }

        public CardA GetBoardCard(string uid)
        {
            foreach (CardA card in cards_board)
            {
                if (card.uid == uid)
                    return card;
            }
            return null;
        }

        public CardA GetHandCard(string uid)
        {
            foreach (CardA card in cards_hand)
            {
                if (card.uid == uid)
                    return card;
            }
            return null;
        }

        public CardA GetDiscardCard(string uid)
        {
            foreach (CardA card in cards_discard)
            {
                if (card.uid == uid)
                    return card;
            }
            return null;
        }

        public bool CanDoMoveAction(CardA card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0;
        }

        public bool CanDoAttackAction(CardA card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0;
        }

        public bool CanDoActivation(CardA card, AbilityData ability)
        {
            if (card == null || !card.CanDoActions())
                return false;

            if (ability.trigger == AbilityTrigger.Activate && card.CanDoAbilities())
            {
                bool has_action = !ability.use_action || actions > 0 ;
                return has_action;
            }
            return false;
        }

        public bool CanDoAnyAction(CardA card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0 || CanDoMoveAction(card) || CanDoAttackAction(card);
        }

        public bool CanDoPoseAction()
        {
            return actions > 0 ;
        }

        public bool CanDoAnyAction()
        {
            foreach (CardA card in cards_board)
            {
                if (CanDoAnyAction(card))
                    return true;
            }

            return actions > 0;
        }

        public void Clear()
        {
            foreach (CardA card in cards_hand)
                card.Clear();
            foreach (CardA card in cards_board)
                card.Clear();
            foreach (CardA card in cards_discard)
                card.Clear();
            cards_hand.Clear();
            cards_board.Clear();
            cards_discard.Clear();
        }
    }

    public class AIData
    {
        public int player_id = 0;
        public int first_player = 0;
        public int current_player = 0;
        public int turn_count = 0;
        public GameState current_state;

        public int orders_count = 0;

        public PlayerA[] players;

        public AIData(Game gdata, int player_id)
        {
            this.player_id = player_id;
            first_player = gdata.first_player;
            current_player = gdata.current_player;
            turn_count = gdata.turn_count;
            current_state = gdata.current_state;

            players = new PlayerA[2];
            players[0] = new PlayerA(gdata.players[0]);
            players[1] = new PlayerA(gdata.players[1]);
        }

        public AIData(AIData odata)
        {
            this.player_id = odata.player_id;
            first_player = odata.first_player;
            current_player = odata.current_player;
            turn_count = odata.turn_count;
            current_state = odata.current_state;

            orders_count = odata.orders_count;

            players = new PlayerA[2];
            players[0] = new PlayerA(odata.players[0]);
            players[1] = new PlayerA(odata.players[1]);
        }

        public static CardStatus Clone(CardStatus astatus)
        {
            CardStatus status = new CardStatus(astatus.effect, astatus.value, astatus.duration);
            status.permanent = astatus.permanent;
            return status;
        }

        public PlayerA GetPlayer(int id)
        {
            if (id >= 0 && id < players.Length)
                return players[id];
            return null;
        }

        public PlayerA GetOpponent(int id)
        {
            int oid = id == 0 ? 1 : 0;
            return GetPlayer(oid);
        }

        public CardA GetSlotCard(Slot slot)
        {
            foreach (PlayerA player in players)
            {
                foreach (CardA card in player.cards_board)
                {
                    if (card != null && card.slot == slot)
                        return card;
                }
            }
            return null;
        }

        public bool IsCardInHand(Card card)
        {
            PlayerA player = GetPlayer(card.player_id);
            return player.GetHandCard(card.uid) != null;
        }

        public bool IsCardOnBoard(Card card)
        {
            PlayerA player = GetPlayer(card.player_id);
            return player.GetBoardCard(card.uid) != null;
        }

        public bool IsCardInDiscard(Card card)
        {
            PlayerA player = GetPlayer(card.player_id);
            return player.GetDiscardCard(card.uid) != null;
        }

        public bool IsCardOnSlot(Slot slot)
        {
            return GetSlotCard(slot) != null;
        }

        public static bool AreConditionsMet(AIData data, AbilityData ability, CardA caster)
        {
            bool met = true;
            foreach (ConditionData cond in ability.conditions)
            {
                if (cond && !cond.IsConditionMet(data, caster, caster))
                    met = false;
            }
            return met;
        }

        public static bool AreTargetConditionsMet(AIData data, AbilityData ability, CardA caster, CardA target)
        {
            bool met = true;
            foreach (ConditionData cond in ability.conditions_target)
            {
                if (cond && !cond.IsConditionMet(data, caster, target))
                    met = false;
            }
            return met;
        }
    }

}
