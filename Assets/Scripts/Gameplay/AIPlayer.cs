﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate.AI;

namespace CardGameTemplate.Gameplay
{
    public class AIPlayer
    {
        public int player_id;
        private int ai_level = 3;

        private GameplayLogic gameplay;
        private bool is_playing = false;
        private int orders_count = 0;
        private Card target_select;
        private Slot slot_select;

        public AIPlayer(GameplayLogic gameplay, int id, int level)
        {
            this.gameplay = gameplay;
            player_id = id;
            ai_level = Mathf.Clamp(level, 0, 10);
        }

        public void Update()
        {
            Game game_data = gameplay.GetGameData();

            if (game_data.current_state == GameState.StartTurn)
            {
                orders_count = 0;
            }

            if (game_data.current_state == GameState.Play)
            {
                if (!is_playing && game_data.current_player == player_id)
                {
                    is_playing = true;
                    GameTool.StartCoroutine(AiTurn());
                }
            }

            if (game_data.current_state == GameState.Selector)
            {
                if (!is_playing && game_data.selector_player == player_id)
                {
                    is_playing = true;
                    GameTool.StartCoroutine(AiSelector());
                }
            }
        }

        private IEnumerator AiTurn()
        {
            Game game_data = gameplay.GetGameData();
            AILogic ai_logic = AILogic.CalculateAI(game_data, player_id, ai_level, orders_count);

            while (ai_logic.IsRunning())
            {
                yield return new WaitForSeconds(0.1f);
            }

            AIOrder best = ai_logic.GetBestMove();
            Debug.Log("Execute AI Order: " + best.GetText() + "\n" + ai_logic.GetNodePath());

            //foreach(NodeState node in ai_logic.GetFirst().childs)
            //    Debug.Log(ai_logic.GetNodePath(node));

            yield return new WaitForSeconds(0.02f);

            if (best != null)
            {
                ExecutePreOrder(best);
                yield return new WaitForSeconds(0.5f);
                ExecuteOrder(best);
            }

            ai_logic.ClearMemory();

            yield return new WaitForSeconds(0.1f);
            is_playing = false;
        }

        private IEnumerator AiSelector()
        {
            yield return new WaitForSeconds(0.5f);

            Game game_data = gameplay.GetGameData();
            Player player = game_data.GetPlayer(player_id);
            Player oplayer = game_data.GetOpponentPlayer(player_id);
            AbilityData iability = AbilityData.Get(game_data.selector_ability_id);
            if (iability.target == AbilityTarget.SelectCard)
            {
                if(target_select == null && oplayer.cards_board.Count > 0)
                    target_select = oplayer.cards_board[Random.Range(0, oplayer.cards_board.Count)];
                if (target_select != null)
                    gameplay.SelectCard(player_id, target_select.uid);
            }
           
            if (iability.target == AbilityTarget.SelectSlot)
            {
                if (slot_select == Slot.None)
                    slot_select = new Slot(Random.Range(1, 5), Random.Range(0, 2));
                if (slot_select != Slot.None)
                    gameplay.SelectSlot(player_id, slot_select.x, slot_select.p);
            }

            if (iability.target == AbilityTarget.SelectInDiscard && player.cards_discard.Count > 0)
            {
                Card card = player.cards_discard[Random.Range(0, player.cards_discard.Count)];
                if (card != null)
                    gameplay.SelectCard(player_id, card.uid);
            }

            if (iability.target == AbilityTarget.SelectInOpponentHand && oplayer.cards_hand.Count > 0)
            {
                Card card = oplayer.cards_hand[Random.Range(0, oplayer.cards_hand.Count)];
                if (card != null)
                    gameplay.SelectCard(player_id, card.uid);
            }

            yield return new WaitForSeconds(0.1f);

            target_select = null;
            slot_select = Slot.None;
            gameplay.CancelSelection(player_id);

            yield return new WaitForSeconds(0.1f);
            is_playing = false;
        }

        //----------

        private void ExecutePreOrder(AIOrder order)
        {
            
        }

        private void ExecuteOrder(AIOrder order)
        {
            orders_count++;

            if (order.type == AIOrderType.Play)
            {
                PlayCard(order.card_uid, order.slot);
            }

            if (order.type == AIOrderType.Attack)
            {
                AttackCard(order.card_uid, order.target_uid);
            }

            if (order.type == AIOrderType.Move)
            {
                MoveCard(order.card_uid, order.slot);
            }

            if (order.type == AIOrderType.CastAbility)
            {
                CastAbility(order.card_uid, order.ability_id, order.target_uid, order.slot);
            }

            if (order.type == AIOrderType.SelectCard)
            {
                SelectCard(order.card_uid);
            }

            if (order.type == AIOrderType.SelectSlot)
            {
                SelectSlot(order.slot);
            }

            if (order.type == AIOrderType.CancelSelect)
            {
                CancelSelect();
            }

            if (order.type == AIOrderType.EndTurn)
            {
                orders_count = 0;
                EndTurn();
            }

            if (order.type == AIOrderType.Resign)
            {
                Resign();
            }
        }

        private void PlayCard(string card_uid, Slot slot)
        {
            Game game_data = gameplay.GetGameData();
            Card card = game_data.GetCard(card_uid);
            if (card != null)
            {
                gameplay.TryPlayCard(player_id, card.uid, slot.x, slot.p);
            }
        }

        private void MoveCard(string card_uid, Slot slot)
        {
            Game game_data = gameplay.GetGameData();
            Card card = game_data.GetCard(card_uid);
            if (card != null)
            {
                gameplay.MoveCard(player_id, card.uid, slot.x, slot.p); 
            }
        }

        private void AttackCard(string attacker_uid, string target_uid)
        {
            Game game_data = gameplay.GetGameData();
            Card card = game_data.GetCard(attacker_uid);
            Card target = game_data.GetCard(target_uid);
            if (card != null && target != null)
            {
                gameplay.AttackTarget(player_id, card.uid, target.uid);
            }
        }

        private void CastAbility(string caster_uid, string ability_id, string target_uid, Slot target_slot)
        {
            Game game_data = gameplay.GetGameData();
            Card card = game_data.GetCard(caster_uid);
            AbilityData iability = AbilityData.Get(ability_id);
            if (card != null && iability != null)
            {
                target_select = game_data.GetCard(target_uid);
                slot_select = target_slot;
                gameplay.CastCardAbility(player_id, card.uid, ability_id);
            }
        }

        private void SelectCard(string card_uid)
        {
            Game game_data = gameplay.GetGameData();
            Card card = game_data.GetCard(card_uid);
            if (card != null)
            {
                gameplay.SelectCard(player_id, card.uid);
            }
        }

        private void SelectSlot(Slot slot)
        {
            gameplay.SelectSlot(player_id, slot.x, slot.p);
        }

        private void CancelSelect()
        {
            gameplay.CancelSelection(player_id);
        }

        private void EndTurn()
        {
            gameplay.NextStep(player_id);
        }

        private void Resign()
        {
            gameplay.Resign(player_id);
        }

    }
    
}