﻿using CardGameTemplate.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.UI;

namespace CardGameTemplate
{

    public class HandCard : MonoBehaviour
    {
        public Image card_sprite;
        public Image card_glow;
        public Text attack;
        public Text power;
        public Text cost;
        public IconValue rarity_icon;

        public Text card_title;
        public Text card_type;
        public Text card_effect;
        public Text card_quote;

        public float move_speed = 10f;
        public float move_rotate_speed = 4f;
        public float move_max_rotate = 10f;

        [HideInInspector]
        public Vector2 deck_position;
        [HideInInspector]
        public float deck_angle;

        private RectTransform hand_transform;
        private RectTransform card_transform;
        private Card card;
        private Vector3 start_scale;
        private float current_alpha = 0f;
        private Vector3 current_rotate;
        private Vector3 target_rotate;
        private Vector3 prev_pos;

        private bool destroyed = false;
        private float focus_timer = 0f;

        private bool focus = false;
        private bool drag = false;

        private static List<HandCard> card_list = new List<HandCard>();

        void Awake()
        {
            card_list.Add(this);
            card_transform = transform.GetComponent<RectTransform>();
            hand_transform = transform.parent.GetComponent<RectTransform>();
            start_scale = transform.localScale;
        }

        private void Start()
        {

        }

        private void OnDestroy()
        {
            card_list.Remove(this);
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            focus_timer += Time.deltaTime;

            Vector2 target_position = deck_position;
            Vector3 target_size = start_scale;

            float target_alpha = 1f;
            bool player_dragging = HandCardArea.Get().IsDragging();

            if (focus && focus_timer > 0.5f)
            {
                target_position = deck_position + Vector2.up * 40f;
            }

            if (drag)
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(hand_transform, Input.mousePosition, Camera.main, out target_position);
                target_size = start_scale * 0.8f;
                Vector3 dir = card_transform.position - prev_pos;
                Vector3 addrot = new Vector3(dir.y * 90f, -dir.x * 90f, 0f);
                target_rotate += addrot * move_rotate_speed * Time.deltaTime;
                target_rotate = new Vector3(Mathf.Clamp(target_rotate.x, -move_max_rotate, move_max_rotate), Mathf.Clamp(target_rotate.y, -move_max_rotate, move_max_rotate), 0f);
                current_rotate = Vector3.Lerp(current_rotate, target_rotate, move_rotate_speed * Time.deltaTime);
                move_speed = 9f;
                target_alpha = 0.8f;
            }
            else
            {
                target_rotate = new Vector3(0f, 0f, deck_angle);
                current_rotate = new Vector3(0f, 0f, deck_angle);
            }

            card_transform.anchoredPosition = Vector2.Lerp(card_transform.anchoredPosition, target_position, Time.deltaTime * move_speed);
            card_transform.rotation = Quaternion.Slerp(card_transform.rotation, Quaternion.Euler(current_rotate), Time.deltaTime * move_speed);
            card_transform.localScale = Vector3.Lerp(card_transform.localScale, target_size, 4f * Time.deltaTime);

            card_glow.enabled = (focus && !player_dragging) || drag;
            current_alpha = Mathf.MoveTowards(current_alpha, target_alpha, 2f * Time.deltaTime);
            card_sprite.color = new Color(1f, 1f, 1f, current_alpha);
            card_glow.color = new Color(card_glow.color.r, card_glow.color.g, card_glow.color.b, current_alpha * 0.8f);

            prev_pos = Vector3.Lerp(prev_pos, card_transform.position, 1f * Time.deltaTime);
        }

        public void SetCard(Card card)
        {
            this.card = card;

            CardData icard = CardData.Get(card.card_id);
            if (icard)
            {
                TeamData team = TeamData.Get(icard.team);
                card_sprite.sprite = icard.GetSmallImage(card.variant);
                attack.text = icard.attack.ToString();
                power.text = icard.hp.ToString();
                cost.text = icard.cost.ToString();

                card_title.text = icard.title.ToUpper();
                card_effect.text = icard.card_text;
                card_quote.text = icard.card_quote;
                card_type.text = icard.GetTypeText();

                RarityData rdata = RarityData.Get(icard.rarity);
                if (rarity_icon != null)
                {
                    rarity_icon.value = rdata.rank - 1;
                    rarity_icon.Refresh();
                }
            }
        }

        public void Kill()
        {
            if (!destroyed)
            {
                destroyed = true;
                Destroy(gameObject, 1f);
            }
        }

        public bool IsVisible()
        {
            return card != null;
        }

        public bool IsFocus()
        {
            return focus && !drag;
        }

        public bool IsDrag()
        {
            return drag;
        }

        public Card GetCard()
        {
            return card;
        }
		
		public string GetCardUID()
        {
            return card.uid;
        }

        public void OnMouseEnterCard()
        {
            if (TheUI.IsUIOpened())
                return;

            focus = true;
        }

        public void OnMouseExitCard()
        {
            focus = false;
            focus_timer = 0f;
        }

        public void OnMouseDownCard()
        {
            if (!TheUI.IsOverUILayer(5))
            {
                drag = true;
                PlayerControls.Get().UnselectAll();
                AudioSystem.Get().PlaySFX("hand_card", AssetData.Get().hand_card_click_audio);
            }
        }

        public void OnMouseUpCard()
        {
            if (drag && card_transform.anchoredPosition.y > 200f)
                HandCardArea.Get().TryPlayCard(this);
            else
                HandCardArea.Get().SortCards();
            drag = false;
        }

        public static HandCard GetDrag()
        {
            foreach (HandCard card in card_list)
            {
                if (card.IsDrag())
                    return card;
            }
            return null;
        }

        public static HandCard GetFocus()
        {
            foreach (HandCard card in card_list)
            {
                if (card.IsFocus())
                    return card;
            }
            return null;
        }

        public static HandCard Get(string uid)
        {
            foreach (HandCard card in card_list)
            {
                if (card && card.GetCard() != null && card.GetCard().uid == uid)
                    return card;
            }
            return null;
        }

        public static List<HandCard> GetAll()
        {
            return card_list;
        }
    }
}
