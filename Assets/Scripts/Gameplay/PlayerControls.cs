﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using UnityEngine.Events;
using CardGameTemplate.UI;

namespace CardGameTemplate
{

    public enum PlayerSelectType
    {
        Normal = 0,
        CardAction = 10,
        //CardAbility=15,
        //AssignPower=20,
    }

    public class PlayerControls : MonoBehaviour
    {
        private PlayerSelectType selection;
        private BoardCard selected_card;

        private static PlayerControls instance;

        void Awake()
        {
            instance = this;
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            if (Input.GetMouseButtonDown(1))
                UnselectAll();

            if (selection == PlayerSelectType.CardAction)
            {
                if (Input.GetMouseButtonUp(0))
                    ReleaseClick();
            }
        }

        public void SelectCard(BoardCard card)
        {
            if (selection == PlayerSelectType.CardAction && selected_card == card)
            {
                UnselectAll();
                return;
            }

            bool yourturn = GameClient.Get().IsYourTurn();
            bool yourcard = card.GetCard().player_id == GameClient.Get().GetPlayerID();

            //Attack
            /*if (prev_select != null && selection == PlayerSelectType.CardAction && yourturn)
            {
                GameClient.Get().AttackTarget(prev_select.GetCard(), card.GetCard());
                UnselectAll();
            }
            else */
            if (yourcard && yourturn) //Select
            {
                selection = PlayerSelectType.CardAction;
                selected_card = card;

                CardData icard = card.GetCardInfo();
                AudioSystem.Get().PlaySFX("card_select", icard.click_audio);
            }
        }

        public void SelectCardRight(BoardCard card)
        {
            bool yourturn = GameClient.Get().IsYourTurn();
            bool yourcard = card.GetCard().player_id == GameClient.Get().GetPlayerID();
            if (yourcard && yourturn && !Input.GetMouseButton(0))
            {

            }
        }

        private void ReleaseClick()
        {
            bool yourturn = GameClient.Get().IsYourTurn();
            Game gdata = GameClient.Get().GetGameData();

            if (yourturn && selection == PlayerSelectType.CardAction && selected_card != null)
            {
                Vector3 wpos = GetMouseWorldPos();
                BoardSlot tslot = BoardSlot.GetNearest(wpos, 2f);
                Card target = tslot ? gdata.GetSlotCard(tslot.GetSlot()) : null;
                AbilityButton ability = AbilityButton.GetHover(wpos, 1f);

                if (ability != null && ability.IsVisible())
                {
                    ability.OnClick();
                }
                else if (target != null && target.uid != selected_card.GetCardUID())
                {
                    GameClient.Get().AttackTarget(selected_card.GetCard(), target);
                }
                else if (tslot != null)
                {
                    GameClient.Get().Move(selected_card.GetCard(), tslot.GetSlot());
                }

                UnselectAll();
            }
        }

        public void UnselectAll()
        {
            selection = PlayerSelectType.Normal;
            selected_card = null;
        }

        public BoardCard GetSelected()
        {
            return selected_card;
        }

        public PlayerSelectType GetSelectType()
        {
            return selection;
        }

        public static Vector3 GetMouseWorldPos()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane plane = new Plane(Vector3.forward, 0f);
            plane.Raycast(ray, out float dist);
            Vector3 tpos = ray.GetPoint(dist);
            return tpos;
        }

        public static PlayerControls Get()
        {
            return instance;
        }
    }
}