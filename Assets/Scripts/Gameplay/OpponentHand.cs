﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;

namespace CardGameTemplate
{

    public class OpponentHand : MonoBehaviour
    {
        public RectTransform card_area;
        public GameObject card_template;
        public float card_spacing = 100f;
        public float card_angle = 10f;
        public float card_offset_y = 10f;

        private List<RectTransform> cards = new List<RectTransform>();

        void Start()
        {
            card_template.SetActive(false);
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            Game gdata = GameClient.Get().GetGameData();
            Player player = gdata.GetPlayer(GameClient.Get().GetOpponentPlayerID());

            if (cards.Count < player.cards_hand.Count)
            {
                GameObject new_card = Instantiate(card_template, card_area);
                new_card.SetActive(true);
                RectTransform card_rect = new_card.GetComponent<RectTransform>();
                card_rect.anchoredPosition = new Vector2(0f, 100f);
                cards.Add(card_rect);
            }

            if (cards.Count > player.cards_hand.Count)
            {
                RectTransform card = cards[cards.Count - 1];
                cards.RemoveAt(cards.Count - 1);
                Destroy(card.gameObject);
            }

            int nb_cards = Mathf.Min(cards.Count, player.cards_hand.Count);

            for (int i = 0; i < nb_cards; i++)
            {
                RectTransform card = cards[i];
                float half = nb_cards / 2f;
                Vector3 tpos = new Vector3((i - half) * card_spacing, (i - half) * (i - half) * card_offset_y);
                float tangle = (i - half) * card_angle;
                card.anchoredPosition = Vector3.Lerp(card.anchoredPosition, tpos, 4f * Time.deltaTime);
                card.transform.localRotation = Quaternion.Slerp(card.transform.localRotation, Quaternion.Euler(0f, 0f, tangle), 4f * Time.deltaTime);
            }

        }
    }
}