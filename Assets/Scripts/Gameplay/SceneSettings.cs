﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{
    public class SceneSettings : MonoBehaviour
    {
        public AudioClip start_audio;
        public AudioClip[] game_music;
        public AudioClip[] game_ambience;

        private static SceneSettings instance;

        private void Awake()
        {
            instance = this;
        }

        void Start()
        {
            if (TheGame.settings.play_mode != PlayMode.Adventure)
            {
                AudioSystem.Get().PlaySFX("game_sfx", start_audio);
                if (game_music.Length > 0)
                    AudioSystem.Get().PlayMusic("music", game_music[Random.Range(0, game_music.Length)]);
                if (game_ambience.Length > 0)
                    AudioSystem.Get().PlaySFX("ambience", game_ambience[Random.Range(0, game_ambience.Length)], 0.5f, true);
            }
        }

        void Update()
        {

        }

        public static SceneSettings Get()
        {
            return instance;
        }
    }
}
