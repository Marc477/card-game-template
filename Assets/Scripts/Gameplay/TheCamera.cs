﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public class TheCamera : MonoBehaviour
    {
        private float shake_timer = 0f;
        private float shake_intensity = 1f;

        private Vector3 shake_vector = Vector3.zero;
        private Vector3 start_pos;

        private static TheCamera instance;

        void Awake()
        {
            instance = this;
            start_pos = transform.position;
        }

        void Update()
        {
            //Shake FX
            if (shake_timer > 0f)
            {
                shake_timer -= Time.deltaTime;
                shake_vector = new Vector3(Mathf.Cos(shake_timer * Mathf.PI * 16f) * 0.02f, Mathf.Sin(shake_timer * Mathf.PI * 12f) * 0.01f, 0f);
                transform.position = start_pos + shake_vector * shake_intensity;
            }
            else
            {
                transform.position = start_pos;
            }
        }

        public void Shake(float intensity = 1f, float duration = 1f)
        {
            shake_intensity = intensity;
            shake_timer = duration;
        }


        public static TheCamera Get()
        {
            return instance;
        }
    }
}