﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using CardGameTemplate;

namespace CardGameTemplate.Gameplay
{
    public class AbilityQueueElement
    {
        public AbilityData ability;
        public Card caster;
    }

    public class AttackQueueElement
    {
        public Card attacker;
        public Card target;
        public bool smart; // if true, will wont attack if enemy is stronger
    }

    public class GameplayLogic
    {
        public UnityAction onReady;
        public UnityAction onGameStart;
        public UnityAction<int> onGameEnd;

        public UnityAction<int> onNewTurn;
        public UnityAction<string> onCardPlayed;
        public UnityAction<string> onCardDiscarded;
        public UnityAction<string> onCardMove;

        public UnityAction<string, string> onAbilityTrigger; //ability, caster
        public UnityAction<string, string, string> onAbilityEffect; //ability, caster, target
        public UnityAction<string, string> onAbilityAfter; //ability, caster

        public UnityAction<string, string> onAttackTarget; //Attacker, target
        public UnityAction<string, string> onAttackAfter; //Attacker, target, result

        public UnityAction onRefreshAllData;

        private GameplayData game_info;
        private Game game_data;

        private Card selector_caster;
        private AbilityData selector_ability;
        private Card selector_target_selected;

        private Card last_played_card = null;
        private Card last_target_card = null;
        private Card last_killed_card = null;
        
        private Card current_caster = null;
        private AbilityData current_ability = null;
        private Card current_card = null;

        private HashSet<string> card_played = new HashSet<string>();
        private HashSet<string> ability_played = new HashSet<string>();
        private List<Card> save_targets = new List<Card>();

        private Queue<AbilityQueueElement> ability_cast_queue = new Queue<AbilityQueueElement>();
        private Queue<AttackQueueElement> attack_queue = new Queue<AttackQueueElement>();

        private float queue_timer = 0f;
        private float refresh_timer = 0f;

        private List<AIPlayer> ai_list = new List<AIPlayer>();

        private static GameplayLogic _instance;

        public GameplayLogic()
        {
            _instance = this;
            game_info = GameplayData.Get();
        }

        public void Update()
        {
            if (game_data == null)
                return;

            queue_timer += Time.deltaTime;
            if (game_data.current_state == GameState.AbilityQueue)
            {
                if (queue_timer > 0.1f)
                {
                    queue_timer = 0;
                    TriggerNextAbilityQueue();
                }
            }

            if (game_data.current_state == GameState.Play)
            {
                game_data.turn_timer -= Time.deltaTime;

                if (ability_cast_queue.Count > 0 || attack_queue.Count > 0)
                {
                    game_data.current_state = GameState.AbilityQueue;
                }
                else if (game_data.turn_timer <= 0f)
                {
                    NextStep(game_data.current_player);
                }
            }

            if (game_data.current_state == GameState.Selector)
            {
                game_data.turn_timer -= Time.deltaTime;
                if (game_data.turn_timer <= 0f)
                {
                    NextStep(game_data.current_player);
                }
            }

            if (game_data.current_state == GameState.Connecting)
            {
                CheckForPlayersReady();
            }

            foreach (AIPlayer ai in ai_list)
                ai.Update();

            refresh_timer += Time.deltaTime;
            if (refresh_timer > 5f)
                RefreshData();
        }

        public void CreateNewGame(string uid, int nb_humans)
        {
            game_data = new Game(uid, 2);
            game_data.first_player = 0;
            game_data.current_player = -1; //No one playing yet

            //Default Settings
            game_data.settings = GameSettings.Default;

            //Fill player info
            foreach (Player player in game_data.players)
            {
                player.username = "-";
                player.is_ai = player.player_id < nb_humans ? false : true;
            }

            //Default AI players
            foreach (Player player in game_data.players)
            {
                if (player.is_ai)
                {
                    player.username = game_info.test_deck_ai.title;
					player.ready = true;
                    DeckData deck = game_info.test_deck_ai;
                    SetPlayerDeck(player.player_id, deck.id, deck.cards);
                }
            }
        }

        private void CheckForPlayersReady()
        {
            if (game_data.current_state == GameState.Connecting)
            {
                bool all_connected = game_data.AreAllPlayersConnected();
                bool all_ready = game_data.AreAllPlayersReady();
                if (all_connected && all_ready)
                {
                    //Debug.Log("Game Starting " + game_data.game_uid);
                    game_data.current_state = GameState.Starting;
					NetworkTool.StartCoroutine(RunStartGame());
                    onReady?.Invoke();
					onGameStart?.Invoke();
                }
            }
        }
		
        public void StartNewTurn()
        {
            if (game_data.current_state == GameState.GameEnded)
                return;

            StartPhase(GameState.StartTurn);
            RefreshData();

            onNewTurn?.Invoke(game_data.current_player);
        }

        public void NextPlayerTurn()
        {
            if (game_data.current_state == GameState.GameEnded)
                return;

            game_data.current_player = (game_data.current_player + 1) % game_data.nb_players;
            
            if (game_data.current_player == game_data.first_player)
                game_data.turn_count++;
            
            StartNewTurn();
        }

        private void ClearTurnData()
        {
            game_data.selector = SelectorType.None;
            selector_target_selected = null;
            selector_caster = null;
            last_played_card = null;
            last_target_card = null;
            last_killed_card = null;
            ability_cast_queue.Clear();
            attack_queue.Clear();
            card_played.Clear();
            ability_played.Clear();
            save_targets.Clear();
        }

        public void StartPhase(GameState phase)
        {
            if (game_data.current_state == GameState.GameEnded)
                return;

            game_data.current_state = phase;

            if (phase == GameState.StartTurn)
                NetworkTool.StartCoroutine(RunStartPhase());
            if (phase == GameState.EndTurn)
                NetworkTool.StartCoroutine(RunEndPhase());
        }

        private IEnumerator RunStartGame()
        {
            RefreshData();

            yield return new WaitForSeconds(1f);

            //Draw first cards and spawn hero
            foreach (Player player in game_data.players)
            {
                ShuffleDeck(player.cards_deck);
                DrawCard(player.player_id, GameplayData.Get().starting_cards);
            }

            RefreshData();
            yield return new WaitForSeconds(0.5f);

            game_data.current_player = game_data.first_player;
            game_data.turn_count = 1;

            StartNewTurn();
            RefreshData();
        }

        private IEnumerator RunStartPhase()
        {
            ClearTurnData();
            CheckForWinner();

            yield return new WaitForSeconds(0.1f);

            Player player = game_data.GetActivePlayer();

            //Refresh current player cards on board
            for (int i = player.cards_board.Count - 1; i >= 0; i--)
            {
                Card card = player.cards_board[i];
                card.Refresh();
            }

            RefreshData();

            yield return new WaitForSeconds(0.2f);

            //Cards draw
            if (game_data.turn_count > 1 || player.player_id != game_data.first_player)
            {
                DrawCard(player.player_id, 1);
            }

            //Actions
            player.actions = 1;

            //Turn timer
            game_data.turn_timer = GameplayData.Get().turn_duration;

            //Abilities
            CalculateOngoingBonus();

            for (int i = player.cards_board.Count - 1; i >= 0; i--)
            {
                Card card = player.cards_board[i];
                TriggerCardAbilityType(card, AbilityTrigger.StartOfTurn, false);
            }

            StartPhase(GameState.AbilityQueue);
            RefreshData();
        }

        private IEnumerator RunEndPhase()
        {
            RefreshData();
            yield return new WaitForSeconds(0.1f);

            Player player = game_data.GetActivePlayer();

            //Remove conditions
            foreach (Player aplayer in game_data.players)
            {
                foreach (Card card in aplayer.cards_all)
                {
                    for (int i = card.status_effects.Count - 1; i >= 0; i--)
                    {
                        if (!card.status_effects[i].permanent)
                        {
                            card.status_effects[i].duration -= 1;
                            if (card.status_effects[i].duration <= 0)
                                card.status_effects.RemoveAt(i);
                        }
                    }
                }
            }

            //Refresh current player cards on board
            for (int i = player.cards_board.Count - 1; i >= 0; i--)
            {
                Card card = player.cards_board[i];
                card.Refresh();
                TriggerCardAbilityType(card, AbilityTrigger.EndOfTurn, true);
            }

            if (player.actions > 0 && player.cards_board.Count == 0 && !player.is_ai)
                player.no_action_count++;
            else
                player.no_action_count = 0;

            player.actions = 0;

            RefreshData();
            yield return new WaitForSeconds(0.1f);

            CheckForWinner();

            NextPlayerTurn();

            RefreshData();
        }

        private void CheckForWinner()
        {
            //Check for winner

            GameplayData gdata = GameplayData.Get();

            List<Player> winners = new List<Player>();

            //Generic wins
            foreach (Player player in game_data.players)
            {
                Player other_player = game_data.GetOpponentPlayer(player.player_id);

                //Generic lose (no more cards)
                if (other_player.cards_hand.Count == 0 && other_player.cards_board.Count == 0 && other_player.cards_deck.Count == 0)
                {
                    if (!winners.Contains(player))
                        winners.Add(player);
                }
            }

            //Test cheat win, REMOVE THIS
            //if (Input.GetKey(KeyCode.W))
            //    winners.Add(game_data.GetPlayer(0));

            //Find the best player among winners
            if (winners.Count > 0)
            {
                EndGame(-1);
            }
        }

        private void CalculateOngoingBonus()
        {
            foreach (Player player in game_data.players)
            {
                foreach (Card card in player.cards_all)
                {
                    card.CleanOngoing();
                }
            }

            //CalculateOngoingTeamEffect();
            CalculateOngoingStatus();
            CalculateOngoingStat();
        }

        private void CalculateOngoingStatus()
        {
            //Self abilities
            foreach (Player player in game_data.players)
            {
                foreach (Card card in player.cards_board)
                {
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        CardData icard = CardData.Get(card.card_id);
                        foreach (AbilityData ability in icard.abilities)
                        {
                            if (ability && ability.trigger == AbilityTrigger.Ongoing && ability.AreConditionsMet(game_data, card))
                            {
                                if (ability.target == AbilityTarget.Self && ability.AreTargetConditionsMet(game_data, card, card))
                                {
                                    foreach (StatusData istatus in ability.status_effects)
                                    {
                                        if (!card.HasStatusEffect(istatus.effect))
                                        {
                                            CardStatus status = new CardStatus(istatus.effect, ability.value, 0);
                                            card.ongoing_status.Add(status);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Status from other cards abilities
            foreach (Player player in game_data.players)
            {
                foreach (Card card in player.cards_board)
                {
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        foreach (Player aplayer in game_data.players)
                        {
                            foreach (Card othercard in aplayer.cards_board)
                            {
                                if (!othercard.HasStatusEffect(StatusEffect.NoAbility) && !card.HasStatusEffect(StatusEffect.NoAbility))
                                {
                                    CardData iothercard = CardData.Get(othercard.card_id);
                                    foreach (AbilityData ability in iothercard.abilities)
                                    {
                                        if (ability && ability.trigger == AbilityTrigger.Ongoing && ability.AreConditionsMet(game_data, card))
                                        {
                                            if (ability.AreTargetConditionsMet(game_data, othercard, card))
                                            {
                                                if (ability.target == AbilityTarget.AllCards
                                                    || (ability.target == AbilityTarget.AllAllies && othercard.player_id == card.player_id)
                                                    || (ability.target == AbilityTarget.AllEnemies && othercard.player_id != card.player_id))
                                                {
                                                    foreach (StatusData istatus in ability.status_effects)
                                                    {
                                                        if (!card.HasStatusEffect(istatus.effect))
                                                        {
                                                            CardStatus status = new CardStatus(istatus.effect, ability.value, 0);
                                                            card.ongoing_status.Add(status);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //Area ongoing effects
                                    if (card != othercard && card.player_id == othercard.player_id)
                                    {
                                        //Protection
                                        if (othercard.HasStatusEffect(StatusEffect.Protection) && !card.HasStatusEffect(StatusEffect.Protection))
                                        {
                                            if (!card.HasStatusEffect(StatusEffect.Protected) && card.slot.IsNextToSameRow(othercard.slot))
                                            {
                                                CardStatus pstatus = new CardStatus(StatusEffect.Protected, 0, 0);
                                                card.ongoing_status.Add(pstatus);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CalculateOngoingStat()
        {
            foreach (Player player in game_data.players)
            {
                foreach (Card card in player.cards_board)
                {
                    //Power bonus from its own status
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        //Add ongoing abilities
                        CardData icard = CardData.Get(card.card_id);
                        foreach (AbilityData ability in icard.abilities)
                        {
                            if (ability && ability.trigger == AbilityTrigger.Ongoing && ability.AreConditionsMet(game_data, card))
                            {
                                if (ability.AreTargetConditionsMet(game_data, card, card))
                                {
                                    List<Card> targets = GetAbilityTargets(card, ability);
                                    foreach (Card target in targets)
                                    {
                                        if(ability.AreTargetConditionsMet(game_data, card, target))
                                            AddOngoingAbilityBonus(ability, target);
                                    }
                                }
                            }
                        }

                        //Add from status effect
                        foreach (CardStatus cond in card.GetAllStatus())
                        {
                            AddOngoingStatusBonus(cond, card);
                        }
                    }
                }
            }
        }

        private void AddOngoingAbilityBonus(AbilityData ability, Card target)
        {
            if (ability == null || target == null)
                return;

            if (ability.effect == AbilityEffect.AddAttack)
                target.attack_ongoing_bonus += ability.value;
            if (ability.effect == AbilityEffect.AddAttack)
                target.hp_ongoing_bonus += ability.value;

            target.attack_ongoing_bonus = Mathf.Max(target.attack_ongoing_bonus, 0);
            target.hp_ongoing_bonus = Mathf.Max(target.hp_ongoing_bonus, 0);
        }

        private void AddOngoingStatusBonus(CardStatus status, Card target)
        {
            StatusData istatus = StatusData.Get(status.effect);
            if (istatus != null)
            {
                if (istatus.effect == StatusEffect.AddAttack)
                    target.attack_ongoing_bonus += status.value;
                if (istatus.effect == StatusEffect.AddHP)
                    target.hp_ongoing_bonus += status.value;

                target.attack_ongoing_bonus = Mathf.Max(target.attack_ongoing_bonus, 0);
                target.hp_ongoing_bonus = Mathf.Max(target.hp_ongoing_bonus, 0);
            }
        }

        private void ShuffleDeck(List<Card> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                Card temp = cards[i];
                int randomIndex = Random.Range(i, cards.Count);
                cards[i] = cards[randomIndex];
                cards[randomIndex] = temp;
            }
        }

        private void DrawCard(int player_id, int nb = 1)
        {
            Player player = game_data.GetPlayer(player_id);
            for (int i = 0; i < nb; i++)
            {
                if (player.cards_deck.Count > 0)
                {
                    Card card = player.cards_deck[0];
                    player.cards_deck.RemoveAt(0);
                    player.cards_hand.Add(card);
                }
            }
        }

        private void DrawDiscardCard(int player_id, int nb = 1)
        {
            Player player = game_data.GetPlayer(player_id);
            for (int i = 0; i < nb; i++)
            {
                if (player.cards_deck.Count > 0)
                {
                    Card card = player.cards_deck[0];
                    player.cards_deck.RemoveAt(0);
                    player.cards_discard.Add(card);
                }
            }
        }

        private Card SummonCard(int player_id, Card copy)
        {
            if (copy == null)
                return null;

            CardData icard = CardData.Get(copy.card_id);
            return SummonCard(player_id, new CardData[] { icard });
        }

        private Card SummonCard(int player_id, CardData[] choices)
        {
            Card acard = SummonCardHand(player_id, choices);
            return acard;
        }

        private Card SummonCardHand(int player_id, CardData[] choices)
        {
            Player player = game_data.GetPlayer(player_id);
            CardData card = choices[Random.Range(0, choices.Length)];
            Card acard = new Card(card.id, GameTool.GenerateUID(11, 15), player.player_id);
            player.AddCard(player.cards_all, acard);
            player.AddCard(player.cards_hand, acard);
            return acard;
        }

        private void KillCard(Card attacker, Card target)
        {
            if (attacker == null || target == null)
                return;

            if (target.HasStatusEffect(StatusEffect.Invincibility))
                return;

            bool doublelife = target.HasStatusEffect(StatusEffect.DoubleLife);
            if (doublelife)
            {
                CardStatus status = target.GetStatus(StatusEffect.DoubleLife);
                status.value--;
                if (status.value <= 0)
                    RemoveStatus(target, StatusEffect.DoubleLife);
                return;
            }

            Player pattacker = game_data.GetPlayer(attacker.player_id);
            if (attacker.player_id != target.player_id)
                pattacker.kill_count++;

            DiscardCard(target);

            TriggerCardAbilityType(attacker, AbilityTrigger.OnKill, false);
        }

        private void DiscardCard(Card card)
        {
            if (card == null)
                return;

            if (game_data.IsDiscarded(card.uid))
                return; //Already discarded

            CardData icard = CardData.Get(card.card_id);
            Player player = game_data.GetPlayer(card.player_id);
            bool was_on_board = game_data.IsCardOnBoard(card.uid);
            
            //Remove card from current location
            RemoveCardFromAllGroups(card);

            //Send card to the right place
            player.AddCard(player.cards_discard, card);
            last_killed_card = card;
            
            if (was_on_board)
            {
                //Trigger on death abilities
                TriggerCardAbilityType(card, AbilityTrigger.OnDeath, true);
            }

            card.Cleanse();

            if (onCardDiscarded != null)
                onCardDiscarded.Invoke(card.uid);
        }

        //Card removed completely (like after a tranform)
        private void RemoveCardFromAllGroups(Card card)
        {
            Player player = game_data.GetPlayer(card.player_id);

            player.RemoveCard(player.cards_deck, card);
            player.RemoveCard(player.cards_hand, card);
            player.RemoveCard(player.cards_board, card);
            player.RemoveCard(player.cards_deck, card);
            player.RemoveCard(player.cards_discard, card);
        }

        private void SendToHand(Card card)
        {
            if (card == null)
                return;

            Player player = game_data.GetPlayer(card.player_id);

            RemoveCardFromAllGroups(card);
            card.Cleanse();

            player.AddCard(player.cards_hand, card);
        }

        private void SendBottomDeck(Card card)
        {
            if (card == null)
                return;

            Player player = game_data.GetPlayer(card.player_id);

            RemoveCardFromAllGroups(card);
            card.Cleanse();

            player.AddCard(player.cards_deck, card);
        }

        //Use if card in hand or deck only
        private void SendDiscard(Card card)
        {
            if (card == null)
                return;

            Player player = game_data.GetPlayer(card.player_id);

            RemoveCardFromAllGroups(card);
            card.Cleanse();

            player.AddCard(player.cards_discard, card);
        }

        //--- Action from client ------

        public void SetPlayerReady(int player_id, bool ready)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player != null && game_data.current_state == GameState.Connecting)
            {
                player.ready = ready;
            }
        }

        public void SetPlayerConnected(int player_id, bool connected)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player != null)
            {
                player.connected = connected;
            }
        }

        public void SetPlayerName(int player_id, string name)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player != null && game_data.current_state == GameState.Connecting)
            {
                player.username = name;
            }
        }

        public void SetPlayerDeck(int player_id, string username, string deck)
        {
            Player player = game_data.GetPlayer(player_id); 
            if (player != null && game_data.current_state == GameState.Connecting)
            {
                ApiTool.GetUserData(username, (UserData user) =>
                {
                    //Use user deck
                    if (user != null)
                    {
                        UserDeckData udeck = user.GetDeck(deck);
                        if (udeck != null)
                        {
                            SetPlayerDeck(player_id, udeck);
                            RefreshData();
                            return;
                        }
                    }

                    //Use premade deck
                    DeckData cdeck = DeckData.Get(deck);

                    //Use test deck
                    if (cdeck == null)
                        cdeck = game_info.test_deck;

                    SetPlayerDeck(player_id, cdeck.id, cdeck.cards);
                    RefreshData();
                });
            }
        }

        public void SetPlayerDeck(int player_id, string deck_id, CardData[] cards)
        {
            Player player = game_data.GetPlayer(player_id);
            player.cards_all.Clear();
            player.cards_deck.Clear();
            player.deck = deck_id;

            foreach (CardData card in cards)
            {
                Card acard = new Card(card.id, GameTool.GenerateUID(11, 15), player.player_id);
                player.cards_all.Add(acard);
                player.cards_deck.Add(acard);
            }

            //Shuffle deck
            ShuffleDeck(player.cards_deck);
        }

        private void SetPlayerDeck(int player_id, UserDeckData deck)
        {
            Player player = game_data.GetPlayer(player_id);
            
            player.cards_all.Clear();
            player.cards_deck.Clear();
            player.deck = deck.tid;

            foreach (string tid in deck.cards)
            {
                string card_id = UserCardData.GetCardId(tid);
                CardVariant variant = UserCardData.GetCardVariant(tid);

                Card acard = new Card(card_id, GameTool.GenerateUID(11, 15), player.player_id);
                acard.variant = variant;
                player.cards_all.Add(acard);
                player.cards_deck.Add(acard);
            }

            //Shuffle deck
            ShuffleDeck(player.cards_deck);
        }

        public void SetAiLevel(int player_id, int ai_level)
        {
            Player player = game_data.GetPlayer(player_id);
            player.is_ai = true;

            if (player.is_ai)
            {
                player.ai_level = ai_level;

                AIPlayer ai_gameplay = new AIPlayer(this, player.player_id, ai_level);
                ai_list.Add(ai_gameplay);
            }
        }

        public void SetGameplaySettings(GameSettings settings)
        {
            if (game_data.current_state == GameState.Connecting)
            {
                game_data.settings = settings;
                
                if(settings.play_mode != PlayMode.Adventure)
                    game_data.first_player = Random.value < 0.5f ? 0 : 1;
            }
        }

        //-----------------

        public void TryPlayCard(int player_id, string card_uid, int slot_x, int slot_side)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player == null)
                return;

            if (game_data.current_state != GameState.Play || player_id != game_data.current_player)
                return;  //Not your turn

            if (player_id != slot_side)
                return; //Can only play on your side

            Card card = player.GetCard(card_uid);
            Slot slot = new Slot(slot_x, player_id);
            if (card != null && slot.IsValid())
            {
                if (player.HasCard(player.cards_hand, card) && CanPayAction(card, false, false, true))
                {
                    PlayCard(card, slot, true);
                }
            }
        }

        private void PlayCard(Card card, Slot slot, bool pay_action = false)
        {
            if (card == null || !slot.IsValid())
                return;

            if (game_data.IsCardOnSlot(slot))
                return; //Already a card there

            Player player = game_data.GetPlayer(card.player_id);
            CardData icard = CardData.Get(card.card_id);

            player.RemoveCard(player.cards_hand, card); //May be from hand
            player.RemoveCard(player.cards_discard, card); //May be from discard (if using ability)
            player.RemoveCard(player.cards_deck, card); //May be from deck (if using ability)

            player.AddCard(player.cards_board, card);
            card.Cleanse();

            card.slot = slot;
            card.attack = icard.attack;
            card.hp = icard.hp;

            if (pay_action)
                PayAction(card, false, false, true);

            //Save play data
            current_card = card;
            card_played.Add(card.card_id);

            CalculateOngoingBonus();

            //Trigger abilities
            TriggerCardAbilityType(card, AbilityTrigger.OnPlay, false);

            if (onCardPlayed != null)
                onCardPlayed.Invoke(card.uid);

            //Refresh
            CalculateOngoingBonus();
            RefreshData();
        }

        public void CastCardAbility(int player_id, string card_uid, string ability_id)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player == null)
                return;

            bool is_your_turn = game_data.current_player == player_id;
            bool is_your_step = (is_your_turn && game_data.current_state == GameState.Play);

            if (!is_your_step)
                return; //Not yours to play

            Card card = player.GetCard(card_uid);
            if (card != null && card.CanDoAbilities())
            {
                CardData icard = CardData.Get(card.card_id);
                if (icard != null && card.player_id == player_id)
                {
                    AbilityData iability = AbilityData.Get(ability_id);
                    if (iability != null && iability.trigger == AbilityTrigger.Activate 
                        && player.CanDoActivation(card, iability) && iability.AreConditionsMet(game_data, card))
                    {
                        if (!iability.use_action || CanPayAction(card, false, false, false))
                        {
                            RemoveStatus(card, StatusEffect.Stealth);
                            AddToAbilityCastQueue(card, iability);
                        }
                    }
                }

                CalculateOngoingBonus();
                RefreshData();
            }
        }

        public void MoveCard(int player_id, string card_uid, int slot_x, int slot_side)
        {
            if (game_data.current_state != GameState.Play || player_id != game_data.current_player)
                return;  //Not your turn

            Player player = game_data.GetPlayer(player_id);
            if (player == null)
                return;

            Card card = player.GetCard(card_uid);
            Slot slot = new Slot(slot_x, slot_side);

            if (card != null)
            {
                if (!CanPayAction(card, true, false, false))
                    return; //No actions

                Card slot_card = game_data.GetSlotCard(slot);
                if (slot_card != null || !slot.IsValid())
                    return; //Cant move to this slot

                if (card.slot == slot)
                    return; // Cant move to save slot

                if (card.CanMoveTo(slot))
                {
                    card.slot = slot;
                    PayAction(card, true, false, false);

                    RemoveStatus(card, StatusEffect.Stealth);

                    game_data.current_state = GameState.AbilityQueue;
                    CalculateOngoingBonus();
                    RefreshData();

                    if (onCardMove != null)
                        onCardMove.Invoke(card_uid);
                }
            }
        }

        public void AttackTarget(int player_id, string attacker_uid, string target_uid)
        {
            int opponent_id = player_id == 0 ? 1 : 0;
            Player player = game_data.GetPlayer(player_id);
            Player other_player = game_data.GetPlayer(opponent_id);

            if (player == null || other_player == null)
                return;

            if (game_data.current_state != GameState.Play || game_data.current_player != player_id)
                return; //Must attack during main phase

            if (attacker_uid == target_uid)
                return; //Cant attack self

            Card attacker = player.GetCard(attacker_uid);
            Card target = other_player.GetCard(target_uid);

            if (attacker != null && target != null && attacker.player_id == player_id && game_data.current_player == player_id)
            {
                if (!CanPayAction(attacker, false, true, false))
                    return; //No actions

                if (attacker.CanAttackTarget(target))
                {
                    PayAction(attacker, false, true, false);

                    StartAttack(attacker, target);
                }
            }
        }

        public void SelectCard(int player_id, string card_uid)
        {
            if (game_data.current_state == GameState.Selector
                && game_data.selector_player == player_id
                && selector_ability != null)
            {
                Card target = game_data.GetCard(card_uid);
                if (target != null)
                {
                    if (game_data.selector == SelectorType.SelectCardTarget)
                    {
                        if (selector_caster.uid == target.uid)
                            return; // Can't target self

                        if (!selector_caster.CanTarget(target))
                            return;

                        //Ability
                        if (!selector_ability.CanTarget(game_data, selector_caster, target))
                            return; //Can't target that target

                        game_data.selector = SelectorType.None;
                        game_data.current_state = GameState.AbilityQueue; //Go back to ability queue
                        selector_target_selected = target;

                        //Ability
                        TriggerAbilityEffectTarget(selector_caster, selector_ability, target);
                        AfterAbilityEffectResolved(selector_caster, selector_ability);
                    }

                    if (game_data.selector == SelectorType.SelectCardSelector)
                    {
                        //Debug.Log("Selected Card: " + target.card_id);

                        CardData icard = CardData.Get(target.card_id);

                        if (!selector_ability.CanTarget(game_data, selector_caster, target))
                            return; //Can't target that target

                        if (game_data.selector_source == AbilityTarget.SelectInDiscard && !game_data.IsDiscarded(target.uid))
                            return; //Wrong target

                        if (game_data.selector_source == AbilityTarget.SelectInHand && !game_data.IsCardInHand(target.uid))
                            return; //Wrong target

                        if (game_data.selector_source == AbilityTarget.SelectInOpponentHand && !game_data.IsCardInHand(target.uid))
                            return; //Wrong target

                        if (game_data.selector_source == AbilityTarget.SelectInDeck && !game_data.IsCardInDeck(target.uid))
                            return; //Wrong target

                        bool same_player = target.player_id == player_id;
                        bool should_be_same = game_data.selector_source != AbilityTarget.SelectInOpponentHand;
                        if (same_player != should_be_same)
                            return; //Wrong player

                        game_data.selector = SelectorType.None;
                        game_data.current_state = GameState.AbilityQueue; //Go back to ability queue
                        selector_target_selected = target;
                        TriggerAbilityEffectTarget(selector_caster, selector_ability, target);
                        AfterAbilityEffectResolved(selector_caster, selector_ability);
                    }
                }
            }
        }

        public void SelectSlot(int player_id, int slot_x, int slot_side)
        {
            if (game_data.current_state == GameState.Selector
                && game_data.selector == SelectorType.SelectSlot
                && game_data.selector_player == player_id
                && selector_ability != null)
            {
                Slot slot = new Slot(slot_x, slot_side);
                Slot cast_slot = selector_caster.slot;
                if (slot.IsValid() && cast_slot.IsValid())
                {
                    Card slot_card = game_data.GetSlotCard(slot);
                    if (slot_card == null && slot.IsNextToSameRow(cast_slot))
                    {
                        game_data.selector = SelectorType.None;
                        game_data.current_state = GameState.AbilityQueue; //Go back to ability queue
                        AfterAbilityEffectResolved(selector_caster, selector_ability);
                    }
                }
            }
        }

        public void SelectChoice(int player_id, int choice)
        {
            if (game_data.current_state == GameState.Selector
                && game_data.selector == SelectorType.SelectChoice
                && game_data.selector_player == player_id
                && selector_ability != null && selector_caster != null)
            {
                if (choice >= 0 && choice < selector_ability.choices.Length)
                {
                    AbilityData achoice = selector_ability.choices[choice];
                    if (achoice != null && achoice.AreConditionsMet(game_data, selector_caster))
                    {
                        game_data.selector = SelectorType.None;
                        game_data.current_state = GameState.AbilityQueue; //Go back to ability queue
                        TriggerAbilityEffect(selector_caster, achoice);
                        AfterAbilityEffectResolved(selector_caster, selector_ability);
                    }
                }
            }
        }

        public void CancelSelection(int player_id)
        {
            if (game_data.current_state == GameState.Selector && game_data.selector_player == player_id)
            {
                //End selection
                game_data.selector = SelectorType.None;
                game_data.current_state = GameState.Play;
                selector_target_selected = null;
                current_ability = null;
                current_caster = null;

                RefreshData();
            }
        }

        //Go to next step, or next turn
        public void NextStep(int player_id)
        {
            Player player = game_data.GetPlayer(player_id);
            if (player == null)
                return;

            //Selection
            if (game_data.current_state == GameState.Selector && game_data.selector_player == player_id)
            {
                CancelSelection(player_id);
                return;
            }

            if (game_data.current_state == GameState.Play && game_data.current_player == player.player_id)
            {
                StartPhase(GameState.EndTurn);
            }
        }

        public void Resign(int player_id)
        {
            if (game_data.current_state != GameState.GameEnded)
            {
                int winner = player_id == 0 ? 1 : 0;
                EndGame(winner);
            }
        }

        private void EndGame(int winner)
        {
            game_data.current_state = GameState.GameEnded;
            game_data.current_player = winner; //Winner player
            onGameEnd?.Invoke(winner);
			RefreshData();
        }

        //------

        private bool CanPayAction(Card card, bool move, bool attack, bool pose)
        {
            Player player = game_data.GetPlayer(card.player_id);

            if (card.HasStatusEffect(StatusEffect.Paralysed))
                return false;

            if(player.actions > 0)
            {
                return true;
            }

            return false;
        }

        private void PayAction(Card card, bool move, bool attack, bool pose)
        {
            Player player = game_data.GetPlayer(card.player_id);

            if (player.actions > 0)
            {
                player.actions--;
            }
        }

        private void StartAttack(Card attacker, Card defender, bool smart=false) {

            if (attacker == null || defender == null)
                return;

            if (!game_data.IsCardOnBoard(attacker.uid) || !game_data.IsCardOnBoard(defender.uid))
                return;

            if (smart && !attacker.IsStrongerThan(defender))
                return;

            if (!attacker.CanAttackTarget(defender))
                return;

            //Debug.Log("Start Attack : " + attacker.card_id + " -> " + defender.card_id);

            game_data.current_state = GameState.AbilityQueue;
            game_data.attacker_uid = attacker.uid;
            game_data.defender_uid = defender.uid;

            RemoveStatus(attacker, StatusEffect.Stealth);
            TriggerCardAbilityType(attacker, AbilityTrigger.OnFight, false);
            TriggerCardAbilityType(defender, AbilityTrigger.OnFight, false);

            CalculateOngoingBonus();
            RefreshData();
        }

        private IEnumerator ResolveAttack()
        {
            game_data.current_state = GameState.Battle;

            yield return new WaitForSeconds(0.2f);

            Card attacker = game_data.GetCard(game_data.attacker_uid);
            Card defender = game_data.GetCard(game_data.defender_uid);

            //Cancel attack if someone died
            if (attacker == null || defender == null || !game_data.IsCardOnBoard(attacker.uid) || !game_data.IsCardOnBoard(defender.uid))
            {
                CancelAttack();
                yield break;
            }

            //Debug.Log("Resolve Attack : " + attacker.card_id + " -> " + defender.card_id);

            //Recalculate bonus
            CalculateOngoingBonus();

            if (onAttackTarget != null)
                onAttackTarget.Invoke(attacker.uid, defender.uid);

            yield return new WaitForSeconds(0.5f);

            //Count attack damage
            int fatt1 = attacker.GetAttack();
            int fatt2 = defender.GetAttack();

            //Discard cards
            if (fatt1 >= fatt2)
                KillCard(attacker, defender);
            if (fatt2 >= fatt1)
                KillCard(defender, attacker);

            //Recalculate bonus
            CalculateOngoingBonus();

            //Refresh
            RefreshData();

            yield return new WaitForSeconds(0.2f);

            game_data.current_state = GameState.AbilityQueue;
            game_data.attacker_uid = "";
            game_data.defender_uid = "";

            if (onAttackAfter != null)
                onAttackAfter.Invoke(attacker.uid, defender.uid);

            RefreshData();
        }

        private void CancelAttack()
        {
            game_data.current_state = GameState.AbilityQueue;
            game_data.attacker_uid = "";
            game_data.defender_uid = "";

            RefreshData();
        }

        private bool AnyCardHasStatus(int player_id, StatusEffect effect)
        {
            Player player = game_data.GetPlayer(player_id);
            foreach (Card card in player.cards_board)
            {
                if (card.HasStatusEffect(effect))
                    return true;
            }
            return false;
        }

        private void TriggerCardAbilityType(Card caster, AbilityTrigger type, bool instant)
        {
            if (caster == null)
                return;

            CardData icard = CardData.Get(caster.card_id);
            if (icard)
            {
                foreach (AbilityData iability in icard.abilities)
                {
                    if (iability && iability.trigger == type)
                    {
                        if (instant)
                            TriggerCardAbility(caster, iability);
                        else
                            AddToAbilityCastQueue(caster, iability);
                    }
                }
            }
        }

        //Trigger a card ability, may stop to ask for target
        private void TriggerCardAbility(Card caster, AbilityData iability)
        {
            if (caster.HasStatusEffect(StatusEffect.NoAbility))
                return;

            if (!game_data.IsCardOnBoard(caster.uid) && iability.trigger != AbilityTrigger.OnDeath && iability.trigger != AbilityTrigger.None)
                return;

            Player player = game_data.GetPlayer(caster.player_id);
            if (iability.AreConditionsMet(game_data, caster))
            {
                //Debug.Log("Trigger Ability " + iability.id + " : " + caster.card_id);

                current_ability = iability;
                current_caster = caster;
                save_targets.Clear();

                TriggerAbilityEffect(caster, iability);

                if (onAbilityTrigger != null)
                    onAbilityTrigger.Invoke(iability.id, caster.uid);
            }
        }

        //Trigger a card ability effect
        private void TriggerAbilityEffect(Card caster, AbilityData iability)
        {
            if (!iability.AreConditionsMet(game_data, caster))
                return;

            if (iability.target == AbilityTarget.SelectCard)
            {
                //Wait for target
                GoToSelectTarget(caster, iability);
            }
            else if (iability.target == AbilityTarget.SelectInHand
                || iability.target == AbilityTarget.SelectInDeck
                || iability.target == AbilityTarget.SelectInDiscard
                || iability.target == AbilityTarget.SelectInOpponentHand)
            {
                GoToSelectCard(caster, iability);
            }
            else if (iability.target == AbilityTarget.SelectChoice)
            {
                GoToSelectChoice(caster, iability);
            }
            else if (iability.target == AbilityTarget.SelectSlot)
            {
                GoToSelectSlot(caster, iability);
            }
            else if (iability.target == AbilityTarget.None)
            {
                TriggerAbilityEffectTarget(caster, iability, null);
                AfterAbilityEffectResolved(caster, iability);
            }
            else
            {
                int target_count = 0;
                List<Card> targets = GetAbilityTargets(caster, iability);
                foreach (Card target in targets)
                {
                    if (target != null && iability.CanTarget(game_data, caster, target))
                    {
                        TriggerAbilityEffectTarget(caster, iability, target);
                        target_count++;
                    }
                }
                AfterAbilityEffectResolved(caster, iability);
            }
        }

        private void TriggerAbilityEffectTarget(Card caster, AbilityData iability, Card target)
        {
            CardData icard = CardData.Get(caster.card_id);
            Player player = game_data.GetPlayer(caster.player_id);

            //Debug.Log("Trigger Ability Effect " + iability.id + " : " + caster.card_id + " -> " + (target != null ? target.card_id : ""));
            last_target_card = target;

            if (target != null){

                if (iability.effect == AbilityEffect.AddAttack)
                {
                    target.attack += iability.value;
                    target.attack = Mathf.Max(target.attack, 0);
                }

                if (iability.effect == AbilityEffect.AddHP)
                {
                    target.hp += iability.value;
                    target.hp = Mathf.Max(target.hp, 0);
                }

                if (iability.effect == AbilityEffect.AddStatus)
                {
                    foreach (StatusData status in iability.status_effects)
                        AddStatus(status, target, iability.value, iability.duration);
                }

                if (iability.effect == AbilityEffect.Destroy)
                {
                    DiscardCard(target);
                }

                if (iability.effect == AbilityEffect.SendHand)
                {
                    SendToHand(target);
                }

                if (iability.effect == AbilityEffect.SendBottomDeck)
                {
                    SendBottomDeck(target);
                }

                if (iability.effect == AbilityEffect.SendDiscard)
                {
                    SendDiscard(target);
                }

                /*if (iability.effect == AbilityEffect.PlayCard)
                {
                    if (target != null)
                        PlayCard(target, );
                }*/

            }

            //Effects that dont need target
            if (iability.effect == AbilityEffect.SummonCard)
            {
                SummonCard(caster.player_id, iability.spawn_cards);
            }

            if (iability.effect == AbilityEffect.DrawCard)
            {
                int target_player = caster.player_id;
                if (target != null)
                    target_player = target.player_id;
                DrawCard(target_player, iability.value);
            }

            if (iability.effect == AbilityEffect.ShuffleDeck)
            {
                ShuffleDeck(player.cards_deck);
            }

            if (onAbilityEffect != null)
                onAbilityEffect.Invoke(iability.id, caster.uid, target != null ? target.uid : "");
        }

        private void AfterAbilityResolved(Card caster, AbilityData iability)
        {
            Player player = game_data.GetPlayer(caster.player_id);

            //Add to played
            ability_played.Add(iability.id);

            //Pay cost
            if (iability.trigger == AbilityTrigger.Activate)
            {
                if (iability.use_action)
                    PayAction(caster, false, false, false);
            }

            //Recalculate and clear
            CalculateOngoingBonus();
            current_ability = null;
            current_caster = null;

            //Chain ability
            foreach (AbilityData chain_ability in iability.chain_abilities)
            {
                AddToAbilityCastQueue(caster, chain_ability);
            }

            if (onAbilityAfter != null)
                onAbilityAfter.Invoke(iability.id, caster.uid);

            RefreshData();
        }

        private void AfterCardResolved(Card card)
        {
            current_card = null;
            last_played_card = card;
            CalculateOngoingBonus();
            RefreshData();
            CheckForWinner();
        }

        private void AfterAbilityEffectResolved(Card caster, AbilityData iability)
        {
            CalculateOngoingBonus();

            RefreshData();
        }

        private Card GetAbilityRandomCard(List<Card> card_list, Card caster, AbilityData ability)
        {
            List<Card> valid_cards = new List<Card>();
            foreach (Card acard in card_list)
            {
                if (ability.CanTarget(game_data, caster, acard))
                    valid_cards.Add(acard);
            }
            if (valid_cards.Count > 0)
                return valid_cards[Random.Range(0, valid_cards.Count)];
            return null;
        }

        private List<Card> GetAbilityTargets(Card caster, AbilityData iability)
        {

            List<Card> targets = new List<Card>();

            if (iability.target == AbilityTarget.Self)
            {
                targets.Add(caster);
            }

            if (iability.target == AbilityTarget.SelectCard)
            {
                targets.Add(selector_target_selected);
            }

            if (iability.target == AbilityTarget.AllCards)
            {
                foreach (Player player in game_data.players)
                {
                    foreach (Card card in player.cards_board)
                        targets.Add(card);
                }
            }

            if (iability.target == AbilityTarget.AllEnemies)
            {
                Player aplayer = game_data.GetOpponentPlayer(caster.player_id);
                foreach (Card card in aplayer.cards_board)
                    targets.Add(card);
            }

            if (iability.target == AbilityTarget.AllAllies)
            {
                Player aplayer = game_data.GetPlayer(caster.player_id);
                foreach (Card card in aplayer.cards_board)
                    targets.Add(card);
            }

            if (iability.target == AbilityTarget.YourLine)
            {
                foreach (Player aplayer in game_data.players)
                {
                    foreach (Card card in aplayer.cards_board)
                    {
                        if(card.slot.p == caster.player_id)
                            targets.Add(card);
                    }
                }
            }

            if (iability.target == AbilityTarget.OpponentLine)
            {
                foreach (Player aplayer in game_data.players)
                {
                    foreach (Card card in aplayer.cards_board)
                    {
                        if (card.slot.p != caster.player_id)
                            targets.Add(card);
                    }
                }
            }

            if (iability.target == AbilityTarget.LastPlayed)
            {
                if (last_played_card != null)
                    targets.Add(last_played_card);
            }

            if (iability.target == AbilityTarget.LastKill)
            {
                if (last_killed_card != null)
                    targets.Add(last_killed_card);
            }

            if (iability.target == AbilityTarget.LastTarget)
            {
                if (last_target_card != null)
                    targets.Add(last_target_card);
            }

            /*if (iability.target == AbilityTarget.SavedTarget)
            {
                targets.AddRange(save_targets);
            }*/

            /*if (iability.target == AbilityTarget.RandomInDeck)
            {
                Player player = game_data.GetPlayer(caster.player_id);
                Card card = GetAbilityRandomCard(player.cards_deck, caster, iability);
                if (card != null)
                    targets.Add(card);
            }

            if (iability.target == AbilityTarget.RandomInDiscard)
            {
                Player player = game_data.GetPlayer(caster.player_id);
                Card card = GetAbilityRandomCard(player.cards_discard, caster, iability);
                if (card != null)
                    targets.Add(card);
            }*/

            return targets;
        }

        private void AddStatus(StatusData istatus, Card target, int value, int duration)
        {
            if (istatus != null && target != null)
            {
                CardStatus effect = target.GetStatus(istatus.effect);
                if (effect == null)
                {
                    effect = new CardStatus(istatus.effect, value, duration);
                    target.status_effects.Add(effect);
                }
                else
                {
                    effect.value += value;
                    effect.duration += duration;
                    effect.permanent = effect.permanent || duration == 0;
                }
            }
        }

        private void RemoveStatus(Card card, StatusEffect effect)
        {
            if (card != null)
            {
                for (int i = card.status_effects.Count - 1; i >= 0; i--)
                {
                    if (card.status_effects[i].effect == effect)
                        card.status_effects.RemoveAt(i);
                }
            }
        }

        private void GoToSelectCard(Card caster, AbilityData iability)
        {
            game_data.selector = SelectorType.SelectCardSelector;
            game_data.current_state = GameState.Selector;
            game_data.selector_source = iability.target;
            game_data.selector_player = caster.player_id;
            game_data.selector_ability_id = iability.id;
            game_data.selector_caster_uid = caster.uid;
            selector_caster = caster;
            selector_ability = iability;
            RefreshData();
        }

        private void GoToSelectTarget(Card caster, AbilityData iability)
        {
            game_data.selector = SelectorType.SelectCardTarget;
            game_data.current_state = GameState.Selector;
            game_data.selector_source = AbilityTarget.SelectCard;
            game_data.selector_player = caster.player_id;
            game_data.selector_ability_id = iability.id;
            game_data.selector_caster_uid = caster.uid;
            game_data.selector_attacker_uid = "";
            selector_caster = caster;
            selector_ability = iability;
            RefreshData();
        }

        private void GoToSelectSlot(Card caster, AbilityData iability)
        {
            game_data.selector = SelectorType.SelectSlot;
            game_data.current_state = GameState.Selector;
            game_data.selector_source = AbilityTarget.SelectSlot;
            game_data.selector_player = caster.player_id;
            game_data.selector_ability_id = iability.id;
            game_data.selector_caster_uid = caster.uid;
            game_data.selector_attacker_uid = "";
            selector_caster = caster;
            selector_ability = iability;
            RefreshData();
        }

        private void GoToSelectChoice(Card caster, AbilityData iability)
        {
            game_data.selector = SelectorType.SelectChoice;
            game_data.current_state = GameState.Selector;
            game_data.selector_source = iability.target;
            game_data.selector_player = caster.player_id;
            game_data.selector_ability_id = iability.id;
            game_data.selector_caster_uid = caster.uid;
            selector_caster = caster;
            selector_ability = iability;
            RefreshData();
        }

        private void AddToAbilityCastQueue(Card caster, AbilityData ability)
        {
            if (ability != null && caster != null)
            {
                AbilityQueueElement elem = new AbilityQueueElement();
                elem.caster = caster;
                elem.ability = ability;
                ability_cast_queue.Enqueue(elem);
                game_data.current_state = GameState.AbilityQueue;
            }
        }

        private void AddToAttackQueue(Card attacker, Card target, bool smart)
        {
            if (attacker != null && target != null)
            {
                AttackQueueElement elem = new AttackQueueElement();
                elem.attacker = attacker;
                elem.target = target;
                elem.smart = smart;
                attack_queue.Enqueue(elem);
                game_data.current_state = GameState.AbilityQueue;
            }
        }

        private void TriggerNextAbilityQueue()
        {
            if (current_ability != null)
            {
                AfterAbilityResolved(current_caster, current_ability);
            }
            else if (ability_cast_queue.Count > 0)
            {
                AbilityQueueElement elem = ability_cast_queue.Dequeue();
                TriggerCardAbility(elem.caster, elem.ability);
            }
            else if (!string.IsNullOrEmpty(game_data.attacker_uid))
            {
                NetworkTool.StartCoroutine(ResolveAttack());
            }
            else if (attack_queue.Count > 0)
            {
                AttackQueueElement elem = attack_queue.Dequeue();
                StartAttack(elem.attacker, elem.target, elem.smart);
            }
            else if (current_card != null)
            {
                AfterCardResolved(current_card);
            }
            else
            {
                game_data.current_state = GameState.Play;
            }

            RefreshData();
        }

        public bool HasGameEnded()
        {
            return game_data.current_state == GameState.GameEnded;
        }

        public void RefreshData()
        {
            refresh_timer = 0f;
            if (onRefreshAllData != null)
                onRefreshAllData.Invoke();
        }

        //----------------------

        public Game GetGameData()
        {
            return game_data;
        }

    }

}