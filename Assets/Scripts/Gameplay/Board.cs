﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using UnityEngine.Events;

namespace CardGameTemplate
{

    public class Board : MonoBehaviour
    {
        public GameObject energy_select_fx;

        public UnityAction<Card> onCardSpawned;
        public UnityAction<Card> onCardKilled;
        public UnityAction<Card> onCardTakeEnergy;
        public UnityAction<Card> onCardDropEnergy;

        private static Board _instance;

        void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            if (energy_select_fx != null)
                energy_select_fx.SetActive(false);
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            int player_id = GameClient.Get().GetPlayerID();
            Game data = GameClient.Get().GetGameData();

            //--- Battle cards --------

            List<BoardCard> cards = BoardCard.GetAll();

            //Add missing cards
            foreach (Player p in data.players)
            {
                foreach (Card card in p.cards_board)
                {
                    BoardCard bcard = BoardCard.Get(card.uid);
                    if (card != null && bcard == null)
                        SpawnNewCard(card);
                }
            }

            //Vanish removed cards
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                BoardCard card = cards[i];
                if (card && data.GetBoardCard(card.GetCard().uid) == null && !card.IsDead())
                {
                    card.Kill();
                    onCardKilled?.Invoke(card.GetCard());
                }
            }

        }

        private void SpawnNewCard(Card card)
        {
            GameObject card_obj = Instantiate(AssetData.Get().card_prefab, Vector3.zero, Quaternion.identity);
            card_obj.SetActive(true);
            card_obj.GetComponent<BoardCard>().SetCard(card);
            onCardSpawned?.Invoke(card);
        }

        public void CardTakeEnergy(Card card)
        {
            onCardTakeEnergy?.Invoke(card);
        }

        public void CardDropEnergy(Card card)
        {
            onCardDropEnergy?.Invoke(card);
        }

        public static Board Get()
        {
            return _instance;
        }
    }
}