﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum BoardRefType
    {
        None = 0,
        MainArea = 2,
    }

    public class BoardRef : MonoBehaviour
    {
        public BoardRefType type;
        public bool opponent;

        private static List<BoardRef> ref_list = new List<BoardRef>();

        void Awake()
        {
            ref_list.Add(this);
        }

        void OnDestroy()
        {
            ref_list.Remove(this);
        }

        public static BoardRef Get(BoardRefType type, bool opponent)
        {
            foreach (BoardRef bref in ref_list)
            {
                if (bref.type == type && bref.opponent == opponent)
                    return bref;
            }
            return null;
        }
    }
}
