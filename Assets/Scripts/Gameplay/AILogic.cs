﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace CardGameTemplate.AI
{
    
    public class AILogic
    {
        //---------- AI PARAMS -------------

        //How much the AI tries to have card superiority on the board
        private const int card_value = 55;

        //How much the AI tries to kill stuff
        private const int kill_value = 10;

        //Other important things on the board
        private const int hand_card_value = 15;
        private const int attack_card_value = 1;
        private const int hp_card_value = 1;


        //--------------------------

        private int ai_player_id;
        private AIData aidata;

        private int ai_level = 10; //AI level
        private int ai_depth = 10; //This can make the AI very slow if too high or very dumb if too low
        private int ai_depth_wide = 5; //For these first few steps, will consider more options, slow!
        private int ai_depth_very_wide = 2; //For these first few steps, will consider even more options, slow!
        private int max_orders_per_turn = 4;
        
        private NodeState first_node;
        private NodeState best_move;

        private bool running;
        private int nb_calculated;
        private int reached_depth;
        private int heuristic_modifier = 0;

        private System.Random random_gen;

        private Queue<NodeState> node_queued;
        private LinkedList<NodeState> all_nodes;

        public static AILogic CalculateAI(Game game_data, int player_id, int level, int orders_count)
        {
            AILogic job = new AILogic();

            job.aidata = new AIData(game_data, player_id);
            job.aidata.orders_count = orders_count;

            job.node_queued = new Queue<NodeState>();
            job.all_nodes = new LinkedList<NodeState>();
            job.random_gen = new System.Random();

            job.ai_player_id = player_id;
            job.first_node = null;
            job.reached_depth = 0;
            job.nb_calculated = 0;

            job.ai_level = level;
            job.ai_depth = level; //should be from 1 to 10
            job.ai_depth_wide = 1 + level / 2; //should be from 1 to 6
            job.ai_depth_very_wide = Mathf.Max(level - 8, 0); //should be from 0 to 2
            job.max_orders_per_turn = level < 5 ? 3 : 4;

            job.Start();

            return job;
        }

        public void Start()
        {
            running = true;
            Thread ai_thread = new Thread(Execute);
            ai_thread.Start();

            //Execute();
        }

        public void Stop()
        {
            running = false;
        }

        public void Execute()
        {
            first_node = new NodeState(null, ai_player_id, 0);
            first_node.hvalue = CalculateHeuristic(aidata, first_node);
            first_node.ori_hvalue = first_node.hvalue;
            first_node.alpha = int.MinValue;
            first_node.beta = int.MaxValue;
            node_queued.Enqueue(first_node); //First node
            all_nodes.AddLast(first_node);
            heuristic_modifier = GetHeuristicModifier();

            System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

            CalculateNode(aidata, first_node);

            Debug.Log("AI: " + watch.ElapsedMilliseconds + " D" + reached_depth + " N" + nb_calculated);

            best_move = first_node.best_child;
            running = false;

        }

        private void CalculateNode(AIData data, NodeState node)
        {
            PlayerA player = data.GetPlayer(data.current_player);
            List<AIOrder> order_list = new List<AIOrder>();

            if (data.orders_count + 1 < max_orders_per_turn)
            {
                //Play card
                if (node.depth <= ai_depth_wide && player.player_id == ai_player_id)
                {
                    foreach (CardA card in player.cards_hand)
                    {
                        AddOrders(order_list, data, node, AIOrderType.Play, card);
                    }
                }
                else if (player.cards_hand.Count > 0)
                {
                    CardA card = player.cards_hand[random_gen.Next(player.cards_hand.Count)];
                    AddOrders(order_list, data, node, AIOrderType.Play, card);
                }

                //Action on board
                foreach (CardA card in player.cards_board)
                {
                    AddOrders(order_list, data, node, AIOrderType.Move, card);

                    AddOrders(order_list, data, node, AIOrderType.Attack, card);

                    AddOrders(order_list, data, node, AIOrderType.CastAbility, card);
                }
            }

            //End Turn
            if (player.actions == 0 || order_list.Count == 0)
            {
                AIOrder ordert = new AIOrder(AIOrderType.EndTurn);
                order_list.Add(ordert);
            }

            foreach (AIOrder order in order_list)
            {
                if (node.alpha < node.beta)
                {
                    CalculateChildNode(data, node, order);
                }
            }

            nb_calculated++;
            if (node.depth > reached_depth)
                reached_depth = node.depth;
        }

        private void CalculateChildNode(AIData data, NodeState node, AIOrder order)
        {
            if (order.type == AIOrderType.None)
                return;

            int player_id = data.current_player;

            AIData ndata = new AIData(data); //Clone
            DoAIMove(ndata, order, player_id); //Update data
            RefreshOngoing(ndata); //Refresh status

            //Get depth
            int next_depth = node.depth + 1;
            if (order.type == AIOrderType.EndTurn && data.orders_count < max_orders_per_turn)
                next_depth += (max_orders_per_turn - data.orders_count); //Skip depth so all equal

            //Create node
            NodeState child_node = CreateNode(node, order, player_id, next_depth);
            node.childs.Add(child_node);

            //Calculate Quick heuristic for win conditions
            child_node.ori_hvalue = CalculateQuickHeuristic(ndata, child_node);
            child_node.hvalue = child_node.ori_hvalue;

            //Calculate child
            if (!child_node.IsWin() && child_node.depth <= ai_depth)
            {
                CalculateNode(ndata, child_node);
            }
            else
            {
                //End of node, calculate full Heuristic
                child_node.ori_hvalue = CalculateHeuristic(ndata, child_node, child_node.ori_hvalue);
                child_node.hvalue = child_node.ori_hvalue;
            }

            //Update parent
            if (player_id == ai_player_id)
            {
                if (node.best_child == null || child_node.hvalue > node.hvalue)
                {
                    node.best_child = child_node;
                    node.hvalue = child_node.hvalue;
                    node.alpha = Mathf.Max(node.alpha, node.hvalue);
                }
            }
            else
            {
                if (node.best_child == null || child_node.hvalue < node.hvalue)
                {
                    node.best_child = child_node;
                    node.hvalue = child_node.hvalue;
                    node.beta = Mathf.Min(node.beta, node.hvalue);
                }
            }
        }

        private NodeState CreateNode(NodeState parent, AIOrder order, int player_id, int depth)
        {
            NodeState nnode = new NodeState(parent, player_id, depth);
            all_nodes.AddLast(nnode);
            nnode.last_move = order;
            nnode.type = order.type;
            nnode.alpha = parent.alpha;
            nnode.beta = parent.beta;
            return nnode;
        }

        private int CalculateQuickHeuristic(AIData data, NodeState node)
        {
            PlayerA aiplayer = data.GetPlayer(ai_player_id);
            PlayerA oplayer = data.GetOpponent(ai_player_id);

            int score = CalculateWinHeuristic(data, node, aiplayer, oplayer);
            return score;
        }

        private int CalculateHeuristic(AIData data, NodeState node)
        {
            PlayerA aiplayer = data.GetPlayer(ai_player_id);
            PlayerA oplayer = data.GetOpponent(ai_player_id);

            int score = CalculateWinHeuristic(data, node, aiplayer, oplayer);
            return CalculateHeuristic(data, node, score);
        }

        private int CalculateHeuristic(AIData data, NodeState node, int winscore)
        {
            PlayerA aiplayer = data.GetPlayer(ai_player_id);
            PlayerA oplayer = data.GetOpponent(ai_player_id);

            int score = winscore;

            //Board state by artefact
            score += aiplayer.cards_board.Count * card_value;
            score += aiplayer.kill_count * kill_value;


            score += oplayer.cards_board.Count * -card_value;
            score += oplayer.kill_count * -kill_value;

            score += aiplayer.cards_hand.Count * hand_card_value;
            score += oplayer.cards_hand.Count * -hand_card_value;

            foreach (CardA card in aiplayer.cards_board)
            {
                score += card.GetAttack() * attack_card_value;
                score += card.GetHP() * hp_card_value;
            }
            foreach (CardA card in oplayer.cards_board)
            {
                score += card.GetAttack() * -attack_card_value;
                score += card.GetHP() * -hp_card_value;
            }

            if (ai_level < 10)
                score += random_gen.Next(-heuristic_modifier, heuristic_modifier);

            return score;
        }

        private int CalculateWinHeuristic(AIData data, NodeState node, PlayerA aiplayer, PlayerA oplayer)
        {
            int score = 0;

            //Victories
            if (aiplayer.cards_board.Count == 0 && aiplayer.cards_hand.Count == 0 && oplayer.cards_board.Count > 0)
                score = -100000 + node.depth * 1000;
            if (oplayer.cards_board.Count == 0 && oplayer.cards_hand.Count == 0 && aiplayer.cards_board.Count > 0)
                score = 100000 - node.depth * 1000;

            return score;
        }

        private void AddOrders(List<AIOrder> orders, AIData data, NodeState node, AIOrderType type, CardA card)
        {
            PlayerA player = data.GetPlayer(data.current_player);
            PlayerA oplayer = data.GetOpponent(data.current_player);

            if (card.HasStatusEffect(StatusEffect.Paralysed))
                return;

            if (ai_level <= 2 && player.actions == 0)
                return; //No extra action at level 1-2

            if (type == AIOrderType.Play)
            {
                if (player.actions > 0 || player.CanDoPoseAction())
                {
                    List<Slot> slots = new List<Slot>();
                    for (int x = 1; x <= 4; x++)
                    {
                        Slot slot = new Slot(x, data.current_player);
                        if (!data.IsCardOnSlot(slot))
                            slots.Add(slot);
                    }

                    if (node.depth <= ai_depth_wide)
                    {
                        //Check All slots
                        foreach (Slot slot in slots)
                        {
                            AIOrder order = new AIOrder(type);
                            order.card_uid = card.uid;
                            order.card_id = card.card_id;
                            order.slot = slot;
                            orders.Add(order);
                        }
                    }
                    else if (slots.Count > 0)
                    {
                        //Check one at random
                        AIOrder order = new AIOrder(type);
                        order.card_uid = card.uid;
                        order.card_id = card.card_id;
                        order.slot = slots[random_gen.Next(slots.Count)];
                        orders.Add(order);
                    }
                }
            }

            if (type == AIOrderType.Move)
            {
                if (player.actions > 0 || player.CanDoMoveAction(card))
                {
                    List<Slot> slots = new List<Slot>();
                    int pside = card.slot.p == 0 ? 1 : 0;
                    for (int x = 1; x <= 4; x++)
                    {
                        Slot slot = new Slot(x, pside); //Opposite side
                        if (card.CanMoveTo(slot) && !data.IsCardOnSlot(slot))
                            slots.Add(slot);
                    }

                    if (node.depth <= ai_depth_very_wide)
                    {
                        for (int x = 1; x <= 4; x++)
                        {
                            Slot slot = new Slot(x, card.slot.p); //Same side
                            if (card.CanMoveTo(slot) && !data.IsCardOnSlot(slot))
                                slots.Add(slot);
                        }
                    }

                    foreach (Slot slot in slots)
                    {
                        AIOrder order = new AIOrder(type);
                        order.card_uid = card.uid;
                        order.card_id = card.card_id;
                        order.slot = slot;
                        orders.Add(order);
                    }
                }
            }

            if (type == AIOrderType.Attack && ai_level >= 1)
            {
                if (player.actions > 0 || player.CanDoAttackAction(card))
                {
                    List<CardA> targets = new List<CardA>();

                    foreach (CardA acard in oplayer.cards_board)
                    {
                        if (card.CanAttackTarget(acard) && !acard.HasStatusEffect(StatusEffect.Invincibility))
                        {
                            targets.Add(acard);
                        }
                    }

                    foreach (CardA target in targets)
                    {
                        AIOrder order = new AIOrder(type);
                        order.card_uid = card.uid;
                        order.card_id = card.card_id;
                        order.target_uid = target.uid;
                        order.target_id = target.card_id;
                        order.value = 0;
                        orders.Add(order);
                    }
                }
            }

            if (type == AIOrderType.CastAbility && ai_level >= 3)
            {
                if (card.HasStatusEffect(StatusEffect.NoAbility))
                    return;

                foreach (AbilityData ability in card.card_data.abilities)
                {
                    if (ability.trigger == AbilityTrigger.Activate && player.CanDoActivation(card, ability) && AIData.AreConditionsMet(data, ability, card))
                    {
                        List<CardA> targets = new List<CardA>();

                        if (ability.target == AbilityTarget.SelectCard)
                        {
                            foreach (CardA acard in player.cards_board)
                            {
                                if (AIData.AreTargetConditionsMet(data, ability, card, acard))
                                {
                                    targets.Add(acard);
                                }
                            }

                            foreach (CardA acard in oplayer.cards_board)
                            {
                                if (AIData.AreTargetConditionsMet(data, ability, card, acard))
                                {
                                    targets.Add(acard);
                                }
                            }

                            if (node.depth <= ai_depth_wide)
                            {
                                foreach (CardA target in targets)
                                {
                                    AIOrder order = new AIOrder(type);
                                    order.card_uid = card.uid;
                                    order.card_id = card.card_id;
                                    order.target_uid = target.uid;
                                    order.target_id = target.card_id;
                                    order.ability_id = ability.id;
                                    order.value = 0;
                                    orders.Add(order);
                                }

                            }
                            else if (targets.Count > 0)
                            {
                                CardA target = targets[random_gen.Next(targets.Count)];
                                AIOrder order = new AIOrder(type);
                                order.card_uid = card.uid;
                                order.card_id = card.card_id;
                                order.target_uid = target.uid;
                                order.target_id = target.card_id;
                                order.ability_id = ability.id;
                                order.value = 0;
                                orders.Add(order);
                            }
                        }

                        else if (ability.target == AbilityTarget.SelectSlot)
                        {
                            List<Slot> slots = new List<Slot>();
                            for (int x = 1; x <= 4; x++)
                            {
                                Slot slot = new Slot(x, card.slot.p);
                                if (card.slot.IsNextTo(slot) && !data.IsCardOnSlot(slot))
                                    slots.Add(slot);
                            }

                            if (node.depth <= ai_depth_wide)
                            {
                                foreach (Slot slot in slots)
                                {
                                    AIOrder order = new AIOrder(type);
                                    order.card_uid = card.uid;
                                    order.card_id = card.card_id;
                                    order.slot = slot;
                                    order.ability_id = ability.id;
                                    order.value = 0;
                                    orders.Add(order);
                                }
                            }
                            else if (slots.Count > 0)
                            {
                                AIOrder order = new AIOrder(type);
                                order.card_uid = card.uid;
                                order.card_id = card.card_id;
                                order.ability_id = ability.id;
                                order.slot = slots[random_gen.Next(slots.Count)];
                                order.value = 0;
                                orders.Add(order);
                            }
                        }

                        else
                        {
                            AIOrder order = new AIOrder(type);
                            order.card_uid = card.uid;
                            order.card_id = card.card_id;
                            order.ability_id = ability.id;
                            order.value = 0;
                            orders.Add(order);
                        }
                    }
                }
            }

            if (type == AIOrderType.EndTurn)
            {
                AIOrder order = new AIOrder(type);
                orders.Add(order);
            }
        }

        private void DoAIMove(AIData data, AIOrder order, int player_id)
        {
            PlayerA player = data.GetPlayer(player_id);
            data.orders_count++;

            if (order.type == AIOrderType.Play)
            {
                CardA card = player.GetHandCard(order.card_uid);
                player.cards_hand.Remove(card);
                player.cards_board.Add(card);
                card.Cleanse();
                card.slot = order.slot;
                card.attack = card.card_data.attack;

                bool has_action = player.actions > 0;
                if (has_action)
                    player.actions--;

                DoAICastAbility(data, AbilityTrigger.OnPlay, card);
            }

            if (order.type == AIOrderType.Move)
            {
                CardA card = player.GetBoardCard(order.card_uid);
                PlayerA oplayer = data.GetOpponent(player.player_id);
                card.slot = order.slot;

                bool has_action = player.actions > 0;
                if(has_action)
                    player.actions--;
            }

            if (order.type == AIOrderType.Attack)
            {
                CardA card = player.GetBoardCard(order.card_uid);
                PlayerA oplayer = data.GetOpponent(player.player_id);
                CardA target = oplayer.GetBoardCard(order.target_uid);

                //Pay action
                bool has_action = player.actions > 0;
                if (has_action)
                {
                    player.actions--;
                }

                //Double life
                if (target.HasStatusEffect(StatusEffect.DoubleLife))
                {
                    target.RemoveStatus(StatusEffect.DoubleLife);
                    return; //Dont die
                }

                //Kill card
                oplayer.cards_board.Remove(target);
                oplayer.cards_discard.Add(target);
                player.kill_count++;

                //other abilities
                DoAICastAbility(data, AbilityTrigger.OnKill, card);
                DoAICastAbility(data, AbilityTrigger.OnDeath, target);
            }

            if (order.type == AIOrderType.CastAbility)
            {
                CardA card = player.GetBoardCard(order.card_uid);
                PlayerA oplayer = data.GetOpponent(player.player_id);
                CardA target = oplayer.GetBoardCard(order.target_uid);
                Slot starget = order.slot;
                AbilityData iability = AbilityData.Get(order.ability_id);

                bool has_action = player.actions > 0 && iability.use_action;
                if (iability.use_action)
                {
                    player.actions--;
                }

                if(iability.target == AbilityTarget.SelectCard || iability.target == AbilityTarget.SelectInDiscard)
                    DoAICastAbility(data, iability, player, card, target);
                else if (iability.target == AbilityTarget.SelectSlot)
                    DoAICastAbility(data, iability, player, card, starget);
                else
                    DoAICastAbility(data, iability, player, card);
            }

            if (order.type == AIOrderType.EndTurn)
            {
                data.current_player = data.current_player == 0 ? 1 : 0;
                if (data.current_player == data.first_player)
                    data.turn_count++;
                PlayerA nplayer = data.GetPlayer(data.current_player);
                nplayer.actions = 1;
                data.orders_count = 0;

                //Remove conditions
                foreach (PlayerA aplayer in data.players)
                {
                    foreach (CardA card in aplayer.cards_board)
                    {
                        card.Refresh();

                        for (int i = card.status_effects.Count - 1; i >= 0; i--)
                        {
                            if (!card.status_effects[i].permanent)
                            {
                                card.status_effects[i].duration -= 1;
                                if (card.status_effects[i].duration <= 0)
                                    card.status_effects.RemoveAt(i);
                            }
                        }
                    }
                }

                //Start of turn
                foreach (PlayerA aplayer in data.players)
                {
                    foreach (CardA card in aplayer.cards_board)
                    {
                        DoAICastAbility(data, AbilityTrigger.StartOfTurn, card);
                    }
                }
            }
        }

        private void RefreshOngoing(AIData data)
        {
            ClearOngoing(data);
            RefreshOngoingStatus(data);
            RefreshOngoingValues(data);
        }

        private void ClearOngoing(AIData data)
        {
            foreach (PlayerA player in data.players)
            {
                foreach (CardA card in player.cards_board)
                {
                    card.CleanseOngoing();
                }
            }
        }

        private void RefreshOngoingStatus(AIData data)
        {
            //Self abilities
            foreach (PlayerA player in data.players)
            {
                foreach (CardA card in player.cards_board)
                {
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        foreach (AbilityData ability in card.card_data.abilities)
                        {
                            if (ability && ability.trigger == AbilityTrigger.Ongoing && AIData.AreConditionsMet(data, ability, card))
                            {
                                if (ability.target == AbilityTarget.Self && AIData.AreTargetConditionsMet(data, ability, card, card))
                                {
                                    foreach (StatusData istatus in ability.status_effects)
                                    {
                                        if (!card.HasStatusEffect(istatus.effect))
                                        {
                                            CardStatus status = new CardStatus(istatus.effect, ability.value, 0);
                                            card.ongoing_status.Add(status);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Status from other cards abilities
            foreach (PlayerA player in data.players)
            {
                foreach (CardA card in player.cards_board)
                {
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        foreach (PlayerA aplayer in data.players)
                        {
                            foreach (CardA othercard in aplayer.cards_board)
                            {
                                if (!othercard.HasStatusEffect(StatusEffect.NoAbility) && !card.HasStatusEffect(StatusEffect.NoAbility))
                                {
                                    foreach (AbilityData ability in othercard.card_data.abilities)
                                    {
                                        if (ability && ability.trigger == AbilityTrigger.Ongoing && AIData.AreConditionsMet(data, ability, card))
                                        {
                                            if (AIData.AreTargetConditionsMet(data, ability, othercard, card))
                                            {
                                                if (ability.target == AbilityTarget.AllCards
                                                    || (ability.target == AbilityTarget.AllAllies && othercard.player_id == card.player_id)
                                                    || (ability.target == AbilityTarget.AllEnemies && othercard.player_id != card.player_id))
                                                {
                                                    foreach (StatusData istatus in ability.status_effects)
                                                    {
                                                        if (!card.HasStatusEffect(istatus.effect))
                                                        {
                                                            CardStatus status = new CardStatus(istatus.effect, ability.value, 0);
                                                            card.ongoing_status.Add(status);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //Area ongoing effects
                                    if (card != othercard && card.player_id == othercard.player_id)
                                    {
                                        //Protection
                                        if (othercard.HasStatusEffect(StatusEffect.Protection) && !card.HasStatusEffect(StatusEffect.Protection))
                                        {
                                            if (!card.HasStatusEffect(StatusEffect.Protected) && card.slot.IsNextToSameRow(othercard.slot))
                                            {
                                                CardStatus pstatus = new CardStatus(StatusEffect.Protected, 0, 0);
                                                card.ongoing_status.Add(pstatus);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RefreshOngoingValues(AIData data)
        {
            foreach (PlayerA player in data.players)
            {
                foreach (CardA card in player.cards_board)
                {
                    //Power bonus from its own status
                    if (!card.HasStatusEffect(StatusEffect.NoAbility))
                    {
                        //Add ongoing abilities
                        CardData icard = CardData.Get(card.card_id);
                        foreach (AbilityData ability in icard.abilities)
                        {
                            if (ability && ability.trigger == AbilityTrigger.Ongoing && AIData.AreConditionsMet(data, ability, card))
                            {
                                if (AIData.AreTargetConditionsMet(data, ability, card, card))
                                {
                                    List<CardA> targets = GetAbilityTargets(data, card, ability);
                                    foreach (CardA target in targets)
                                        AddOngoingAbilityBonus(ability, target);
                                }
                            }
                        }

                        //Add from status effect
                        foreach (CardStatus cond in card.status_effects)
                            AddOngoingStatusBonus(cond, card);
                        foreach (CardStatus cond in card.ongoing_status)
                            AddOngoingStatusBonus(cond, card);
                    }
                }
            }
        }

        private void AddOngoingAbilityBonus(AbilityData ability, CardA target)
        {
            if (ability == null || target == null)
                return;

            if (ability.effect == AbilityEffect.AddAttack)
                target.attack_ongoing_bonus += ability.value;
            if (ability.effect == AbilityEffect.AddHP)
                target.hp_ongoing_bonus += ability.value;

            target.attack_ongoing_bonus = Mathf.Max(target.attack_ongoing_bonus, 0);
            target.hp_ongoing_bonus = Mathf.Max(target.hp_ongoing_bonus, 0);
        }

        private void AddOngoingStatusBonus(CardStatus status, CardA target)
        {
            StatusData istatus = StatusData.Get(status.effect);
            if (istatus != null)
            {
                if (istatus.effect == StatusEffect.AddAttack)
                    target.attack_ongoing_bonus += status.value;
                if (istatus.effect == StatusEffect.AddHP)
                    target.hp_ongoing_bonus += status.value;

                target.attack_ongoing_bonus = Mathf.Max(target.attack_ongoing_bonus, 0);
                target.hp_ongoing_bonus = Mathf.Max(target.hp_ongoing_bonus, 0);
            }
        }

        private void DoAICastAbility(AIData data, AbilityTrigger trigger, CardA caster)
        {
            PlayerA player = data.GetPlayer(caster.player_id);
            foreach (AbilityData iability in caster.card_data.abilities)
            {
                if (iability.trigger == trigger)
                {
                    DoAICastAbility(data, iability, player, caster);
                }
            }
        }

        private void DoAICastAbility(AIData data, AbilityData iability, PlayerA player, CardA caster)
        {
            if (caster.HasStatusEffect(StatusEffect.NoAbility))
                return;

            List<CardA> targets = GetAbilityTargets(data, caster, iability);
            foreach (CardA target in targets)
            {
                DoAICastAbility(data, iability, player, caster, target);
            }
        }

        private void DoAICastAbility(AIData data, AbilityData iability, PlayerA player, CardA caster, CardA target)
        {
            if (target == null)
                return;

            PlayerA tplayer = data.GetPlayer(target.player_id);

            if (iability.effect == AbilityEffect.SendHand)
            {
                tplayer.cards_board.Remove(target);
                tplayer.cards_hand.Add(target);
            }

            if (iability.effect == AbilityEffect.SendBottomDeck)
            {
                tplayer.cards_board.Remove(target);
                tplayer.cards_hand.Remove(target);
            }

            if (iability.effect == AbilityEffect.SendDiscard)
            {
                tplayer.cards_hand.Remove(target);
            }

            if (iability.effect == AbilityEffect.AddStatus)
            {
                foreach (StatusData istatus in iability.status_effects)
                {
                    CardStatus effect = target.GetStatus(istatus.effect);
                    if (effect == null)
                    {
                        effect = new CardStatus(istatus.effect, iability.value, iability.duration);
                        target.status_effects.Add(effect);
                    }
                    else
                    {
                        effect.value += iability.value;
                        effect.duration += iability.duration;
                        effect.permanent = effect.permanent || iability.duration == 0;
                    }
                }
            }
        }

        private void DoAICastAbility(AIData data, AbilityData iability, PlayerA player, CardA caster, Slot starget)
        {
            if (starget == Slot.None)
                return;


        }

        private List<CardA> GetAbilityTargets(AIData data, CardA caster, AbilityData iability)
        {

            List<CardA> targets = new List<CardA>();

            if (iability.target == AbilityTarget.Self)
            {
                targets.Add(caster);
            }

            if (iability.target == AbilityTarget.AllCards)
            {
                foreach (PlayerA player in data.players)
                {
                    foreach (CardA card in player.cards_board)
                        targets.Add(card);
                }
            }

            if (iability.target == AbilityTarget.AllEnemies)
            {
                PlayerA aplayer = data.GetOpponent(caster.player_id);
                foreach (CardA card in aplayer.cards_board)
                    targets.Add(card);
            }

            if (iability.target == AbilityTarget.AllAllies)
            {
                PlayerA aplayer = data.GetPlayer(caster.player_id);
                foreach (CardA card in aplayer.cards_board)
                    targets.Add(card);
            }

            if (iability.target == AbilityTarget.YourLine)
            {
                foreach (PlayerA aplayer in data.players)
                {
                    foreach (CardA card in aplayer.cards_board)
                    {
                        if (card.slot.p == caster.player_id)
                            targets.Add(card);
                    }
                }
            }

            if (iability.target == AbilityTarget.OpponentLine)
            {
                foreach (PlayerA aplayer in data.players)
                {
                    foreach (CardA card in aplayer.cards_board)
                    {
                        if (card.slot.p != caster.player_id)
                            targets.Add(card);
                    }
                }
            }

            return targets;
        }

        private int GetHeuristicModifier()
        {
            if (ai_level >= 10)
                return 0;
            if (ai_level == 9)
                return 5;
            if (ai_level == 8)
                return 10;
            if (ai_level == 7)
                return 20;
            if (ai_level == 6)
                return 30;
            if (ai_level == 5)
                return 40;
            if (ai_level == 4)
                return 50;
            if (ai_level == 3)
                return 75;
            if (ai_level == 2)
                return 100;
            if (ai_level <= 1)
                return 200;
            return 0;
        }

        public bool IsRunning()
        {
            return running;
        }

        //Return values

        public string GetNodePath()
        {
            return GetNodePath(first_node);
        }

        public string GetNodePath(NodeState node)
        {
            string path = "";
            NodeState current = node;
            AIOrder move;

            while (current != null)
            {
                move = current.last_move;
                if (move != null && (move.type != AIOrderType.EndTurn || current.parent == node))
                    path += current.current_player + " " + move.GetText() + " D:" + current.depth + " H:" + current.hvalue + "\n";
                else if(move == null)
                    path += "First" + " D:" + current.depth + " H:" + current.hvalue + "\n";
                current = current.best_child;
            }
            return path;
        }

        public string FirstLevelMovesList()
        {
            string path = "";
            NodeState node;

            for (int i = 0; i < first_node.childs.Count; i++)
            {
                node = first_node.childs[i];
                path += node.hvalue + " ";
            }

            if (first_node.best_child != null)
            {
                path += "\n";
                for (int i = 0; i < first_node.best_child.childs.Count; i++)
                {
                    node = first_node.best_child.childs[i];
                    path += node.hvalue + " ";
                }
            }

            return path;
        }

        public void ClearMemory()
        {
            aidata = null;
            first_node = null;
            best_move = null;
            node_queued.Clear();

            foreach (NodeState node in all_nodes)
                node.Clear();
            all_nodes.Clear();

            System.GC.Collect(); //Free memory from AI
        }

        public int GetNbNodesCalculated()
        {
            return nb_calculated;
        }

        public int GetNbNodesLeft()
        {
            return node_queued.Count;
        }

        public int GetDepthReached()
        {
            return reached_depth;
        }

        public NodeState GetBest()
        {
            return best_move;
        }

        public NodeState GetFirst()
        {
            return first_node;
        }

        public AIOrder GetBestMove()
        {
            return best_move != null ? best_move.last_move : null;
        }

        public int GetBestMoveValue()
        {
            return best_move != null ? best_move.hvalue : 0;
        }

        public bool IsBestFound()
        {
            return best_move != null;
        }
    }

    public class NodeState
    {
        public int depth;
        public int hvalue; //Heuristic
        public int ori_hvalue; //Heuristic
        public int alpha;
        public int beta;

        public AIOrderType type;
        public AIOrder last_move = null;
        public int current_player;

        public NodeState parent;
        public NodeState best_child = null;
        public List<NodeState> childs = new List<NodeState>();

        public NodeState(NodeState parent, int player_id, int depth)
        {
            this.parent = parent;
            this.current_player = player_id;
            this.depth = depth;
        }

        public bool IsWin()
        {
            return hvalue > 50000 || hvalue < -50000;
        }

        public void Clear()
        {
            last_move = null;
            best_child = null;
            parent = null;
            childs.Clear();
            childs = null;
        }
    }

}
