﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using UnityEngine.Events;
using CardGameTemplate.UI;
using CardGameTemplate.FX;

namespace CardGameTemplate
{

    public class BoardCard : MonoBehaviour
    {
        public SpriteRenderer card_sprite;
        public SpriteRenderer card_glow;
        public SpriteRenderer card_shadow;

        public Text attack;
        public Text power;
        public Text cost;
        public IconValue rarity_icon;

        public Text card_title;
        public Text card_type;
        public Text card_effect;
        public Text card_quote;

        public CanvasGroup status_group;
        public Text status_text;

        public AbilityButton[] buttons;

        public Color glow_ally;
        public Color glow_enemy;
        public float attack_roll_speed = 10f;

        public UnityAction onKill;

        private BoardCardFX card_fx;
        private Canvas canvas;
        private string card_uid = "";

        private Animator animator;

        private bool destroyed = false;
        private bool focus = false;
        private float timer = 0f;
        private float shadow_intensity = 1f;
        private float status_alpha_target = 0f;

        private float attack_current = 0f;
        private float power_current = 0f;

        private bool back_to_hand;
        private Vector3 back_to_hand_target;

        private int prev_power;
        private float shake_timer = 0f;
        private float shake_intensity = 1f;

        private static List<BoardCard> card_list = new List<BoardCard>();

        private Dictionary<StatusEffect, GameObject> status_fx_list = new Dictionary<StatusEffect, GameObject>();

        void Awake()
        {
            card_list.Add(this);
            animator = GetComponent<Animator>();
            card_fx = GetComponent<BoardCardFX>();
            canvas = GetComponentInChildren<Canvas>();
            card_glow.color = new Color(card_glow.color.r, card_glow.color.g, card_glow.color.b, 0f);
            canvas.gameObject.SetActive(false);
            shadow_intensity = card_shadow.color.a;
            status_alpha_target = 0f;

            if (status_group != null)
                status_group.alpha = 0f;
        }

        void OnDestroy()
        {
            card_list.Remove(this);
        }

        private void Start()
        {
            Game data = GameClient.Get().GetGameData();
            Card card = data.GetCard(card_uid);

            CardData icard = CardData.Get(card.card_id);

            transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(-2f, 2f));
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            timer += Time.deltaTime;
            if (timer > 0.15f && !destroyed && !canvas.gameObject.activeSelf)
                canvas.gameObject.SetActive(true);

            PlayerControls controls = PlayerControls.Get();
            Game data = GameClient.Get().GetGameData();
            Player player = GameClient.Get().GetPlayer();
            Card card = data.GetCard(card_uid);
            bool selected = controls.GetSelected() == this;

            CardData cdata = CardData.Get(card.card_id);
            TeamData tdata = TeamData.Get(cdata.team);

            Vector3 targ_pos = GetTargetPos();
            float speed = 12f;

            transform.position = Vector3.MoveTowards(transform.position, targ_pos, speed * Time.deltaTime);

            float target_alpha = IsFocus() || selected ? 1f : 0f;
            if (destroyed || timer < 1f)
                target_alpha = 0f;

            float calpha = Mathf.MoveTowards(card_glow.color.a, target_alpha, 4f * Time.deltaTime);
            Color ccolor = player.player_id == card.player_id ? glow_ally : glow_enemy;
            card_glow.color = new Color(ccolor.r, ccolor.g, ccolor.b, calpha);
            card_shadow.color = new Color(card_shadow.color.r, card_shadow.color.g, card_shadow.color.b, Mathf.Clamp01(timer) * shadow_intensity);
            card_shadow.enabled = !destroyed;

            card_sprite.color = card.HasStatusEffect(StatusEffect.Stealth) ? Color.gray : Color.white;

            float attack_target = card.GetAttack();
            float power_target = card.GetHP();
            attack_current = Mathf.Lerp(attack_current, attack_target, attack_roll_speed * Time.deltaTime);
            power_current = Mathf.Lerp(power_current, power_target, attack_roll_speed * Time.deltaTime);

            attack.text = Mathf.RoundToInt(attack_current).ToString();
            power.text = Mathf.RoundToInt(power_current).ToString();
            cost.text = cdata.cost.ToString();

            //Ability
            foreach (AbilityButton button in buttons)
                button.Hide();

            if (selected && card.player_id == player.player_id && card.CanDoAbilities())
            {
                int index = 0;
                CardData icard = CardData.Get(card.card_id);
                foreach (AbilityData iability in icard.abilities)
                {
                    if (iability && iability.trigger == AbilityTrigger.Activate && iability.AreConditionsMet(data, card) && player.CanDoActivation(card, iability))
                    {
                        if (iability.target != AbilityTarget.Self || iability.AreTargetConditionsMet(data, card, card))
                        {
                            if (index < buttons.Length)
                            {
                                AbilityButton button = buttons[index];
                                button.SetAbility(card, iability);
                            }
                            index++;
                        }
                    }
                }
            }

            //Status stuff
            List<CardStatus> status_all = card.GetAllStatus();
            foreach (CardStatus status in status_all)
            {
                StatusData istatus = StatusData.Get(status.effect);
                if (istatus != null && !string.IsNullOrEmpty(istatus.title) && !status_fx_list.ContainsKey(status.effect) && istatus.status_fx != null)
                {
                    GameObject fx = Instantiate(istatus.status_fx, transform);
                    fx.transform.localPosition = Vector3.zero;
                    status_fx_list[istatus.effect] = fx;
                }
            }

            List<StatusEffect> remove_list = new List<StatusEffect>();
            foreach (KeyValuePair<StatusEffect, GameObject> pair in status_fx_list)
            {
                if (!card.HasStatusEffect(pair.Key))
                {
                    remove_list.Add(pair.Key);
                    Destroy(pair.Value);
                }
            }

            foreach (StatusEffect status in remove_list)
                status_fx_list.Remove(status);

            if (status_group != null)
                status_group.alpha = Mathf.MoveTowards(status_group.alpha, status_alpha_target, 5f * Time.deltaTime);

            if (shake_timer > 0f)
            {
                shake_timer -= Time.deltaTime;
                Vector3 shake_vector = new Vector3(Mathf.Cos(shake_timer * Mathf.PI * 8f) * 0.02f, Mathf.Sin(shake_timer * Mathf.PI * 7f) * 0.02f, 0f);
                transform.position += shake_vector * shake_intensity;
            }
        }

        private Vector3 GetTargetPos()
        {
            PlayerControls controls = PlayerControls.Get();
            Game data = GameClient.Get().GetGameData();
            Player player = GameClient.Get().GetPlayer();
            Card card = data.GetCard(card_uid);

            if (destroyed && back_to_hand && timer > 0.5f)
                return back_to_hand_target;

            BoardSlot slot = BoardSlot.Get(card.slot);
            if (slot != null)
            {
                Vector3 targ_pos = slot.transform.position;
                targ_pos.z = 0f;

                if (controls.GetSelectType() == PlayerSelectType.CardAction && controls.GetSelected() == this && !destroyed)
                {
                    if (player.CanDoAnyAction(card)) // Change condition if we add drag drop abilities
                    {
                        Vector3 source = transform.position;
                        Vector3 dest = PlayerControls.GetMouseWorldPos();
                        Vector3 dir = (dest - source);
                        float magnitude = Mathf.Clamp(dir.magnitude - 0.5f, 0.2f, 5f);
                        targ_pos += magnitude * magnitude * dir.normalized * 0.0005f;
                    }
                }

                return targ_pos;
            }

            return transform.position;
        }

        public void SetCard(Card card)
        {
            this.card_uid = card.uid;

            BoardSlot slot = BoardSlot.Get(card.slot);
            if (slot != null)
                transform.position = new Vector3(slot.transform.position.x, slot.transform.position.y, 0f);

            CardData icard = CardData.Get(card.card_id);
            if (icard)
            {
                card_sprite.sprite = icard.GetSmallImage(card.variant);

                attack.text = card.GetAttack().ToString();
                power.text = card.GetHP().ToString();
                cost.text = icard.cost.ToString();

                card_title.text = icard.title.ToUpper();
                card_effect.text = icard.card_text;
                card_quote.text = icard.card_quote;
                card_type.text = icard.GetTypeText();

                RarityData rdata = RarityData.Get(icard.rarity);
                if (rarity_icon != null)
                {
                    rarity_icon.value = rdata.rank - 1;
                    rarity_icon.Refresh();
                }

                status_alpha_target = 0f;
            }
        }

        public void SetOrder(int order)
        {
            card_sprite.sortingOrder = order;
            canvas.sortingOrder = order + 1;
        }

        public void OnClickAbility(AbilityData iability)
        {
            Game data = GameClient.Get().GetGameData();
            Card card = data.GetCard(card_uid);
            //GameClient.Get().CastCardActionAbility(card, iability);
        }

        public void Kill()
        {
            if (!destroyed)
            {
                destroyed = true;
                timer = 0f;
                status_alpha_target = 0f;
                card_glow.enabled = false;
                card_shadow.enabled = false;
                SetOrder(card_sprite.sortingOrder - 2);
                Destroy(gameObject, 1.3f);

                GameTool.DoAfter(0.8f, () =>
                {
                    canvas.gameObject.SetActive(false);
                });

                Game data = GameClient.Get().GetGameData();
                Card card = data.GetCard(card_uid);
                Player player = data.GetPlayer(card.player_id);
                if (player.HasCard(player.cards_hand, card) || player.HasCard(player.cards_deck, card))
                {
                    back_to_hand = true;
                    back_to_hand_target = player == GameClient.Get().GetPlayer() ? Vector3.down : Vector3.up;
                    back_to_hand_target = back_to_hand_target * 10f;
                }


                if (onKill != null)
                    onKill.Invoke();
            }
        }

        public void Shake(float intensity = 2f, float duration = 0.5f)
        {
            shake_intensity = intensity;
            shake_timer = duration;
        }

        public bool IsDead()
        {
            return destroyed;
        }

        public bool IsFocus()
        {
            return focus;
        }

        public void OnMouseEnter()
        {
            if (TheUI.IsUIOpened())
                return;

            focus = true;

            Game data = GameClient.Get().GetGameData();
            Card card = data.GetCard(card_uid);
            if (card != null && status_text != null && !destroyed)
            {
                status_text.text = "";

                foreach (CardStatus astatus in card.GetAllStatus())
                {
                    StatusData istats = StatusData.Get(astatus.effect);
                    if (istats != null && !string.IsNullOrEmpty(istats.title))
                        status_text.text += istats.title + ", ";
                }

                if (status_text.text.Length > 2)
                    status_text.text = status_text.text.Substring(0, status_text.text.Length - 2);
            }

            bool show_status = status_text != null && status_text.text.Length > 0;
            status_alpha_target = show_status ? 1f : 0f;
        }

        public void OnMouseExit()
        {
            focus = false;
            status_alpha_target = 0f;
        }

        public void OnMouseDown()
        {
            if (TheUI.IsOverUI())
                return;

            Game gdata = GameClient.Get().GetGameData();
            int player_id = GameClient.Get().GetPlayerID();
            if (gdata.current_state == GameState.Play && player_id == gdata.current_player)
            {
                PlayerControls.Get().SelectCard(this);
            }

            if (gdata.current_state == GameState.Selector && gdata.selector == SelectorType.SelectCardTarget && player_id == gdata.selector_player)
            {
                GameClient.Get().SelectCard(GetCard());
            }
        }

        public void OnMouseUp()
        {

        }

        public void OnMouseOver()
        {
            if (Input.GetMouseButtonDown(1))
            {
                Game gdata = GameClient.Get().GetGameData();
                int player_id = GameClient.Get().GetPlayerID();
                if (gdata.current_state == GameState.Play && player_id == gdata.current_player)
                {
                    PlayerControls.Get().SelectCardRight(this);
                }
            }
        }

        public string GetCardUID()
        {
            return card_uid;
        }

        public Card GetCard()
        {
            Game data = GameClient.Get().GetGameData();
            Card card = data.GetCard(card_uid);
            return card;
        }

        public CardData GetCardInfo()
        {
            Card card = GetCard();
            if (card != null)
                return CardData.Get(card.card_id);
            return null;
        }

        public Slot GetSlot()
        {
            return GetCard().slot;
        }

        public BoardCardFX GetCardFX()
        {
            return card_fx;
        }

        public static int GetNbCardsBoardPlayer(int player_id)
        {
            int nb = 0;
            foreach (BoardCard acard in card_list)
            {
                if (acard != null && acard.GetCard().player_id == player_id)
                    nb++;
            }
            return nb;
        }

        public static BoardCard GetNearestPlayer(Vector3 pos, int skip_player_id, BoardCard skip, float range = 2f)
        {
            BoardCard nearest = null;
            float min_dist = range;
            foreach (BoardCard card in card_list)
            {
                float dist = (card.transform.position - pos).magnitude;
                if (dist < min_dist && card != skip && skip_player_id != card.GetCard().player_id)
                {
                    min_dist = dist;
                    nearest = card;
                }
            }
            return nearest;
        }

        public static BoardCard GetNearest(Vector3 pos, BoardCard skip, float range = 2f)
        {
            BoardCard nearest = null;
            float min_dist = range;
            foreach (BoardCard card in card_list)
            {
                float dist = (card.transform.position - pos).magnitude;
                if (dist < min_dist && card != skip)
                {
                    min_dist = dist;
                    nearest = card;
                }
            }
            return nearest;
        }

        public static BoardCard GetFocus()
        {
            foreach (BoardCard card in card_list)
            {
                if (card.IsFocus())
                    return card;
            }
            return null;
        }

        public static BoardCard Get(string uid)
        {
            foreach (BoardCard card in card_list)
            {
                if (card.card_uid == uid)
                    return card;
            }
            return null;
        }

        public static List<BoardCard> GetAll()
        {
            return card_list;
        }
    }
}