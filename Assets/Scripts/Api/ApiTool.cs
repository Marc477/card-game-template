﻿using UnityEngine;
using System;
using System.Text;
using System.Security.Cryptography;
using UnityEngine.Events;

namespace CardGameTemplate
{

    public class ApiTool : MonoBehaviour
    {
        // ----- Convertions ------

        public static T JsonToObject<T>(string json)
        {
            T value = (T)Activator.CreateInstance(typeof(T));
            try
            {
                value = JsonUtility.FromJson<T>(json);
            }
            catch (Exception) { }
            return value;
        }

        public static ListResponse<T> JsonToArray<T>(string json)
        {
            ListResponse<T> list = new ListResponse<T>();
            list.list = new T[0];
            try
            {
                string wrap_json = "{ \"list\": " + json + "}";
                list = JsonUtility.FromJson<ListResponse<T>>(wrap_json);
            }
            catch (Exception) { }
            return list;
        }

        public static string ToJson(object data)
        {
            return JsonUtility.ToJson(data);
        }

        public static int Parse(string int_str, int default_val = 0)
        {
            bool success = int.TryParse(int_str, out int val);
            return success ? val : default_val;
        }

        //-------- Proof creation

        public static string CreateProof(string value, string key, int length = 20)
        {
            string svalue = "";
            int i = 0;
            foreach (char s in value.ToCharArray())
            {
                svalue += Char.ConvertFromUtf32(s + (i % 11)).ToString();
                i++;
            }

            byte[] valueBytes = Encoding.UTF8.GetBytes(svalue);
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);

            HMACSHA512 hash = new HMACSHA512(keyBytes);
            byte[] hashBytes = hash.ComputeHash(valueBytes);

            string hashValue = Convert.ToBase64String(hashBytes);
            var proof = hashValue.Substring(0, length);
            return proof;
        }

        // ---- Checks -----

        public static void GetUserData(string username, UnityAction<UserData> callback)
        {
            ApiManager.Get().SendGetUserData(username, (bool success, UserData user) =>
            {
                if (success)
                {
                    callback(user);
                }
                else
                {
                    callback(null);
                }
            });
        }

        public static bool IsServer()
        {
            return ApiManager.Get().is_server;
        }
    }
}
