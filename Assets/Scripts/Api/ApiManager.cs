﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace CardGameTemplate
{

    public class ApiManager : MonoBehaviour
    {
        public bool is_server = false;

        public const string public_secret = "CGT_0sAg7MNcsdf0mJs";
        public const float refresh_interval = 300f; //5 min

        public UnityAction<bool, string> onLoginAttempt; //Triggered only if attempting to login
        public UnityAction afterLogin; //Triggered once per scene, even if already logged in
        public UnityAction<UserData> afterUserLoad; //Triggered once per scene, even if already logged in

        private static ApiManager _instance;
        private static bool logged_in = false;

        private static string user_id = "";
        private static string username = "";
        private static string access_token = "";
        private static string refresh_token = "";
        private static string api_version = "";

        private static UserData user_data;

        private bool logging_in = false;
        private bool was_logged_in = false;
        private bool triggered_evt = false;
        private bool triggered_evt_user = false;
        private float refresh_timer = 0f;
        private float valid_timer = 0f;

        private Queue<WebRequest> request_queue = new Queue<WebRequest>();
        private float queue_timer = 0f;

        void Awake()
        {
            _instance = this;
            Load();
        }

        private void Start()
        {

        }

        private void Load()
        {
            if (!logged_in && !is_server)
            {
                user_id = PlayerPrefs.GetString("eoa_user_id");
                username = PlayerPrefs.GetString("eoa_username");
                access_token = PlayerPrefs.GetString("eoa_access_token");
                refresh_token = PlayerPrefs.GetString("eoa_refresh_token");
            }
        }

        private void SaveTokens()
        {
            if (logged_in && !is_server)
            {
                PlayerPrefs.SetString("eoa_user_id", user_id);
                PlayerPrefs.SetString("eoa_username", username);
                PlayerPrefs.SetString("eoa_access_token", access_token);
                PlayerPrefs.SetString("eoa_refresh_token", refresh_token);
            }
        }

        private void Update()
        {
            //After login event
            if (logged_in && !triggered_evt)
            {
                triggered_evt = true;
                afterLogin?.Invoke();
            }

            //After login event
            if (logged_in && user_data != null && !triggered_evt_user)
            {
                triggered_evt_user = true;
                afterUserLoad?.Invoke(user_data);
            }

            //Auto refresh login
            refresh_timer += Time.deltaTime;
            if (logged_in && refresh_timer > refresh_interval)
            {
                refresh_timer = 0f;
                RefreshLogin((bool success, LoginResponse res) =>
                {
                    Debug.Log("Login Refresh: " + success);
                });
            }

            //Check if login still valid
            valid_timer += Time.deltaTime;
            if (valid_timer > 30f && logged_in && !logging_in)
            {
                valid_timer = 0f;
                string url = server_url + "/auth/validate";
                SendGetRequest(url, (WebResponse res) =>
                {
                    if (!logging_in)
                    {
                        logged_in = res.success;
                    }
                });
            }

            //Try loggin again if disconnected
            if (valid_timer > 5f && !logged_in && !logging_in && was_logged_in)
            {
                valid_timer = 0f;
                refresh_timer = 0f;
                RefreshLogin((bool success, LoginResponse res) =>
                {
                    Debug.Log("Login Refresh: " + success);
                });
            }
            was_logged_in = was_logged_in || logged_in;

            //Send queued requests
            queue_timer += Time.deltaTime;
            if (request_queue.Count > 0 && queue_timer > 0.2f)
            {
                queue_timer = 0f;
                WebRequest req = request_queue.Dequeue();
                SendRequest(req);
            }
        }

        // ---------- Requests --------------

        public void Register(string email, string user, string password, UnityAction<bool, UserIdResponse> callback)
        {
            string url = server_url + "/users/register";
            RegisterRequest data = new RegisterRequest();
            data.email = email;
            data.username = user;
            data.avatar = "";
            data.password = password;
            data.version = NetworkData.Get().api_version;

            string proof_val = email + "-" + user;
            data.proof = ApiTool.CreateProof(proof_val, public_secret);
            string json = ApiTool.ToJson(data);

            SendRequest(url, WebRequest.METHOD_POST, json, (WebResponse res) =>
            {
                UserIdResponse regist_res = ApiTool.JsonToObject<UserIdResponse>(res.body);
                callback.Invoke(res.success, regist_res);
            });
        }

        public void Login(string user, string password, UnityAction<bool, LoginResponse> callback)
        {
            string url = server_url + "/auth";

            LoginRequest data = new LoginRequest();
            data.password = password;

            Regex phoneregex = new Regex("^[0-9]+$");
            if (username.Contains("@"))
                data.email = user;
            else
                data.username = user;

            string json = ApiTool.ToJson(data);
            logging_in = true;
            SendRequest(url, WebRequest.METHOD_POST, json, (WebResponse res) =>
            {
                LoginResponse login_res = ApiTool.JsonToObject<LoginResponse>(res.body);
                logged_in = res.success;
                logging_in = false;
                if (res.success)
                {
                    user_id = login_res.id;
                    username = login_res.username;
                    access_token = login_res.accessToken;
                    refresh_token = login_res.refreshToken;
                    api_version = login_res.version;
                    SaveTokens();
                }

                if (res.success && !IsVersionValid())
                {
                    res.success = false;
                    login_res.error = "Invalid version, update the app!";
                }

                callback.Invoke(res.success, login_res);
                if (onLoginAttempt != null)
                    onLoginAttempt.Invoke(res.success, login_res.error);
            });
        }

        public void RefreshLogin(UnityAction<bool, LoginResponse> callback)
        {
            string url = server_url + "/auth/refresh";
            AutoLoginRequest data = new AutoLoginRequest();
            data.refreshToken = refresh_token;
            string json = ApiTool.ToJson(data);
            logging_in = true;
            SendRequest(url, WebRequest.METHOD_POST, json, (WebResponse res) =>
            {
                LoginResponse login_res = ApiTool.JsonToObject<LoginResponse>(res.body);
                logged_in = res.success;
                logging_in = false;
                if (res.success)
                {
                    user_id = login_res.id;
                    username = login_res.username;
                    access_token = login_res.accessToken;
                    refresh_token = login_res.refreshToken;
                    api_version = login_res.version;
                    SaveTokens();
                }

                if (res.success && !IsVersionValid())
                {
                    res.success = false;
                    login_res.error = "Invalid version, update the app!";
                }

                callback.Invoke(res.success, login_res);
                if (onLoginAttempt != null)
                    onLoginAttempt.Invoke(res.success, login_res.error);
            });
        }

        public void SendGetUserData(Action<bool, UserData> callback)
        {
            if (!logged_in)
                return;

            string url = server_url + "/users/" + user_id;
            SendRequest(url, WebRequest.METHOD_GET, "", (WebResponse res) =>
            {
                UserData udata = null;
                if (res.success)
                {
                    udata = ApiTool.JsonToObject<UserData>(res.body);
                    user_data = udata;
                }
                callback(res.success, udata);
            });
        }

        public void SendGetUserData(string username, Action<bool, UserData> callback)
        {
            if (!logged_in)
                return;

            string url = server_url + "/users/" + username;
            SendRequest(url, WebRequest.METHOD_GET, "", (WebResponse res) =>
            {
                UserData udata = null;
                if (res.success)
                {
                    udata = ApiTool.JsonToObject<UserData>(res.body);
                }
                callback(res.success, udata);
            });
        }

        public void Logout()
        {
            user_id = "";
            username = "";
            access_token = "";
            refresh_token = "";
            api_version = "";
            user_data = null;
            SaveTokens();
            logged_in = false;
        }

        public void SendGetVersion(Action<bool, string> callback)
        {
            string url = server_url + "/version";
            SendRequest(url, WebRequest.METHOD_GET, "", (WebResponse res) =>
            {
                VersionResponse version_data = ApiTool.JsonToObject<VersionResponse>(res.body);
                callback.Invoke(res.success, version_data.version);
            });
        }

        public void SendGetRequest(string url, Action<WebResponse> callback)
        {
            SendRequest(url, WebRequest.METHOD_GET, "", callback);
        }

        public void SendPostRequest(string url, Action<WebResponse> callback)
        {
            SendRequest(url, WebRequest.METHOD_POST, "", callback);
        }

        public void SendGetJsonRequest(string url, string json_data, Action<WebResponse> callback)
        {
            SendRequest(url, WebRequest.METHOD_GET, json_data, callback);
        }

        public void SendPostJsonRequest(string url, string json_data, Action<WebResponse> callback)
        {
            SendRequest(url, WebRequest.METHOD_POST, json_data, callback);
        }

        public void SendRequest(string url, string method, string json_data, Action<WebResponse> callback)
        {
            WebRequest request = WebRequest.CreateRequest(url, method, json_data, access_token, callback);
            request_queue.Enqueue(request);
        }

        public void SendRequest(WebRequest req)
        {
            StartCoroutine(SendRequestRoutine(req));
        }

        public void SendUploadRequest(string url, string path, byte[] data, Action<WebResponse> callback)
        {
            UnityWebRequest request = WebRequest.CreateImageUploadForm(url, path, data, access_token);
            StartCoroutine(SendRequestRoutine(request, callback));
        }

        private IEnumerator SendRequestRoutine(WebRequest req)
        {
            UnityWebRequest request = WebRequest.Create(req, 5);

            yield return request.SendWebRequest();

            WebResponse res = WebRequest.GetResponse(request);

            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
                DisplayError(request, res);
			
			request.Dispose();
			
            if (req.callback != null)
                req.callback.Invoke(res);
        }

        private IEnumerator SendRequestRoutine(UnityWebRequest request, Action<WebResponse> callback)
        {
            yield return request.SendWebRequest();

            WebResponse res = WebRequest.GetResponse(request);

            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
                DisplayError(request, res);
			
			request.Dispose();

            if (callback != null)
                callback.Invoke(res);
        }

        private void DisplayError(UnityWebRequest request, WebResponse res)
        {
            ErrorResponse err = ApiTool.JsonToObject<ErrorResponse>(res.body);
            Debug.Log(request.url + " " + request.error + " " + err.error);
        }

        // ---------- Getters --------------

        public string GetUserID()
        {
            return user_id;
        }

        public string GetUsername()
        {
            return username;
        }

        public string GetAvatar()
        {
            if (user_data != null)
                return user_data.avatar;
            return "";
        }

        public UserData GetUserData()
        {
            return user_data;
        }

        public string GetAccessToken()
        {
            return access_token;
        }

        public string GetApiVersion()
        {
            return api_version;
        }

        public bool IsVersionValid()
        {
            return api_version == version;
        }

        public bool IsLoggedIn()
        {
            return logged_in;
        }

        public bool IsLoggingIn()
        {
            return logging_in;
        }

        public static string server_url
        {
            get { return "https://" + NetworkData.Get().api_url; }
        }

        public static string version
        {
            get { return NetworkData.Get().api_version; }
        }

        public static ApiManager Get()
        {
            return _instance;
        }
    }
}
