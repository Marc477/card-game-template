﻿using System;
using UnityEngine;

namespace CardGameTemplate
{

    //--------- Requests -----------

    [Serializable]
    public struct LoginRequest
    {
        public string email;
        public string username;
        public string password;
    }

    [Serializable]
    public struct AutoLoginRequest
    {
        public string refreshToken;
    }

    [Serializable]
    public struct RegisterRequest
    {
        public string email;
        public string username;
        public string proof;
        public string password;
        public string avatar;
        public string version;
    }

    [Serializable]
    public struct ContactRequest
    {
        public string subject;
        public string msg;
    }

    [Serializable]
    public class EditUserRequest
    {
        public string username;
        public string email;
        public string firstName;
        public string lastName;
        public string avatar;
    }

    [Serializable]
    public class EditPasswordRequest
    {
        public string password;
        public string password_previous;
    }

    [Serializable]
    public class AddMatchRequest
    {
        public string tid;
        public string[] players;
        public string[] decks;
        public string mode;
        public string tournament;
        public bool ranked;
        public int gems;
    }

    [Serializable]
    public class CompleteMatchRequest
    {
        public string tid;
        public string winner;
    }


    [Serializable]
    public class MarketAddResquest
    {
        public string username;
        public string card;
        public int price;
        public int quantity;
    }

    [Serializable]
    public class MarketRemoveResquest
    {
        public string username;
        public string card;
    }

    [Serializable]
    public class MarketTradeResquest
    {
        public string username;
        public string seller;
        public string card;
        public int quantity;
    }

    [Serializable]
    public class RewardGainRequest
    {
        public string reward;
        public string choice;
    }

    [Serializable]
    public class TournamentCreateRequest
    {
        public string organizer;
        public string title;
        public string desc;
        public string mode;
        public int speed;
        public int gems;
        public int prestige;
        public int cost;
        public int elo_min;
        public int elo_max;
    }

    [Serializable]
    public class TournamentJoinRequest
    {
        public string tournament;
        public string username;
        public string deck;
        public int gems;
    }

    //--------- Response -----------

    [Serializable]
    public struct VersionResponse
    {
        public string version;
    }

    [Serializable]
    public struct ErrorResponse
    {
        public string error;
    }

    [Serializable]
    public struct IdResponse
    {
        public string id;
    }

    [Serializable]
    public struct LoginResponse
    {
        public string id;
        public string username;
        public string refreshToken;
        public string accessToken;
        public string version;
        public string error;
    }

    [Serializable]
    public struct UserIdResponse
    {
        public string id;
        public string username;
        public string error;
    }

    [Serializable]
    public struct MatchResponse
    {
        public string tid;
        public string[] players;
        public DateTime start;
        public DateTime end;
        public string winner;
        public bool completed;
        public MatchDataResponse[] udata;
    }

    [Serializable]
    public struct MatchDataResponse
    {
        public string username;
        public int rank;
        public DeckData deck;
        public RewardResponse reward;
    }

    [Serializable]
    public struct RewardResponse
    {
        public string tid;
        public int credits;
        public int rank;
        public int xp;
        public string[] cards;
        public string[] decks;
    }

    [Serializable]
    public struct MarketResponse
    {
        public string seller;
        public string card;
        public int price;
        public int quantity;
    }

    [Serializable]
    public struct FriendListResponse
    {
        public string username;
        public string serverTime;
        public FriendResponse[] friends;
    }

    [Serializable]
    public struct FriendResponse
    {
        public string username;
        public string avatar;
        public int rank;
        public int xp;
        public string lastLoginTime;
    }

    [Serializable]
    public class TournamentResponse
    {
        public string tid;
        public string title;
        public string desc;
        public string organizer;
        public string mode;
        public int gems;
        public int speed;
        public int prestige;
        public int cost;
        public string start;
        public string end;
        public int elo_min;
        public int elo_max;
        public int matches;
        public bool completed;
        public int online_users;
        public string[] cards;
        public string[] users;
        public TournamentDataResponse[] udata;
    }

    [Serializable]
    public struct TournamentDataResponse
    {
        public string username;
        public int rank;
        public int matches;
        public int victories;
        public int defeats;
        public int gems;
        public UserDeckData deck;
    }

    [Serializable]
    public class ListResponse<T>
    {
        public T[] list;
        public string error;
    }
}
