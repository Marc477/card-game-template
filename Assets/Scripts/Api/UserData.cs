﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [System.Serializable]
    public class UserData
    {
        public string id;
        public string username;
        public string email;
        public string firstName;
        public string lastName;
        public string avatar;
        public int permissionLevel;

        public int credits;
        public int xp;
        public int rank;

        public int matches;
        public int victories;
        public int defeats;

        public UserCardData[] cards;
        public UserDeckData[] decks;
        public string[] rewards;
        public string[] avatars;
        public string[] cardbacks;
        public string[] friends;
        public string[] tournaments;

        public UserDeckData GetDeck(string tid)
        {
            foreach (UserDeckData deck in decks)
            {
                if (deck.tid == tid)
                    return deck;
            }
            return null;
        }

        public UserCardData GetCard(string tid)
        {
            foreach (UserCardData card in cards)
            {
                if (card.tid == tid)
                    return card;
            }
            return null;
        }

        public int GetCardQuantity(string tid)
        {
            foreach (UserCardData card in cards)
            {
                if (card.tid == tid)
                    return card.quantity;
            }
            return 0;
        }

        public int CountUniqueCards()
        {
            HashSet<string> unique_cards = new HashSet<string>();
            foreach (UserCardData card in cards)
            {
                string card_id = UserCardData.GetCardId(card.tid);
                if (!unique_cards.Contains(card_id))
                    unique_cards.Add(card_id);
            }
            return unique_cards.Count;
        }

        public int CountCardType(CardVariant variant)
        {
            int value = 0;
            foreach (UserCardData card in cards)
            {
                if (UserCardData.GetCardVariant(card.tid) == variant)
                    value += 1;
            }
            return value;
        }

        public bool HasDeckCards(UserDeckData deck)
        {
            foreach (string card_tid in deck.cards)
            {
                if (GetCardQuantity(card_tid) <= 0)
                    return false;
            }
            return true;
        }

        public bool IsDeckValid(UserDeckData deck)
        {
            return HasDeckCards(deck) && deck.IsValid();
        }

        public bool IsDeckValidNuclear(UserDeckData deck, int max_gems)
        {
            return HasDeckCards(deck) && deck.IsValidNuclear(max_gems);
        }

        public bool HasReward(string reward_id)
        {
            foreach (string reward in rewards)
            {
                if (reward == reward_id)
                    return true;
            }
            return false;
        }

        public string GetCreditsString()
        {
            return credits.ToString();
        }

        public bool HasFriend(string username)
        {
            List<string> flist = new List<string>(friends);
            return flist.Contains(username);
        }

        public void AddFriend(string username)
        {
            List<string> flist = new List<string>(friends);
            if (!flist.Contains(username))
                flist.Add(username);
            friends = flist.ToArray();
        }

        public void RemoveFriend(string username)
        {
            List<string> flist = new List<string>(friends);
            if (flist.Contains(username))
                flist.Remove(username);
            friends = flist.ToArray();
        }
    }

    [System.Serializable]
    public class UserCardData
    {
        public string tid;
        public int quantity;

        public static string GetTid(string card_id, CardVariant variant)
        {
            if (variant == CardVariant.Foil)
                return card_id + "_foil";
            return card_id;
        }

        public static string GetCardId(string tid)
        {
            if (tid.EndsWith("_nft_foil"))
                return tid.Replace("_nft_foil", "");
            if (tid.EndsWith("_nft"))
                return tid.Replace("_nft", "");
            if (tid.EndsWith("_foil"))
                return tid.Replace("_foil", "");
            return tid;
        }

        public static CardVariant GetCardVariant(string tid)
        {
            if (tid.EndsWith("_foil"))
                return CardVariant.Foil;
            return CardVariant.Normal;
        }

        public static bool IsFoil(string tid)
        {
            CardVariant variant = GetCardVariant(tid);
            return variant == CardVariant.Foil;
        }
    }

    [System.Serializable]
    public class UserDeckData
    {
        public string tid;
        public string title;
        public string artefact;
        public string[] cards;

        public int GetCost()
        {
            int cost = 0;
            foreach (string tid in cards)
            {
                CardData acard = CardData.Get(UserCardData.GetCardId(tid));
                if (acard != null)
                    cost += acard.cost;
            }
            return cost;
        }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(tid) && !string.IsNullOrWhiteSpace(title) && !string.IsNullOrEmpty(artefact) && cards.Length == 8;
        }

        public bool IsValidNuclear(int max_gems)
        {
            return IsValid() && GetCost() <= max_gems;
        }

        public bool IsOverGemLimit(int max_gems)
        {
            return GetCost() > max_gems;
        }
    }

    [System.Serializable]
    public class UserChallengesData
    {
        public string username;
        public string time;
        public string game;
        public bool accepted;
    }
}

