﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGameTemplate.FX
{

    public class IconFadeFX : MonoBehaviour
    {
        private Image image;
        private bool visible;

        void Start()
        {
            image = GetComponent<Image>();
            visible = false;
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
        }

        void Update()
        {
            float alpha = image.color.a;
            float talpha = visible ? 1f : 0f;
            alpha = Mathf.MoveTowards(alpha, talpha, 5f * Time.deltaTime);
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
        }

        public void Show()
        {
            visible = true;
        }

        public void Hide()
        {
            visible = false;
        }

        public void SetVisible(bool visi)
        {
            if (visi != visible)
            {
                if (visi)
                    Show();
                else
                    Hide();
            }
        }

        public bool IsVisible()
        {
            return visible;
        }
    }
}
