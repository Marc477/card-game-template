﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate.FX
{

    public class TargetFX : MonoBehaviour
    {
        public float move_speed = 20f;
        public GameObject target;

        void Start()
        {

        }

        void Update()
        {
            if (target == null)
            {
                Destroy(gameObject);
                return;
            }

            Vector3 dir = target.transform.position - transform.position;
            if (dir.magnitude < 0.1f)
                Destroy(gameObject);

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, move_speed * Time.deltaTime);

        }
    }
}
