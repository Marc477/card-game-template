﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.UI;

namespace CardGameTemplate.FX
{

    public class DamageFX : MonoBehaviour
    {
        public FontSprite sprite_value;
        public Text text_value;

        void Start()
        {

        }

        void Update()
        {

        }

        public void SetValue(int value)
        {
            if (sprite_value != null)
                sprite_value.value = value;
            if (text_value != null)
                text_value.text = value.ToString();
        }

        public void SetValue(string value)
        {
            if (text_value != null)
                text_value.text = value;
        }
    }
}