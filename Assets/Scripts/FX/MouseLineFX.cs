﻿using CardGameTemplate.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate;

namespace CardGameTemplate.FX
{

    public class MouseLineFX : MonoBehaviour
    {
        public GameObject dot_template;
        public float dot_spacing = 0.2f;

        private List<GameObject> dot_list = new List<GameObject>();
        private List<Vector3> points = new List<Vector3>();

        void Start()
        {
            dot_template.SetActive(false);
        }

        void Update()
        {
            RefreshLine();
            RefreshRender();
        }

        private void RefreshLine()
        {
            points.Clear();

            PlayerControls controls = PlayerControls.Get();

            if (controls.GetSelectType() == PlayerSelectType.CardAction)
            {
                BoardCard bcard = controls.GetSelected();
                Card card = bcard.GetCard();
                Player player = GameClient.Get().GetPlayer();

                if (player.CanDoAnyAction(card)) // Change condition if we add drag drop abilities
                {
                    Vector3 source = bcard.transform.position;
                    Vector3 dest = MouseToWorld(Input.mousePosition);
                    Vector3 dir = (dest - source).normalized;
                    float dist = (dest - source).magnitude;

                    float value = 0f;
                    while (value < dist)
                    {
                        Vector3 pos = source + dir * value;
                        points.Add(pos);

                        value += dot_spacing;
                    }
                }
            }
        }

        private void RefreshRender()
        {
            while (dot_list.Count < points.Count)
            {
                AddDot();
            }

            int index = 0;
            foreach (GameObject dot in dot_list)
            {
                bool active = false;
                if (index < points.Count)
                {
                    Vector3 pos = points[index];
                    dot.transform.position = pos;
                    active = true;
                }

                if (dot.activeSelf != active)
                    dot.SetActive(active);

                index++;
            }
        }

        public void AddDot()
        {
            GameObject dot = Instantiate(dot_template, transform);
            dot.SetActive(true);
            dot_list.Add(dot);
        }

        public Vector3 MouseToWorld(Vector3 mouse_pos)
        {
            Vector3 wpos = Camera.main.ScreenToWorldPoint(mouse_pos);
            wpos.z = 0f;
            return wpos;
        }
    }
}
