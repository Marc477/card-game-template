﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapFX : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset = Vector3.zero;

    void Start()
    {

    }

    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        transform.position = target.transform.position + offset;
    }
}
