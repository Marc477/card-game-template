﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate.UI;

namespace CardGameTemplate.FX
{

    public class BoardFX : MonoBehaviour
    {
        private Dictionary<string, GameObject> items_fx = new Dictionary<string, GameObject>();

        void Start()
        {
            GameClient.Get().onCardPlayed += OnPlayCard;
            GameClient.Get().onAbilityTrigger += OnAbility;
        }

        private void OnDestroy()
        {
            GameClient.Get().onAbilityTrigger -= OnAbility;
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            int player_id = GameClient.Get().GetPlayerID();
            Game data = GameClient.Get().GetGameData();

        }
		
		void OnPlayCard(string card_uid)
        {
            Game gdata = GameClient.Get().GetGameData();
            Card card = gdata.GetCard(card_uid);
            if (card != null)
            {
                CardData icard = CardData.Get(card.card_id);
                if (icard.type == CardType.Spell)
                {
                    GameObject obj = DoFX(AssetData.Get().play_card_fx, AssetData.Get().play_card_fx.transform.position);
                    CardUI ui = obj.GetComponentInChildren<CardUI>();
                    ui.SetCard(icard, card.variant, card);

                }
            }
        }

        void OnAbility(string ability_id, string caster_uid)
        {
            AbilityData iability = AbilityData.Get(ability_id);
            if (iability != null)
            {
                if (iability.board_fx != null)
                    Instantiate(iability.board_fx, Vector3.zero, Quaternion.identity);
            }
        }

        private GameObject GetItemFX(string uid)
        {
            if (items_fx.ContainsKey(uid))
                return items_fx[uid];
            return null;
        }
		
		public static GameObject DoFX(GameObject fx_prefab, Vector3 pos, float duration = 5f)
        {
            if (fx_prefab != null)
            {
                GameObject fx = Instantiate(fx_prefab, pos, fx_prefab.transform.rotation);
                Destroy(fx, duration);
                return fx;
            }
            return null;
        }
    }
}