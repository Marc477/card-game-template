﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate.Client;
using UnityEngine.Events;
using CardGameTemplate;

namespace CardGameTemplate.FX
{

    public class BoardCardFX : MonoBehaviour
    {
        public Material kill_mat;
        public string kill_mat_fade = "Fade";

        private BoardCard bcard;

        void Awake()
        {
            bcard = GetComponent<BoardCard>();
            bcard.onKill += OnKill;

            GameClient.Get().onAttackTarget += OnAttack;
            GameClient.Get().onAbilityEffect += OnAbilityEffect;
            GameClient.Get().onAbilityTrigger += OnAbilityCast;
            GameClient.Get().onAbilityAfter += OnAbilityAfter;
        }

        void Start()
        {
            OnSpawn();
        }

        private void OnDestroy()
        {
            GameClient.Get().onAttackTarget -= OnAttack;
            GameClient.Get().onAbilityTrigger -= OnAbilityCast;
            GameClient.Get().onAbilityEffect -= OnAbilityEffect;
        }

        void Update()
        {
            if (!GameClient.Get().IsConnected())
                return;

            //Attack result
            bool win_active = false;
            bool die_active = false;
            BoardCard selected = PlayerControls.Get().GetSelected();
            Card attacker = selected?.GetCard();
            Card target = bcard?.GetCard();
            if (attacker != null && attacker.uid != target.uid && attacker.player_id != target.player_id && attacker.CanAttackTarget(target))
            {
                int value1 = attacker.GetAttack();
                int value2 = target.GetAttack();
                win_active = value1 > value2;
                die_active = value1 <= value2;
            }
        }

        private void OnSpawn()
        {
            FadeSetVal(bcard.card_sprite, 0f);
            FadeKill(bcard.card_sprite, 1f, 0.5f);

            CardData icard = bcard.GetCardInfo();
            TheCamera.Get().Shake(0.2f * icard.shake_multiplier, 0.5f);

            AudioClip audio = icard?.spawn_audio != null ? icard.spawn_audio : AssetData.Get().card_spawn_audio;
            AudioSystem.Get().PlaySFX("card_fx", audio);

            GameObject spawn_fx = icard.spawn_fx != null ? icard.spawn_fx : AssetData.Get().card_spawn_fx;
            if (spawn_fx != null)
                Instantiate(spawn_fx, transform.position, Quaternion.identity);
        }

        private void OnKill()
        {
            StartCoroutine(KillRoutine());
        }

        private IEnumerator KillRoutine()
        {
            yield return new WaitForSeconds(0.5f);

            CardData icard = bcard.GetCardInfo();

            GameObject death_fx = icard.death_fx != null ? icard.death_fx : AssetData.Get().card_destroy_fx;
            if (death_fx != null)
                Instantiate(death_fx, transform.position, Quaternion.identity);

            AudioClip audio = icard?.death_audio != null ? icard.death_audio : AssetData.Get().card_destroy_audio;
            AudioSystem.Get().PlaySFX("card_fx", audio);

            FadeKill(bcard.card_sprite, 0f, 0.5f);

            TheCamera.Get().Shake();
        }
		
		private void FadeSetVal(SpriteRenderer render, float val)
        {
            render.material = kill_mat;
            render.material.SetFloat(kill_mat_fade, val);
        }

        private void FadeKill(SpriteRenderer render, float val, float duration)
        {
            AnimMatFX anim = AnimMatFX.Create(render.gameObject, render.material);
            anim.SetFloat(kill_mat_fade, val, duration);
        }
		
		private void FadeSetVal(Image render, float val)
        {
            render.material = kill_mat;
            render.material.SetFloat(kill_mat_fade, val);
        }

        private void FadeKill(Image render, float val, float duration)
        {
            AnimMatFX anim = AnimMatFX.Create(render.gameObject, render.material);
			anim.SetFloat(kill_mat_fade, val, duration);
        }

        private void OnAttack(string attacker_uid, string target_uid)
        {
            if (bcard.GetCard().uid == attacker_uid || bcard.GetCard().uid == target_uid)
            {
                Game gdata = GameClient.Get().GetGameData();
                Card attacker = gdata.GetCard(attacker_uid);
                Card target = gdata.GetCard(target_uid);

                int value = bcard.GetCard().GetAttack();
                GameObject fx = Instantiate(AssetData.Get().damage_fx, transform.position, Quaternion.identity);
                fx.GetComponent<DamageFX>().SetValue(value);
                Destroy(fx.gameObject, 2f);
            }

            if (bcard.GetCard().uid == attacker_uid)
            {

                BoardCard target = BoardCard.Get(target_uid);
                if (target != null)
                {
                    ChargeInto(target);

                    CardData icard = bcard.GetCardInfo();
                    AudioClip audio = icard?.attack_audio != null ? icard.attack_audio : AssetData.Get().card_attack_audio;
                    AudioSystem.Get().PlaySFX("card_attack", audio);
                    float shake_mult = icard.shake_multiplier;

                    GameTool.DoAfter(0.3f, () =>
                    {
                        TheCamera.Get().Shake(0.2f * shake_mult, 0.5f);
                    });
                }

            }

            if (bcard.GetCard().uid == target_uid)
            {
                GameTool.DoAfter(0.5f, () =>
                {
                    CardData icard = bcard.GetCardInfo();
                    AudioClip audio = icard?.defend_audio != null ? icard.defend_audio : AssetData.Get().card_defend_audio;
                    AudioSystem.Get().PlaySFX("card_defend", audio);
                });
            }
        }

        private void ChargeInto(BoardCard target)
        {
            if (target != null)
            {
                int current_order = bcard.card_sprite.sortingOrder;
                Vector3 dir = target.transform.position - transform.position;
                Vector3 target_pos = target.transform.position - dir.normalized * 1f;
                Vector3 current_pos = transform.position;
                bcard.SetOrder(current_order + 2);

                AnimFX anim = AnimFX.Create(gameObject);
                anim.MoveTo(current_pos - dir.normalized * 0.5f, 0.3f);
                anim.MoveTo(target.transform.position, 0.1f);
                anim.MoveTo(current_pos, 0.3f);
                anim.Callback(0f, () =>
                {
                    if (bcard != null)
                        bcard.SetOrder(current_order);
                });
            }
        }

        private void OnAbilityCast(string ability_id, string caster_uid)
        {
            AbilityData iability = AbilityData.Get(ability_id);
            if (iability != null)
            {
                if (caster_uid == bcard.GetCardUID())
                {
                    if (iability.caster_fx != null)
                        Instantiate(iability.caster_fx, bcard.transform.position, Quaternion.identity);

                    AudioSystem.Get().PlaySFX("fx", iability.cast_audio);
                }
            }
        }

        private void OnAbilityAfter(string ability_id, string caster_uid)
        {
            AbilityData iability = AbilityData.Get(ability_id);
            if (iability != null)
            {
                if (caster_uid == bcard.GetCardUID())
                {

                }
            }
        }

        private void OnAbilityEffect(string ability_id, string caster_uid, string target_uid)
        {
            AbilityData iability = AbilityData.Get(ability_id);
            if (iability != null)
            {
                if (target_uid == bcard.GetCardUID())
                {
                    if (iability.target_fx != null)
                        Instantiate(iability.target_fx, bcard.transform.position, Quaternion.identity);

                    AudioSystem.Get().PlaySFX("fx", iability.target_audio);

                }

                if (caster_uid == bcard.GetCardUID())
                {
                    if (iability.effect == AbilityEffect.SendHand && iability.target == AbilityTarget.SelectCard)
                    {
                        BoardCard target = BoardCard.Get(target_uid);
                        ChargeInto(target);
                    }
                }
            }
        }

        public static void DoFX(GameObject fx_prefab, Vector3 pos)
        {
            if (fx_prefab != null)
            {
                GameObject fx = Instantiate(fx_prefab, pos, fx_prefab.transform.rotation);
                Destroy(fx, 5f);
            }
        }

        public static void DoSnapFX(GameObject fx_prefab, GameObject snap_target)
        {
            if (fx_prefab != null && snap_target != null)
            {
                GameObject fx = Instantiate(fx_prefab, snap_target.transform.position + snap_target.transform.up * 2f, fx_prefab.transform.rotation);
                SnapFX snap = fx.AddComponent<SnapFX>();
                snap.target = snap_target;
                snap.offset = snap_target.transform.up * 2f;
                Destroy(fx, 5f);
            }
        }
    }
}
