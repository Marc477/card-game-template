﻿using CardGameTemplate.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate;

namespace CardGameTemplate.FX
{

    public class MouseLineTopFX : MonoBehaviour
    {
        public GameObject fx;

        void Start()
        {

        }

        void Update()
        {
            PlayerControls controls = PlayerControls.Get();
            bool visible = false;
            if (controls.GetSelectType() == PlayerSelectType.CardAction)
            {
                BoardCard bcard = controls.GetSelected();
                Card card = bcard.GetCard();
                Player player = GameClient.Get().GetPlayer();

                if (player.CanDoAnyAction(card)) // Change condition if we add drag drop abilities
                {
                    visible = true;
                }
            }

            if (fx.activeSelf != visible)
                fx.SetActive(visible);

            if (visible)
            {
                Vector3 dest = MouseToWorld(Input.mousePosition);
                transform.position = dest;
            }
        }

        public Vector3 MouseToWorld(Vector3 mouse_pos)
        {
            Vector3 wpos = Camera.main.ScreenToWorldPoint(mouse_pos);
            wpos.z = 0f;
            return wpos;
        }
    }
}
