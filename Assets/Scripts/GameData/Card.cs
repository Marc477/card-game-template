﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [System.Serializable]
    public class CardStatus
    {
        public StatusEffect effect;
        public int value;
        public int duration = 1;
        public bool permanent = true;

        public CardStatus(StatusEffect status_effect, int value, int duration)
        {
            this.effect = status_effect;
            this.value = value;
            this.duration = duration;
            this.permanent = (duration == 0);
        }
    }

    [System.Serializable]
    public class Card
    {
        public string card_id;
        public string uid;
        public int player_id;
        public CardVariant variant;

        public Slot slot;
        public int energy;

        public int attack = 0;
        public int hp = 0;

        public int attack_bonus = 0;
        public int hp_bonus = 0;

        public int attack_ongoing_bonus = 0;
        public int hp_ongoing_bonus = 0;

        public List<CardStatus> ongoing_status = new List<CardStatus>();
        public List<CardStatus> status_effects = new List<CardStatus>();

        public Card(string card_id, string uid, int player_id) { this.card_id = card_id; this.uid = uid; this.player_id = player_id; }

        public void Refresh() { }
        public void CleanOngoing() { ongoing_status.Clear(); attack_ongoing_bonus = 0; hp_ongoing_bonus = 0; }
        public void Cleanse()
        {
            CleanOngoing(); Refresh(); attack = 0; hp = 0; attack_bonus = 0; hp_bonus = 0;
            status_effects.Clear(); ongoing_status.Clear();
        }

        public int GetOriginalPower()
        {
            CardData icard = CardData.Get(card_id);
            if (icard)
                return icard.hp;
            return 0;
        }

        public int GetOriginalAttack()
        {
            CardData icard = CardData.Get(card_id);
            if (icard)
                return icard.attack;
            return 0;
        }

        public int GetAttack() { return attack + attack_bonus + attack_ongoing_bonus; }
        public int GetHP() { return hp + hp_bonus + hp_ongoing_bonus; }

        public bool IsStrongerThan(Card card)
        {
            return GetAttack() > card.GetAttack();
        }

        public List<CardStatus> GetAllStatus()
        {
            List<CardStatus> all_status = new List<CardStatus>();
            all_status.AddRange(status_effects);
            all_status.AddRange(ongoing_status);
            return all_status;
        }

        public bool HasStatusEffect(StatusEffect effect)
        {
            foreach (CardStatus status in status_effects)
            {
                if (status.effect == effect)
                    return true;
            }
            foreach (CardStatus status in ongoing_status)
            {
                if (status.effect == effect)
                    return true;
            }
            return false;
        }

        public int GetStatusEffectValue(StatusEffect effect)
        {
            int value = 0;
            foreach (CardStatus status in status_effects)
            {
                if (status.effect == effect)
                    value += status.value;
            }
            foreach (CardStatus status in ongoing_status)
            {
                if (status.effect == effect)
                    value += status.value;
            }
            return value;
        }

        public CardStatus GetStatus(StatusEffect effect)
        {
            foreach (CardStatus status in status_effects)
            {
                if (status.effect == effect)
                    return status;
            }
            return null;
        }

        public CardStatus GetOngoingStatus(StatusEffect effect)
        {
            foreach (CardStatus status in ongoing_status)
            {
                if (status.effect == effect)
                    return status;
            }
            return null;
        }

        public AbilityData GetAbility(AbilityTrigger trigger, AbilityEffect effect)
        {
            CardData icard = CardData.Get(card_id);
            foreach (AbilityData iability in icard.abilities)
            {
                if (iability.trigger == trigger && iability.effect == effect)
                    return iability;
            }
            return null;
        }

        public bool HasAbility(AbilityTrigger trigger, AbilityEffect effect)
        {
            AbilityData iability = GetAbility(trigger, effect);
            if (iability != null)
                return true;
            return false;
        }

        public bool HasActiveAbility(Game data, AbilityTrigger trigger, AbilityEffect effect)
        {
            AbilityData iability = GetAbility(trigger, effect);
            if (iability != null && CanDoAbilities() && iability.AreConditionsMet(data, this))
                return true;
            return false;
        }

        public bool CanMoveTo(Slot aslot)
        {
            return this.slot.IsNextTo(aslot);
        }

        public bool CanAttackTarget(Card target)
        {
            if (player_id == target.player_id)
                return false; //Cant attack same player

            if (target.HasStatusEffect(StatusEffect.Stealth))
                return false; //Stealth cant be attacked

            if (target.HasStatusEffect(StatusEffect.Protected) && !target.HasStatusEffect(StatusEffect.Protection))
                return false; //Protected by adjacent card

            return true;
        }

        public bool CanDoActions()
        {
            if (HasStatusEffect(StatusEffect.Paralysed))
                return false;
            return true;
        }

        public bool CanDoAbilities()
        {
            if (HasStatusEffect(StatusEffect.Paralysed))
                return false;
            if (HasStatusEffect(StatusEffect.NoAbility))
                return false;

            return true;
        }

        public bool CanTarget(Card target)
        {
            if (target == null)
                return false;
            if (target.HasStatusEffect(StatusEffect.Stealth))
                return false;
            return true;
        }
    }
}
