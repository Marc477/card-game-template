﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [System.Serializable]
    public struct Slot
    {
        public int x; //From 1 to 4
        public int p; //0 or 1

        public Slot(int x, int p)
        {
            this.x = x;
            this.p = p;
        }

        //Diagonals included
        public bool IsNextTo(Slot slot)
        {
            return Mathf.Abs(x - slot.x) <= 1;
        }

        //No diagonals
        public bool IsVeryNextTo(Slot slot)
        {
            return Mathf.Abs(x - slot.x) <= 1 && (x == slot.x || p == slot.p);
        }

        //Same row only
        public bool IsNextToSameRow(Slot slot)
        {
            return Mathf.Abs(x - slot.x) <= 1 && p == slot.p;
        }

        public bool IsSameRow(Slot slot)
        {
            return slot.p == p;
        }

        public bool IsValid()
        {
            return x >= 1 && x <= 4 && p >= 0 && p <= 1;
        }

        public static bool operator ==(Slot slot1, Slot slot2)
        {
            return slot1.x == slot2.x && slot1.p == slot2.p;
        }

        public static bool operator !=(Slot slot1, Slot slot2)
        {
            return slot1.x != slot2.x || slot1.p != slot2.p;
        }

        public override bool Equals(object o)
        {
            return base.Equals(o);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Slot None
        {
            get { return new Slot(0, 0); }
        }
    }
}
