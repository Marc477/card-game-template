﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift;

namespace CardGameTemplate
{

    [System.Serializable]
    public enum PlayMode
    {
        Training = 0,
        Adventure = 10,
        Multiplayer = 20,
        Observer = 30,
    }

    [System.Serializable]
    public enum RankMode
    {
        Casual = 0,
        Ranked = 10,
        Reversal = 20,
        Mirror = 30,
    }

    [System.Serializable]
    public class GameSettings : IDarkRiftSerializable
    {
        public PlayMode play_mode = PlayMode.Training;
        public RankMode rank_mode = RankMode.Casual;

        public string scene;
        public float turn_duration = 60f;
        public string tournament;

        public void Deserialize(DeserializeEvent e)
        {
            play_mode = (PlayMode)e.Reader.ReadInt32();
            rank_mode = (RankMode)e.Reader.ReadInt32();
            scene = e.Reader.ReadString();
            tournament = e.Reader.ReadString();
            turn_duration = (float)e.Reader.ReadSingle();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write((int)play_mode);
            e.Writer.Write((int)rank_mode);
            e.Writer.Write(scene);
            e.Writer.Write(tournament);
            e.Writer.Write(turn_duration);
        }

        public static string GetRankModeString(RankMode rank_mode)
        {
            if (rank_mode == RankMode.Ranked)
                return "ranked";
            if (rank_mode == RankMode.Casual)
                return "casual";
            if (rank_mode == RankMode.Reversal)
                return "reversal";
            if (rank_mode == RankMode.Mirror)
                return "mirror";
            return "";
        }

        public static RankMode GetRankMode(string rank_id)
        {
            if (rank_id == "ranked")
                return RankMode.Ranked;
            if (rank_id == "casual")
                return RankMode.Casual;
            if (rank_id == "reversal")
                return RankMode.Reversal;
            if (rank_id == "mirror")
                return RankMode.Mirror;
            return RankMode.Casual;
        }

        public static GameSettings Default
        {
            get
            {
                GameSettings settings = new GameSettings();
                settings.play_mode = PlayMode.Multiplayer;
                settings.rank_mode = RankMode.Ranked;
                settings.scene = "Game";
                settings.tournament = "";
                return settings;
            }
        }

        public static GameSettings Tournament(string tid, string mode, int gems)
        {
            GameSettings settings = new GameSettings();
            settings.play_mode = PlayMode.Multiplayer;
            settings.rank_mode = GetRankMode(mode);
            settings.scene = "Game";
            settings.tournament = tid;
            return settings;
        }

    }
}