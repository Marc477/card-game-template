﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [System.Serializable]
    public class Player
    {
        public int player_id;
        public string username;
        public string deck;
        public bool is_ai = false;
        public int ai_level;

        public bool connected = false; //Connected to server and game
        public bool ready = false; //Clicked ready to start game

        public int actions = 0;
        public int kill_count = 0;
        public int no_action_count = 0;

        public List<Card> cards_all = new List<Card>();
        public List<Card> cards_deck = new List<Card>();
        public List<Card> cards_hand = new List<Card>();
        public List<Card> cards_board = new List<Card>();
        public List<Card> cards_discard = new List<Card>();

        public Player(int id) { this.player_id = id; }

        public bool IsReady() { return ready && cards_all.Count > 0; }
        public bool IsConnected() { return connected || is_ai; }

        public void AddCard(List<Card> card_list, Card card)
        {
            card_list.Add(card);
        }

        public void RemoveCard(List<Card> card_list, Card card)
        {
            card_list.Remove(card);
        }

        public Slot GetRandomSlot()
        {
            return new Slot(Random.Range(1, 5), player_id);
        }

        public Card GetRandomCard(List<Card> card_list)
        {
            if (card_list.Count > 0)
                return card_list[Random.Range(0, card_list.Count)];
            return null;
        }

        public bool HasCard(List<Card> card_list, Card card)
        {
            return card_list.Contains(card);
        }

        public Card GetHandCard(string uid)
        {
            foreach (Card card in cards_hand)
            {
                if (card.uid == uid)
                    return card;
            }
            return null;
        }

        public Card GetCard(string uid)
        {
            foreach (Card card in cards_all)
            {
                if (card.uid == uid)
                    return card;
            }
            return null;
        }

        public int CountTeamCards(TeamType team)
        {
            int count = 0;
            foreach (Card card in cards_board)
            {
                CardData icard = CardData.Get(card.card_id);
                if (icard != null && icard.team == team)
                    count++;
            }
            return count;
        }

        public int CountLineCard(int side)
        {
            int count = 0;
            foreach (Card card in cards_board)
            {
                if (card.slot.p == side)
                    count++;
            }
            return count;
        }

        public bool CanDoMoveAction(Card card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0;
        }

        public bool CanDoAttackAction(Card card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0;
        }

        public bool CanDoActivation(Card card, AbilityData ability)
        {
            if (card == null || !card.CanDoActions())
                return false;

            if (ability.trigger == AbilityTrigger.Activate && card.CanDoAbilities())
            {
                bool has_action = !ability.use_action || actions > 0;
                return has_action;
            }
            return false;
        }

        public bool CanDoAnyAction(Card card)
        {
            if (card == null || !card.CanDoActions())
                return false;

            return actions > 0 || CanDoMoveAction(card) || CanDoAttackAction(card);
        }

        public bool CanDoPoseAction()
        {
            return actions > 0;
        }

        public bool CanDoAnyAction()
        {
            foreach (Card card in cards_board)
            {
                if (CanDoAnyAction(card))
                    return true;
            }

            return actions > 0;
        }
    }
}