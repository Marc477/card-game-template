﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [System.Serializable]
    public enum GameState
    {
        Connecting = 0, //Player are not connected OR still selecting settings and not ready
        Starting = 1, //Players are ready but did not load the game scene, so game not started

        StartTurn = 3, //Start of turn effects
        Play = 5, //Play step
        EndTurn = 8, //End of turn effects

        Selector = 10,

        Battle = 15,

        AbilityQueue = 20,

        GameEnded = 30,
    }

    [System.Serializable]
    public enum SelectorType
    {
        None = 0,
        SelectCardTarget = 10,
        SelectCardSelector = 20,
        SelectSlot = 30,
        SelectChoice = 40,
    }

    [System.Serializable]
    public class Game
    {
        public int nb_players = 2;
        public GameSettings settings;
        public string game_uid;

        public int first_player = 0;
        public int current_player = 0;
        public GameState current_state = GameState.Connecting;
        public int turn_count = 0;
        public float turn_timer = 0f;

        //Battle
        public string attacker_uid;
        public string defender_uid;

        //Target Selection step
        public SelectorType selector = SelectorType.None;
        public int selector_player = 0;
        public string selector_ability_id;
        public string selector_effect_id;
        public string selector_caster_uid;
        public string selector_attacker_uid;
        public AbilityTarget selector_source;

        public Player[] players;

        public Game(string uid, int nb_players)
        {
            this.game_uid = uid;
            this.nb_players = nb_players;
            players = new Player[nb_players];
            for (int i = 0; i < nb_players; i++)
                players[i] = new Player(i);
            settings = GameSettings.Default;
        }

        public Player GetPlayer(int id)
        {
            if (id >= 0 && id < players.Length)
                return players[id];
            return null;
        }

        public Player GetActivePlayer()
        {
            return GetPlayer(current_player);
        }

        public Player GetOpponentPlayer(int id)
        {
            int oid = id == 0 ? 1 : 0;
            return GetPlayer(oid);
        }

        public bool AreAllPlayersReady()
        {
            int ready = 0;
            foreach (Player player in players)
            {
                if (player.IsReady())
                    ready++;
            }
            return ready >= nb_players;
        }

        public bool AreAllPlayersConnected()
        {
            int ready = 0;
            foreach (Player player in players)
            {
                if (player.IsConnected())
                    ready++;
            }
            return ready >= nb_players;
        }

        public Card GetCard(string card_uid)
        {
            foreach (Player player in players)
            {
                Card acard = player.GetCard(card_uid);
                if (acard != null)
                    return acard;
            }
            return null;
        }

        public Card GetRandomBoardCard(int player_id)
        {
            Player player = GetPlayer(player_id);
            List<Card> board_cards = new List<Card>();
            foreach (Card card in player.cards_board)
            {
                if (card.player_id == player_id)
                    board_cards.Add(card);
            }
            if (board_cards.Count > 0)
                return board_cards[Random.Range(0, board_cards.Count)];
            return null;
        }

        public Card GetBoardCard(string card_uid)
        {
            foreach (Player player in players)
            {
                foreach (Card card in player.cards_board)
                {
                    if (card != null && card.uid == card_uid)
                        return card;
                }
            }
            return null;
        }

        public Card GetSlotCard(Slot slot)
        {
            foreach (Player player in players)
            {
                foreach (Card card in player.cards_board)
                {
                    if (card != null && card.slot == slot)
                        return card;
                }
            }
            return null;
        }

        public Slot GetRandomSlot()
        {
            Player player = GetPlayer(Random.value > 0.5f ? 1 : 0);
            return player.GetRandomSlot();
        }

        public bool IsCardInHand(string card_uid)
        {
            Card card = GetCard(card_uid);
            if (card != null)
            {
                Player player = GetPlayer(card.player_id);
                return player.HasCard(player.cards_hand, card);
            }
            return false;
        }

        public bool IsCardInDeck(string card_uid)
        {
            Card card = GetCard(card_uid);
            if (card != null)
            {
                Player player = GetPlayer(card.player_id);
                return player.HasCard(player.cards_deck, card);
            }
            return false;
        }

        public bool IsDiscarded(string card_uid)
        {
            Card card = GetCard(card_uid);
            if (card != null)
            {
                Player player = GetPlayer(card.player_id);
                return player.HasCard(player.cards_discard, card);
            }
            return false;
        }

        public bool IsCardOnBoard(string card_uid)
        {
            return GetBoardCard(card_uid) != null;
        }

        public bool IsCardOnSlot(Slot slot)
        {
            return GetSlotCard(slot) != null;
        }
    }
}