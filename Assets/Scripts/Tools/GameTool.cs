﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CardGameTemplate
{

    public class GameTool
    {
        public static Coroutine StartCoroutine(IEnumerator routine)
        {
            return GameToolMono.Inst.StartCoroutine(routine);
        }

        public static void StopCoroutine(Coroutine routine)
        {
            GameToolMono.Inst.StopCoroutine(routine);
        }

        //Do function after x secs
        public static void DoAfter(float duration, UnityAction callback)
        {
            StartCoroutine(DoAfterRoutine(duration, callback));
        }

        private static IEnumerator DoAfterRoutine(float duration, UnityAction callback)
        {
            yield return new WaitForSeconds(duration);
            callback?.Invoke();
        }

        public static string GenerateUID(int length, int length_max = 0)
        {
            int lgt = UnityEngine.Random.Range(length, Mathf.Max(length, length_max));
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string unique_id = "";
            for (int i = 0; i < lgt; i++)
            {
                unique_id += chars[UnityEngine.Random.Range(0, chars.Length - 1)];
            }
            return unique_id;
        }
    }
}