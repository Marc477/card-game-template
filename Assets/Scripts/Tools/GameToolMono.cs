﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{
	public class GameToolMono : MonoBehaviour
	{
		private static GameToolMono _instance;

		public Coroutine StartRoutine(IEnumerator routine)
		{
			return StartCoroutine(routine);
		}

		public void StopRoutine(Coroutine routine)
		{
			StopCoroutine(routine);
		}

		public static GameToolMono Inst
		{
			get
			{
				if (_instance == null)
				{
					GameObject ntool = new GameObject("GameTool Monobehavior");
					_instance = ntool.AddComponent<GameToolMono>();
				}
				return _instance;
			}
		}
	}
}
