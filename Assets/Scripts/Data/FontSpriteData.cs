﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "FontSpriteData", menuName = "Data/FontSpriteData", order = 10)]
    public class FontSpriteData : ScriptableObject
    {
        public Sprite[] numbers;
        [HideInInspector] public Sprite plus;
        [HideInInspector] public Sprite minus;

        public Sprite GetSprite(string alphanumeric)
        {
            int number;
            bool parsed = int.TryParse(alphanumeric, out number);
            if (parsed && number >= 0 && number < numbers.Length)
                return numbers[number];
            return null;
        }

        public Sprite GetSprite(int number)
        {
            if (number >= 0 && number < numbers.Length)
                return numbers[number];
            return null;
        }
    }
}