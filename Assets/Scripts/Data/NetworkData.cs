﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "NetworkData", menuName = "Data/NetworkData", order = 5)]
    public class NetworkData : ScriptableObject
    {
        [Header("Game Server")]
        public string url;
        public int port;

        [Header("API")]
        public string api_url;
        public string api_version;

        public static NetworkData Get()
        {
            return DataLoader.Get().network;
        }
    }
}
