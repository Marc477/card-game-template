﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace CardGameTemplate
{

    [System.Serializable]
    public class PlayerData
    {
        public string filename;
        public string version;
        public DateTime last_save;

        //-------------------

        public List<string> completed_levels = new List<string>();

        //-------------------

        private static string file_loaded = "";
        public static PlayerData player_data = null;

        public PlayerData(string name)
        {
            filename = name;
            version = Application.version;
            last_save = DateTime.Now;

        }

        public void FixData()
        {
            if (completed_levels == null)
                completed_levels = new List<string>();
        }

        public void CompleteLevel(string level_id)
        {
            if (!completed_levels.Contains(level_id))
                completed_levels.Add(level_id);
        }

        public bool IsLevelCompleted(string level_id)
        {
            return completed_levels.Contains(level_id);
        }

        //--- Save / load -----

        public bool IsVersionValid()
        {
            return version == Application.version;
        }

        public void Save()
        {
            Save(file_loaded, this);
        }

        public static void Save(string filename, PlayerData data)
        {
            if (!string.IsNullOrEmpty(filename) && data != null)
            {
                data.filename = filename;
                data.last_save = DateTime.Now;
                data.version = Application.version;
                player_data = data;
                file_loaded = filename;

                SaveSystem.SaveFile<PlayerData>(filename, data);
                SaveSystem.SetLastSave(filename);
            }
        }

        public static void NewGame()
        {
            NewGame(GetLastSave()); //default name
        }

        //You should reload the scene right after NewGame
        public static PlayerData NewGame(string filename)
        {
            file_loaded = filename;
            player_data = new PlayerData(filename);
            player_data.FixData();
            return player_data;
        }

        public static PlayerData Load(string filename)
        {
            if (player_data == null || file_loaded != filename)
            {
                player_data = SaveSystem.LoadFile<PlayerData>(filename);
                if (player_data != null)
                {
                    file_loaded = filename;
                    player_data.FixData();
                }
            }
            return player_data;
        }

        public static PlayerData LoadLast()
        {
            return AutoLoad(GetLastSave());
        }

        //Load if found, otherwise new game
        public static PlayerData AutoLoad(string filename)
        {
            if (player_data == null)
                player_data = Load(filename);
            if (player_data == null)
                player_data = NewGame(filename);
            return player_data;
        }

        public static string GetLastSave()
        {
            string name = SaveSystem.GetLastSave();
            if (string.IsNullOrEmpty(name))
                name = "player"; //Default name
            return name;
        }

        public static void Unload()
        {
            player_data = null;
            file_loaded = "";
        }

        public static void Delete(string filename)
        {
            if (file_loaded == filename)
            {
                player_data = new PlayerData(filename);
                player_data.FixData();
            }

            SaveSystem.DeleteFile(filename);
        }

        public static bool IsLoaded()
        {
            return player_data != null && !string.IsNullOrEmpty(file_loaded);
        }

        public static PlayerData Get()
        {
            return player_data;
        }
    }
}