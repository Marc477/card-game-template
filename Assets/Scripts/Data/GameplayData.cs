﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "GameplayData", menuName = "Data/GameplayData", order = 0)]
    public class GameplayData : ScriptableObject
    {
        [Header("Game Stats")]
        public int starting_cards = 4;
        public float turn_duration = 30f;

        [Header("Scenes")]
        public string[] arena_list;

        [Header("AI Decks")]
        public DeckData[] ai_decks;

        [Header("Test")]
        public DeckData test_deck;
        public DeckData test_deck_ai;

        public int GetPlayerLevel(int xp)
        {
            return 1;
        }

        public static GameplayData Get()
        {
            return DataLoader.Get().data;
        }
    }
}