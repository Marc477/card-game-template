﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.AI;

namespace CardGameTemplate
{

    public enum ConditionType
    {
        None = 0,

        IsYourTurn = 5,

        CardIsOfType = 10,
        CardHasStatus = 12,
        CardIsYours = 14,

        CountDiscard = 30,
        CountBoard = 32,
        OpponentCountBoard = 33,

        IsNextTo = 40,
        IsNextToSameRow = 42,

        CardAttack = 50,
        CardHP = 52,

    }

    public enum ConditionOperator
    {
        Equal,
        NotEqual,
        GreaterEqual,
        LessEqual,
        Greater,
        Less,
    }

    [CreateAssetMenu(fileName = "ConditionData", menuName = "Data/ConditionData", order = 5)]
    public class ConditionData : ScriptableObject
    {
        public ConditionType type;
        public ConditionOperator oper;
        public int value;

        [Header("Traits")]
        public CardType has_type;
        public TeamType has_team;

        [Header("Status")]
        public StatusEffect has_status;

        public bool IsConditionMet(Game data, Card caster, Card trigger_card = null)
        {
            if (type == ConditionType.IsYourTurn)
            {
                bool turn = caster.player_id == data.current_player;
                return ConditionBool(turn);
            }

            if (type == ConditionType.CountBoard)
            {
                int count = 0;
                Player player = data.GetPlayer(caster.player_id);
                foreach (Card card in player.cards_board)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (type == ConditionType.OpponentCountBoard)
            {
                int count = 0;
                Player player = data.GetPlayer(caster.player_id == 0 ? 1 : 0);
                foreach (Card card in player.cards_board)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (type == ConditionType.CountDiscard)
            {
                int count = 0;
                Player player = data.GetPlayer(caster.player_id);
                foreach (Card card in player.cards_discard)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (trigger_card != null)
            {
                if (type == ConditionType.CardIsOfType)
                {
                    CardData icard = CardData.Get(trigger_card.card_id);
                    return ConditionBool(IsTrait(icard));
                }

                if (type == ConditionType.CardHasStatus)
                {
                    bool hstatus = trigger_card.HasStatusEffect(has_status);
                    return ConditionBool(hstatus);
                }

                if (type == ConditionType.CardIsYours)
                {
                    return ConditionBool(caster.player_id == trigger_card.player_id);
                }

                if (type == ConditionType.IsNextTo)
                {
                    bool next = caster.slot.IsNextTo(trigger_card.slot);
                    return ConditionBool(next);
                }

                if (type == ConditionType.IsNextToSameRow)
                {
                    bool next = caster.slot.IsNextToSameRow(trigger_card.slot);
                    return ConditionBool(next);
                }

                if (type == ConditionType.CardAttack)
                {
                    return CompareInt(trigger_card.GetAttack(), value);
                }

                if (type == ConditionType.CardHP)
                {
                    return CompareInt(trigger_card.GetHP(), value);
                }

            }

            return false;
        }

        public bool IsConditionMet(AIData data, CardA caster, CardA trigger_card = null)
        {

            if (type == ConditionType.CountBoard)
            {
                int count = 0;
                PlayerA player = data.GetPlayer(caster.player_id);
                foreach (CardA card in player.cards_board)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (type == ConditionType.OpponentCountBoard)
            {
                int count = 0;
                PlayerA player = data.GetPlayer(caster.player_id == 0 ? 1 : 0);
                foreach (CardA card in player.cards_board)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (type == ConditionType.CountDiscard)
            {
                int count = 0;
                PlayerA player = data.GetPlayer(caster.player_id);
                foreach (CardA card in player.cards_discard)
                {
                    CardData icard = CardData.Get(card.card_id);
                    if (IsTrait(icard))
                        count++;
                }
                return CompareInt(count, value);
            }

            if (trigger_card != null)
            {
                if (type == ConditionType.CardIsOfType)
                {
                    CardData icard = CardData.Get(trigger_card.card_id);
                    return ConditionBool(IsTrait(icard));
                }

                if (type == ConditionType.CardHasStatus)
                {
                    bool hstatus = trigger_card.HasStatusEffect(has_status);
                    return ConditionBool(hstatus);
                }

                if (type == ConditionType.CardIsYours)
                {
                    return ConditionBool(caster.player_id == trigger_card.player_id);
                }

                if (type == ConditionType.IsNextTo)
                {
                    bool next = caster.slot.IsNextTo(trigger_card.slot);
                    return ConditionBool(next);
                }

                if (type == ConditionType.IsNextToSameRow)
                {
                    bool next = caster.slot.IsNextToSameRow(trigger_card.slot);
                    return ConditionBool(next);
                }

                if (type == ConditionType.CardAttack)
                {
                    return CompareInt(trigger_card.GetAttack(), value);
                }

                if (type == ConditionType.CardHP)
                {
                    return CompareInt(trigger_card.GetHP(), value);
                }

            }

            return false;
        }

        private bool IsTrait(CardData icard)
        {
            bool is_type = icard.type == has_type || has_type == CardType.None;
            bool is_team = icard.team == has_team || has_team == TeamType.None;
            return (is_type && is_team);
        }

        public bool ConditionBool(bool condition)
        {
            if (CompareInt(value, 1))
                return condition;
            return !condition;
        }

        public bool CompareInt(int ival1, int ival2)
        {
            bool condition_met = true;
            if (oper == ConditionOperator.Equal && ival1 != ival2)
            {
                condition_met = false;
            }
            if (oper == ConditionOperator.NotEqual && ival1 == ival2)
            {
                condition_met = false;
            }
            if (oper == ConditionOperator.GreaterEqual && ival1 < ival2)
            {
                condition_met = false;
            }
            if (oper == ConditionOperator.LessEqual && ival1 > ival2)
            {
                condition_met = false;
            }
            if (oper == ConditionOperator.Greater && ival1 <= ival2)
            {
                condition_met = false;
            }
            if (oper == ConditionOperator.Less && ival1 >= ival2)
            {
                condition_met = false;
            }
            return condition_met;
        }
    }
}