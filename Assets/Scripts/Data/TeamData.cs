﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum TeamType
    {
        None = 0,
        Team1 = 10,
        Team2 = 20,
        Team3 = 30,
    }

    [CreateAssetMenu(fileName = "TeamData", menuName = "Data/TeamData", order = 1)]
    public class TeamData : ScriptableObject
    {
        public TeamType type;
        public string title;
        public Sprite icon;
        public Sprite team_effect;
        public Sprite team_effect_off;
        public StatusData team_effect_status;
        public GameObject team_effect_fx;
        public AudioClip team_effect_audio;

        public static TeamData Get(TeamType type)
        {
            foreach (TeamData team in GetAll())
            {
                if (team.type == type)
                    return team;
            }
            return null;
        }

        public static List<TeamData> GetAll()
        {
            return DataLoader.Get().teams;
        }
    }
}