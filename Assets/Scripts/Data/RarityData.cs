﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum CardRarity
    {
        None = 0,
        Common = 10,
        Uncommon = 20,
        Rare = 30,
        Mythic = 40,
        Echo = 50,
    }

    [CreateAssetMenu(fileName = "RarityData", menuName = "Data/RarityData", order = 1)]
    public class RarityData : ScriptableObject
    {
        public CardRarity type;
        public string title;
        public Sprite icon;
        public int rank;

        public int GetDefaultCost()
        {
            if (type == CardRarity.Common)
                return 1000;
            if (type == CardRarity.Uncommon)
                return 3000;
            if (type == CardRarity.Rare)
                return 10000;
            if (type == CardRarity.Mythic)
                return 50000;
            if (type == CardRarity.Echo)
                return 500000;
            return 0;
        }

        public static RarityData Get(CardRarity type)
        {
            foreach (RarityData rarity in GetAll())
            {
                if (rarity.type == type)
                    return rarity;
            }
            return null;
        }

        public static List<RarityData> GetAll()
        {
            return DataLoader.Get().rarities;
        }
    }
}