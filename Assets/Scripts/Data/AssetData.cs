﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "AssetData", menuName = "Data/AssetData", order = 0)]
    public class AssetData : ScriptableObject
    {
        [Header("Prefabs")]
        public GameObject card_prefab;

        [Header("FX")]
        public GameObject card_spawn_fx;
        public GameObject card_destroy_fx;
        public GameObject damage_fx;
        public GameObject play_card_fx;
        public GameObject mouse_line_fx;
        public GameObject live_text_fx;
        public GameObject hover_text_box;
        public GameObject win_fx;
        public GameObject lose_fx;
        public GameObject tied_fx;

        [Header("Audio")]
        public AudioClip card_spawn_audio;
        public AudioClip card_destroy_audio;
        public AudioClip card_attack_audio;
        public AudioClip card_defend_audio;
        public AudioClip hand_card_click_audio;
        public AudioClip win_audio;
        public AudioClip defeat_audio;
        public AudioClip win_music;
        public AudioClip defeat_music;

        public static AssetData Get()
        {
            return DataLoader.Get().assets;
        }
    }
}
