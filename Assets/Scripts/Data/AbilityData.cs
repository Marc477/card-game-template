﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum AbilityTrigger
    {
        None = 0,
        Ongoing = 2, //Always active
        Activate = 5, //Action
        OnPlay = 10, //Entré
        OnAction = 15, //When doing action
        StartOfTurn = 20, //Every turn
        EndOfTurn = 22, //Every turn
        OnFight = 34, //Fight (either att or defend)
        OnKill = 36, //Kill opponent
        OnDeath = 40, //When dies

    }

    public enum AbilityEffect
    {
        None = 0,

        AddAttack = 4, //Add permanant base Attack
        AddHP = 5, //Add permanant HP

        AddStatus = 10, //Add Status to card
        Destroy = 15, //Kill character

        PlayCard = 20, //Play an existing card (ex: summon from graveyard, from hand)
        SummonCard = 22, //Creates a totally new card that is not in any deck
        SendHand = 24, //Send card to players hand
        SendBottomDeck = 25, //Send card to bottom of deck
        SendDiscard = 26, //Send card to discard (dont use for cards on board

        DrawCard = 30, //Draw X cards
        ShuffleDeck = 32, //Shuffle deck

        //SaveTarget = 90, //Save target for future ability

    }

    public enum AbilityTarget
    {
        None = 0,
        Self = 1,

        AllCards = 10,
        AllAllies = 11,
        AllEnemies = 12,

        YourLine = 16,
        OpponentLine = 17,

        SelectCard = 20,
        SelectSlot = 22,
        SelectChoice = 24,

        SelectInHand = 30,
        SelectInDeck = 31,
        SelectInDiscard = 32,

        SelectInOpponentHand = 35,

        //RandomInHand = 35,
        //RandomInDeck = 36,
        //RandomInDiscard = 37,

        LastPlayed = 50,
        LastTarget = 52,
        LastKill = 54,
        //SavedTarget = 55,

    }

    [CreateAssetMenu(fileName = "AbilityData", menuName = "Data/AbilityData", order = 4)]
    public class AbilityData : ScriptableObject
    {
        public string id;

        [Header("Trigger")]
        public AbilityTrigger trigger;

        [Header("Conditions")]
        public ConditionData[] conditions;
        public ConditionData[] conditions_target;
        //public bool once_per_turn = false;

        [Header("Effect")]
        public AbilityTarget target;
        public AbilityEffect effect;
        public int value = 0;
        public int duration = 0;

        [Header("Specific Effect")]
        public StatusData[] status_effects;
        public CardData[] spawn_cards;
        public AbilityData[] choices;

        [Header("Activate Ability")]
        public string title;
        public string subtitle;
        public bool use_action;

        [Header("Chain abilities")]
        public AbilityData[] chain_abilities;

        [Header("FX")]
        public GameObject board_fx;
        public GameObject caster_fx;
        public GameObject target_fx;
        public AudioClip cast_audio;
        public AudioClip target_audio;

        public bool AreConditionsMet(Game data, Card caster)
        {
            bool met = true;
            foreach (ConditionData cond in conditions)
            {
                if (cond && !cond.IsConditionMet(data, caster, caster))
                    met = false;
            }
            return met;
        }

        public bool AreTargetConditionsMet(Game data, Card caster, Card target_card)
        {
            bool met = true;
            foreach (ConditionData cond in conditions_target)
            {
                if (cond && !cond.IsConditionMet(data, caster, target_card))
                    met = false;
            }
            return met;
        }

        public bool CanTarget(Game data, Card caster, Card target)
        {
            if (target == null)
                return false;

            bool condition_match = AreTargetConditionsMet(data, caster, target);

            return condition_match;
        }

        public static AbilityData Get(string id)
        {
            foreach (AbilityData ability in GetAll())
            {
                if (ability.id == id)
                    return ability;
            }
            return null;
        }

        public static List<AbilityData> GetAll()
        {
            return DataLoader.Get().abilities;
        }
    }
}
