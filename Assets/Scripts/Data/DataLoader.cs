﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;

namespace CardGameTemplate
{

    /// <summary>
    /// This script initiates loading all the game data
    /// </summary>

    public class DataLoader : MonoBehaviour
    {
        public GameplayData data;
        public AssetData assets;
        public NetworkData network;
        public bool server;

        [HideInInspector] public List<CardData> cards = new List<CardData>();
        [HideInInspector] public List<TeamData> teams = new List<TeamData>();
        [HideInInspector] public List<RarityData> rarities = new List<RarityData>();
        [HideInInspector] public List<DeckData> decks = new List<DeckData>();
        [HideInInspector] public List<AbilityData> abilities = new List<AbilityData>();
        [HideInInspector] public List<StatusData> status_effects = new List<StatusData>();
        [HideInInspector] public List<AvatarData> avatar = new List<AvatarData>();

        private static DataLoader instance;

        void Awake()
        {
            instance = this;
            LoadData();
        }

        public void LoadData()
        {
            cards.Clear();
            teams.Clear();
            rarities.Clear();
            decks.Clear();
            abilities.Clear();
            status_effects.Clear();
            avatar.Clear();

            CardData[] cards_array = Resources.LoadAll<CardData>("/");
            TeamData[] teams_array = Resources.LoadAll<TeamData>("/");
            RarityData[] rarities_array = Resources.LoadAll<RarityData>("/");
            DeckData[] decks_array = Resources.LoadAll<DeckData>("/");
            StatusData[] status_array = Resources.LoadAll<StatusData>("/");
            AvatarData[] avatar_array = Resources.LoadAll<AvatarData>("/");

            foreach (DeckData deck in decks_array)
                LoadDeck(deck);

            foreach (CardData card in cards_array)
                LoadCard(card);

            foreach (TeamData team in teams_array)
                LoadTeam(team);

            foreach (RarityData rarity in rarities_array)
                LoadRarity(rarity);

            foreach (StatusData status in status_array)
                LoadStatus(status);

            avatar.AddRange(avatar_array);
        }

        public void LoadDeck(DeckData deck)
        {
            if (deck != null && !decks.Contains(deck))
            {
                decks.Add(deck);

                foreach (CardData card in deck.cards)
                    LoadCard(card);
            }
        }

        public void LoadCard(CardData card)
        {
            if (card != null && !cards.Contains(card))
            {
                cards.Add(card);

                foreach (AbilityData ability in card.abilities)
                    LoadAbility(ability);
            }
        }

        public void LoadTeam(TeamData team)
        {
            if (team != null && !teams.Contains(team))
            {
                teams.Add(team);
            }
        }

        public void LoadRarity(RarityData rarity)
        {
            if (rarity != null && !rarities.Contains(rarity))
            {
                rarities.Add(rarity);
            }
        }

        public void LoadAbility(AbilityData ability)
        {
            if (ability != null && !abilities.Contains(ability))
            {
                abilities.Add(ability);
				
				foreach (AbilityData oabi in ability.choices)
                    LoadAbility(oabi);

                foreach (AbilityData oabi in ability.chain_abilities)
                    LoadAbility(oabi);

                foreach (StatusData status in ability.status_effects)
                    LoadStatus(status);
            }
        }

        public void LoadStatus(StatusData status)
        {
            if (status != null && !status_effects.Contains(status))
            {
                status_effects.Add(status);
            }
        }

        public static DataLoader Get()
        {
            return instance;
        }
    }
}