﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "Avatar", menuName = "Data/Avatar", order = 10)]
    public class AvatarData : ScriptableObject
    {
        public string id;
        public Sprite avatar;
        public Sprite avatar_large;

        public static AvatarData Get(string id)
        {
            foreach (AvatarData avatar in GetAll())
            {
                if (avatar.id == id)
                    return avatar;
            }
            return null;
        }

        public static List<AvatarData> GetAll()
        {
            return DataLoader.Get().avatar;
        }
    }
}
