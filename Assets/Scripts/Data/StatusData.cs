﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum StatusEffect
    {
        None = 0,

        AddAttack = 4, //Ongoing Attack bonus
        AddHP = 5, //Ongoing AttackFin bonus

        Stealth = 20, //Cant be attacked until do action
        Invincibility = 22, //Cant be attacked for X turns
        DoubleLife = 23, //Must be killed twice
        Protection = 24, //Taunt, gives protected to adjacent cards
        Protected = 25, //Taunt, can't be attacked unless they have Protection
        Paralysed = 28, //Cant do any actions for X turns

        NoAbility = 80, //All abilities canceled
    }

    [CreateAssetMenu(fileName = "StatusData", menuName = "Data/StatusData", order = 7)]
    public class StatusData : ScriptableObject
    {
        public StatusEffect effect;

        [Header("Display")]
        public string title;
        public Sprite icon;

        [TextArea(3, 5)]
        public string desc;

        [Header("FX")]
        public GameObject status_fx;

        public static StatusData Get(StatusEffect effect)
        {
            foreach (StatusData status in GetAll())
            {
                if (status.effect == effect)
                    return status;
            }
            return null;
        }

        public static List<StatusData> GetAll()
        {
            return DataLoader.Get().status_effects;
        }
    }
}