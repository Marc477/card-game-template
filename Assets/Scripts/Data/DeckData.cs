﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    [CreateAssetMenu(fileName = "DeckData", menuName = "Data/DeckData", order = 7)]
    public class DeckData : ScriptableObject
    {
        public string id;

        [Header("Display")]
        public string title;
        public AvatarData avatar;

        [Header("Cards")]
        public CardData[] cards;

        public int GetCost()
        {
            int cost = 0;
            foreach (CardData card in cards)
            {
                cost += card.cost;
            }
            return cost;
        }

        public bool IsValid()
        {
            return cards.Length == 8;
        }

        public bool IsOverGemLimit(int max_gems)
        {
            return GetCost() > max_gems;
        }

        public static DeckData Get(string id)
        {
            foreach (DeckData deck in GetAll())
            {
                if (deck.id == id)
                    return deck;
            }
            return null;
        }

        public static List<DeckData> GetAll()
        {
            return DataLoader.Get().decks;
        }
    }
}