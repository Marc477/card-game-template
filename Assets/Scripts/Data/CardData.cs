﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public enum CardType
    {
        None = 0,
        Character = 10,
        Spell = 20,
    }

    public enum CardVariant
    {
        Normal = 0,
        Foil = 5,
    }

    [CreateAssetMenu(fileName = "CardData", menuName = "Data/CardData", order = 5)]
    public class CardData : ScriptableObject
    {
        public string id;

        [Header("Display")]
        public string title;
        public Sprite image_full;
        public Sprite image_deck;

        [Header("Stats")]
        public CardType type;
        public TeamType team;
        public CardRarity rarity;
        public int cost;
        public int attack;
        public int hp;

        [Header("Abilities")]
        public AbilityData[] abilities;

        [Header("Card Text")]
        [TextArea(3, 5)]
        public string card_text;

        [TextArea(2, 4)]
        public string card_quote;

        [TextArea(1, 2)]
        public string artist;
        [TextArea(1, 2)]
        public string artist_collector;

        [Header("Description")]
        [TextArea(5, 10)]
        public string desc;

        [TextArea(5, 10)]
        public string gameplay_desc;

        [Header("FX")]
        public GameObject spawn_fx;
        public GameObject death_fx;
        public AudioClip spawn_audio;
        public AudioClip attack_audio;
        public AudioClip defend_audio;
        public AudioClip death_audio;
        public AudioClip click_audio;
        public float shake_multiplier = 1f;

        [Header("Availability")]
        //public bool adventure = false;
        public bool multiplayer = false;

        public string GetTypeText()
        {
            return type == CardType.Character ? "CHARACTER" : "SPELL";
        }

        public Sprite GetSmallImage(CardVariant variant)
        {
            return GetFullImage(variant);
        }

        public Sprite GetFullImage(CardVariant variant)
        {
            return image_full;
        }

        public bool HasAbility(AbilityTrigger trigger, AbilityEffect effect)
        {
            foreach (AbilityData ability in abilities)
            {
                if (ability && ability.trigger == trigger && ability.effect == effect)
                    return true;
            }
            return false;
        }

        public static string GetVariantText(CardVariant variant)
        {
            if (variant == CardVariant.Foil)
                return "Foil";
            return "";
        }

        public static CardData Get(string id)
        {
            foreach (CardData card in GetAll())
            {
                if (card.id == id)
                    return card;
            }
            return null;
        }

        public static List<CardData> GetAllMulti()
        {
            List<CardData> multi_list = new List<CardData>();
            foreach (CardData acard in GetAll())
            {
                if (acard.multiplayer)
                    multi_list.Add(acard);
            }
            return multi_list;
        }

        public static List<CardData> GetAllMulti(CardVariant variant)
        {
            List<CardData> multi_list = new List<CardData>();
            foreach (CardData acard in GetAll())
            {
                if (acard.multiplayer)
                {
                    multi_list.Add(acard);
                }
            }
            return multi_list;
        }

        public static List<CardData> GetAll()
        {
            return DataLoader.Get().cards;
        }
    }
}