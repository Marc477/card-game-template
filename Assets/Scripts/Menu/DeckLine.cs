﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class DeckLine : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Image image;
        public Text title;
        public Text value;
        public Image value_img;
        public IconValue cost;
        public Image foil;
        public Image warning;
        public Image warning_gem;
        public UIPanel delete_btn;
        public AudioClip click_audio;
        public Material disabled_mat;

        public UnityAction<DeckLine> onClick;
        public UnityAction<DeckLine> onClickRight;
        public UnityAction<DeckLine> onClickDelete;

        private CardData card;
        private CardVariant variant;
        private DeckData deck;
        private UserDeckData udeck;
        private Material default_mat;
        private bool hidden = false;
        private bool hover = false;

        void Awake()
        {
            if (image != null)
                default_mat = image.material;
        }

        void Update()
        {
            if (delete_btn != null)
            {
                delete_btn.SetVisible(hover && !hidden && udeck != null);
            }
        }

        public void SetLine(CardData card, CardVariant variant, bool warn = false)
        {
            this.card = card;
            this.variant = variant;
            this.deck = null;
            this.udeck = null;
            hidden = false;

            if (title != null)
                title.text = card.title;
            if (title != null)
                title.color = variant != CardVariant.Normal ? Color.yellow : Color.white;
            if (value != null)
                value.text = "";
            if (value_img != null)
                value_img.enabled = false;
            if (cost != null)
                cost.value = card.cost;
            if (this.foil != null)
                this.foil.enabled = foil;
            if (warning != null)
                warning.enabled = warn;
            if (warning_gem != null)
                warning_gem.enabled = false;

            if (image != null)
            {
                image.sprite = card.image_deck;
                image.enabled = true;
                image.material = warn ? disabled_mat : default_mat;
            }
        }

        public void SetLine(DeckData deck)
        {
            this.card = null;
            this.deck = deck;
            this.udeck = null;
            hidden = false;

            if (this.title != null)
                this.title.text = deck.title;
            if (this.title != null)
                this.title.color = Color.white;
            if (this.value != null)
                this.value.text = deck.GetCost().ToString();
            if (this.value != null)
                this.value.enabled = deck.GetCost() > 0;
            if (value_img != null)
                value_img.enabled = value.enabled;
            if (this.foil != null)
                this.foil.enabled = false;
            if (warning != null)
                warning.enabled = !deck.IsValid();
        }

        public void SetLine(UserData udata, UserDeckData deck)
        {
            this.card = null;
            this.deck = null;
            this.udeck = deck;
            hidden = false;

            if (this.title != null)
                this.title.text = deck.title;
            if (this.title != null)
                this.title.color = Color.white;
            if (this.value != null)
                this.value.text = deck.GetCost().ToString();
            if (this.value != null)
                this.value.enabled = deck.GetCost() > 0;
            if (value_img != null)
                value_img.enabled = value.enabled;
            if (this.foil != null)
                this.foil.enabled = false;
            if (warning != null)
                warning.enabled = !udata.IsDeckValid(deck);
        }

        public void SetLine(string title)
        {
            this.card = null;
            this.deck = null;
            this.udeck = null;
            hidden = false;

            if (this.title != null)
                this.title.text = title;
            if (this.title != null)
                this.title.color = Color.white;

            if (this.value != null)
                this.value.enabled = false;
            if (value_img != null)
                value_img.enabled = false;
            if (this.foil != null)
                this.foil.enabled = false;
            if (warning != null)
                warning.enabled = false;
            if (warning_gem != null)
                warning_gem.enabled = false;
        }

        public void Hide()
        {
            this.card = null;
            this.deck = null;
            this.udeck = null;
            hidden = true;
            hover = false;

            if (title != null)
                title.text = "";
            if (this.title != null)
                this.title.color = Color.white;
            if (value != null)
                value.text = "";
            if (value != null)
                value.enabled = true;
            if (value_img != null)
                value_img.enabled = false;
            if (cost != null)
                cost.value = 0;
            if (image != null)
                image.enabled = false;
            if (this.foil != null)
                this.foil.enabled = false;
            if (warning != null)
                warning.enabled = false;
            if (warning_gem != null)
                warning_gem.enabled = false;
            if (delete_btn != null)
                delete_btn.SetVisible(false);
        }

        public CardData GetCard()
        {
            return card;
        }

        public CardVariant GetVariant()
        {
            return variant;
        }

        public DeckData GetDeck()
        {
            return deck;
        }

        public UserDeckData GetUserDeck()
        {
            return udeck;
        }

        public bool IsHidden()
        {
            return hidden;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (hidden)
                return;

            if (eventData.button == PointerEventData.InputButton.Left)
            {
                onClick?.Invoke(this);
                AudioSystem.Get().PlaySFX("ui", click_audio);
            }

            if (eventData.button == PointerEventData.InputButton.Right)
            {
                onClickRight?.Invoke(this);
                AudioSystem.Get().PlaySFX("ui", click_audio);
            }
        }

        public void OnClickDelete()
        {
            onClickDelete?.Invoke(this);
            AudioSystem.Get().PlaySFX("ui", click_audio);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            hover = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            hover = false;
        }
    }
}