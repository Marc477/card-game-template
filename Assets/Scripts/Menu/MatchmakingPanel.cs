﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Client;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class MatchmakingPanel : UIPanel
    {

        private static MatchmakingPanel instance;

        protected override void Awake()
        {
            base.Awake();
            instance = this;
        }

        protected override void Start()
        {
            base.Start();

        }

        protected override void Update()
        {
            base.Update();

        }

        public void OnClickCancel()
        {
            GameClientMatchmaker.Get().CancelMatchmaking();
            Hide();
        }

        public static MatchmakingPanel Get()
        {
            return instance;
        }
    }
}