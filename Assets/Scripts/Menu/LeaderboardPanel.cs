﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{
    public class LeaderboardPanel : UIPanel
    {
        //public ScrollRect scroll_view;
        //public RectTransform scroll_content;

        public RankLine[] lines;
        public RankLine my_line;

        private static LeaderboardPanel instance;

        protected override void Awake()
        {
            base.Awake();
            instance = this;
            TabButton.onClickButton += OnClickTab;
            //lines = scroll_content.GetComponentsInChildren<RankLine>();

            foreach (RankLine line in lines)
                line.onClick += OnClickLine;
            my_line.onClick += OnClickLine;
        }

        private void OnDestroy()
        {
            TabButton.onClickButton -= OnClickTab;
        }

        private void RefreshPanel()
        {
            UserData udata = ApiManager.Get().GetUserData();

            my_line.Hide();
            foreach (RankLine line in lines)
                line.Hide();

            int index = 0;
            string url = ApiManager.server_url + "/users";
            ApiManager.Get().SendGetRequest(url, (WebResponse res) =>
            {
                ListResponse<UserData> users = ApiTool.JsonToArray<UserData>(res.body);
                List<UserData> sorted_users = new List<UserData>(users.list);
                sorted_users.Sort((UserData a, UserData b) => { return b.rank.CompareTo(a.rank); });

                string[] forbidden_array = new string[] { "admin", "website", "server" };
                List<string> forbidden = new List<string>(forbidden_array);

                int previous_rank = 0;
                int previous_index = 0;

                foreach (UserData user in sorted_users)
                {
                    if (forbidden.Contains(user.username) || user.matches == 0)
                        continue;

                    if (user.username == udata.username)
                    {
                        my_line.SetLine(user.username, user.rank, index + 1, true);
                    }

                    if (index < lines.Length)
                    {
                        RankLine line = lines[index];
                        int rank_order = (previous_rank == user.rank) ? previous_index : index;
                        line.SetLine(user.username, user.rank, rank_order + 1, user.username == udata.username);
                        previous_rank = user.rank;
                        previous_index = rank_order;
                    }

                    index++;
                }
            });
        }

        private void OnClickTab(TabButton btn)
        {
            if (btn.tab && btn.tab.tab_group == "menu")
                Hide();
        }

        private void OnClickLine(string username)
        {

        }

        public override void Show(bool instant = false)
        {
            base.Show(instant);
            RefreshPanel();
        }

        public void OnClickBack()
        {
            Hide();
        }

        public static LeaderboardPanel Get()
        {
            return instance;
        }
    }
}