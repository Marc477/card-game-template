﻿using CardGameTemplate.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class MainMenu : MonoBehaviour
    {
        public AudioClip music;
        public AudioClip ambience;

        [Header("Player UI")]
        public Text player_name;
        public Text player_credits;
        public GameObject loader;

        [Header("UI")]
        public TabPanel play_panel;
        public Text version_text;

        private static MainMenu instance;

        void Awake()
        {
            instance = this;
            PlayerData.LoadLast();

            //Set default settings
            TheGame.settings = GameSettings.Default;
            GameClient.online_mode = false;
        }

        private void Start()
        {
            BlackPanel.Get().Show(true);
            BlackPanel.Get().Hide();
            AudioSystem.Get().PlayMusic("music", music);
            AudioSystem.Get().PlaySFX("ambience", ambience, 0.5f, true, true);

            version_text.text = Application.version;

            if (ApiManager.Get().IsLoggedIn())
            {
                AfterLogin();
                return; //Already logged in
            }

            //Try auto login, otherwise go to login panel
            ApiManager.Get().RefreshLogin((bool success, LoginResponse res) =>
            {
                if (success)
                    AfterLogin();
                else
                    SceneNav.GoTo("LoginMenu");
            });
        }

        void Update()
        {
            UserData udata = ApiManager.Get().GetUserData();
            if (udata != null)
            {
                player_credits.text = TheUI.FormatCredits(udata.credits);
            }

            bool connected = ApiManager.Get().IsLoggedIn();
            ConnectionPanel.Get().SetVisible(!connected);

            bool matchmaking = GameClientMatchmaker.Get().IsMatchmaking();
            if (loader.activeSelf != matchmaking)
                loader.SetActive(matchmaking);

        }

        private void AfterLogin()
        {
            BlackPanel.Get().Hide();
            RefreshUserData();

            //Events
            GameClientMatchmaker matchmaker = GameClientMatchmaker.Get();
            matchmaker.onMatchingComplete += OnMatchmakingDone;

            //Friend list
            FriendPanel.Get().Show();
        }

        public void RefreshUserData()
        {
            ApiManager.Get().SendGetUserData((bool success, UserData user) =>
            {
                if (success)
                {
                    player_name.text = user.username;
                    player_credits.text = TheUI.FormatCredits(user.credits);
                }
            });
        }

        private void OnMatchmakingDone(bool success)
        {
            if (success)
            {
                BlackPanel.Get().Show();
            }

            MatchmakingPanel.Get().Hide();
        }

        public void OnClickTraining()
        {
            if (!ApiManager.Get().IsLoggedIn())
            {
                FadeToScene("LoginMenu");
                return;
            }

            TheGame.settings.play_mode = PlayMode.Training;
            GameClient.online_mode = false;
            SceneNav.GoTo("Game");
        }

        public void OnClickPvP()
        {
            if (!ApiManager.Get().IsLoggedIn())
            {
                FadeToScene("LoginMenu");
                return;
            }

            TheGame.settings.play_mode = PlayMode.Multiplayer;
            TheGame.settings.tournament = "";
            GameClient.online_mode = true;
            GameClientMatchmaker.Get().StartMatchmaking("");
			MatchmakingPanel.Get().Show();
        }

        public void OnClickTournament()
        {
            if (!ApiManager.Get().IsLoggedIn())
            {
                FadeToScene("LoginMenu");
                return;
            }

            TheGame.settings.play_mode = PlayMode.Multiplayer;
            TheGame.settings.tournament = "";
            GameClient.online_mode = true;
        }

        public void OnClickCancelMatch()
        {
            GameClientMatchmaker.Get().CancelMatchmaking();
        }

        public void FadeToScene(string scene)
        {
            StartCoroutine(FadeToRun(scene));
        }

        private IEnumerator FadeToRun(string scene)
        {
            BlackPanel.Get().Show();
            AudioSystem.Get().FadeOutMusic("music");
            yield return new WaitForSeconds(1f);
            SceneNav.GoTo(scene);
        }

        public void OnClickLogout()
        {
            ApiManager.Get().Logout();
            FadeToScene("LoginMenu");
        }

        public void OnClickQuit()
        {
            Application.Quit();
        }

        public static MainMenu Get()
        {
            return instance;
        }
    }
}
