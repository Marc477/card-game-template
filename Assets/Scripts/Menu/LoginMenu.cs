﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class LoginMenu : MonoBehaviour
    {
        [Header("Login")]
        public UIPanel login_panel;
        public InputField login_user;
        public InputField login_password;
        public Button login_button;
        public Text error_msg;

        [Header("Register")]
        public UIPanel register_panel;
        public InputField register_username;
        public InputField register_email;
        public InputField register_password;
        public InputField register_password_confirm;
        public Button register_button;

        [Header("Music")]
        public AudioClip music;

        private bool clicked = false;

        private static LoginMenu instance;

        void Awake()
        {
            instance = this;
            PlayerData.LoadLast();
        }

        private void Start()
        {
            AudioSystem.Get().PlayMusic("music", music);
            BlackPanel.Get().Show(true);
            error_msg.text = "";

            string user = PlayerPrefs.GetString("eoa_user", "");
            login_user.text = user;

            if (!string.IsNullOrEmpty(user))
                login_password.Select();

            ApiManager.Get().RefreshLogin((bool success, LoginResponse res) =>
            {
                if (success)
                {
                    SceneNav.GoTo("Menu");
                }
                else
                {
                    login_panel.Show();
                    BlackPanel.Get().Hide();
                }
            });
        }

        void Update()
        {
            login_button.interactable = !clicked && !string.IsNullOrWhiteSpace(login_user.text) && !string.IsNullOrWhiteSpace(login_password.text);
            register_button.interactable = !clicked && !string.IsNullOrWhiteSpace(register_username.text) && !string.IsNullOrWhiteSpace(register_email.text)
                && !string.IsNullOrWhiteSpace(register_password.text) && register_password.text == register_password_confirm.text;

            if (login_panel.IsVisible())
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    if (login_user.isFocused)
                        login_password.Select();
                    else
                        login_user.Select();
                }

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (login_button.interactable)
                        OnClickLogin();
                }
            }

            if (register_panel.IsVisible())
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    if (register_username.isFocused)
                        register_email.Select();
                    else if (register_email.isFocused)
                        register_password.Select();
                    else if (register_password.isFocused)
                        register_password_confirm.Select();
                    else
                        register_username.Select();
                }

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (register_button.interactable)
                        OnClickRegister();
                }
            }
        }

        public void OnClickLogin()
        {
            if (string.IsNullOrWhiteSpace(login_user.text))
                return;
            if (string.IsNullOrWhiteSpace(login_password.text))
                return;
            if (clicked)
                return;

            clicked = true;
            error_msg.text = "";

            ApiManager.Get().Login(login_user.text, login_password.text, (bool success, LoginResponse res) =>
            {
                if (success)
                {
                    PlayerPrefs.SetString("eoa_user", login_user.text);
                    FadeToScene("Menu");
                }
                else
                {
                    clicked = false;
                    error_msg.text = res.error;
                }
            });
        }

        public void OnClickRegister()
        {
            if (string.IsNullOrWhiteSpace(register_username.text))
                return;
            if (string.IsNullOrWhiteSpace(register_email.text))
                return;

            if (register_password.text != register_password_confirm.text)
                return;

            if (clicked)
                return;

            clicked = true;
            error_msg.text = "";

            ApiManager.Get().Register(register_email.text, register_username.text, register_password.text, (bool success, UserIdResponse res) =>
            {
                if (success)
                {
                    login_user.text = register_username.text;
                    login_password.text = register_password.text;
                    login_panel.Show();
                    register_panel.Hide();
                }
                else
                {
                    error_msg.text = res.error != null ? res.error : "Can't connect to server";
                }
                clicked = false;
            });
        }

        public void OnClickSwitchLogin()
        {
            login_panel.Show();
            register_panel.Hide();
            login_user.text = "";
            login_password.text = "";
            error_msg.text = "";
            login_user.Select();
        }

        public void OnClickSwitchRegister()
        {
            login_panel.Hide();
            register_panel.Show();
            register_username.Select();
            error_msg.text = "";
        }

        public void OnClickGo()
        {
            FadeToScene("Menu");
        }

        public void OnClickQuit()
        {
            Application.Quit();
        }

        public void FadeToScene(string scene)
        {
            StartCoroutine(FadeToRun(scene));
        }

        private IEnumerator FadeToRun(string scene)
        {
            BlackPanel.Get().Show();
            AudioSystem.Get().FadeOutMusic("music");
            yield return new WaitForSeconds(1f);
            SceneNav.GoTo(scene);
        }

        public static LoginMenu Get()
        {
            return instance;
        }
    }
}