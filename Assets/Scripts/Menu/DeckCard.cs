﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class DeckCard : MonoBehaviour, IPointerClickHandler
    {
        public Image image;
        public FontSprite attack;
        public FontSprite power;
        public Image team_power;
        public IconBar cost_icon;
        public IconValue rarity_icon;

        public Text card_title;
        public Text card_type;
        public Text card_effect;
        public Text card_quote;
        public Image quantity_bar;
        public Text quantity;

        public Material mat_default;
        public Material mat_notowned;

        public AudioClip click_audio;

        public UnityAction<DeckCard> onClick;
        public UnityAction<DeckCard> onClickRight;

        private CardData card;
        private CardVariant variant;

        void Start()
        {

        }

        void Update()
        {

        }

        public void SetCard(CardData card, CardVariant variant, int quantity)
        {
            this.card = card;
            this.variant = variant;
            image.sprite = card.GetSmallImage(variant);
            attack.value = card.attack;
            power.value = card.hp;
            attack.Refresh();
            power.Refresh();
            SetOpacity(1f);

            card_title.text = card.title.ToUpper();
            card_effect.text = card.card_text;
            card_quote.text = card.card_quote;
            card_type.text = card.GetTypeText();
            this.quantity.text = "x" + quantity.ToString();
            this.quantity.enabled = quantity > 1;
            quantity_bar.enabled = quantity > 1;

            TeamData tdata = TeamData.Get(card.team);
            team_power.sprite = tdata.team_effect;
            cost_icon.value = card.cost;
            cost_icon.Refresh();

            RarityData rdata = RarityData.Get(card.rarity);
            rarity_icon.value = rdata.rank - 1;
            rarity_icon.Refresh();

            Material mat = quantity > 0 ? mat_default : mat_notowned;
            image.material = mat;
            team_power.material = mat;
            team_power.material = mat;
            attack.SetMat(mat);
            power.SetMat(mat);
            cost_icon.SetMat(mat);
            rarity_icon.SetMat(mat);

            if (quantity == 0)
                card_effect.text = card_effect.text.Replace("#ffc500", "#ffffff");
        }

        public void SetOpacity(float opacity)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, opacity);
            if (attack != null)
                attack.SetOpacity(opacity);
            if (power != null)
                power.SetOpacity(opacity);
        }

        public CardData GetCard()
        {
            return card;
        }

        public CardVariant GetVariant()
        {
            return variant;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (onClick != null)
                    onClick.Invoke(this);
            }

            if (eventData.button == PointerEventData.InputButton.Right)
            {
                if (onClickRight != null)
                    onClickRight.Invoke(this);
            }

            AudioSystem.Get().PlaySFX("ui", click_audio);
        }
    }
}
