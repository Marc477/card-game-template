﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class FriendPanel : UIPanel
    {
        public FriendLine[] lines;

        [Header("Add")]
        public InputField add_field;
        public Text add_error;

        private float refresh_timer = 0f;

        public UnityAction<UserChallengesData> onChallenge;

        private static FriendPanel instance;

        protected override void Awake()
        {
            base.Awake();
            instance = this;

            foreach (FriendLine line in lines)
            {
                line.onClick += ClickFriend;
                line.onClickChallenge += ChallengeFriend;
                line.onClickDelete += DeleteFriend;
                line.Hide();
            }
        }

        protected override void Update()
        {
            base.Update();

            refresh_timer += Time.deltaTime;
            if (refresh_timer > 8f)
            {
                refresh_timer = 0f;
                RefreshPanel();
            }
        }

        private void RefreshPanel()
        {
            add_field.text = "";
            add_error.text = "";

            string url = ApiManager.server_url + "/users/friends/list/" + ApiManager.Get().GetUserID();
            ApiManager.Get().SendGetRequest(url, (WebResponse res) =>
            {
                if (!res.success)
                    return;

                foreach (FriendLine line in lines)
                    line.Hide();

                FriendListResponse list = ApiTool.JsonToObject<FriendListResponse>(res.body);
                DateTime server_time = DateTime.Parse(list.serverTime);

                int index = 0;
                foreach (FriendResponse friend in list.friends)
                {
                    if (index < lines.Length)
                    {
                        FriendLine line = lines[index];
                        DateTime login_time = DateTime.Parse(friend.lastLoginTime);
                        line.SetFriend(friend.username, friend.avatar, friend.xp, login_time.AddMinutes(10) > server_time);
                        index++;
                    }
                }
            });
        }

        public void AddFriend(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return;

            string url = ApiManager.server_url + "/users/friends/add/" + ApiManager.Get().GetUserID() + "/" + username;
            ApiManager.Get().SendPostRequest(url, (WebResponse res) =>
            {
                if (res.success)
                {
                    UserData udata = ApiManager.Get().GetUserData();
                    udata.AddFriend(username);
                    RefreshPanel();
                }
                else
                {
                    ErrorResponse error = ApiTool.JsonToObject<ErrorResponse>(res.body);
                    add_error.text = error.error;
                }
            });
        }

        private void ClickFriend(string username)
        {

        }

        private void ChallengeFriend(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return;

            string url = ApiManager.server_url + "/users/friends/challenge/" + ApiManager.Get().GetUserID() + "/" + username;
            ApiManager.Get().SendPostRequest(url, (WebResponse res) =>
            {
                if (res.success)
                {
                    onChallenge?.Invoke(ApiTool.JsonToObject<UserChallengesData>(res.body));
                    Hide();
                }
                else
                {
                    ErrorResponse error = ApiTool.JsonToObject<ErrorResponse>(res.body);
                    add_error.text = error.error;
                }
            });
        }

        public void DeleteFriend(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return;

            string url = ApiManager.server_url + "/users/friends/remove/" + ApiManager.Get().GetUserID() + "/" + username;
            ApiManager.Get().SendPostRequest(url, (WebResponse res) =>
            {
                if (res.success)
                {
                    UserData udata = ApiManager.Get().GetUserData();
                    udata.RemoveFriend(username);
                    RefreshPanel();
                }
            });
        }

        public void OnClickAdd()
        {
            add_error.text = "";
            string username = add_field.text;
            AddFriend(username);
        }

        public void OnClickBack()
        {
            Hide();
        }

        public void OnClickAddChallenge()
        {
            add_error.text = "";
            string username = add_field.text;
            ChallengeFriend(username);
        }

        public override void Show(bool instant = false)
        {
            base.Show(instant);
            RefreshPanel();
        }

        public static FriendPanel Get()
        {
            return instance;
        }
    }
}