﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using CardGameTemplate;

namespace CardGameTemplate.UI
{

    public class FriendLine : MonoBehaviour
    {
        public Text title;
        public Image avatar;
        public Image online;
        public Text level;
        public Button challenge_btn;
        public Button delete_btn;

        public Sprite online_sprite;
        public Sprite offline_sprite;

        public UnityAction<string> onClick;
        public UnityAction<string> onClickChallenge;
        public UnityAction<string> onClickDelete;

        private string username;
        private Sprite default_sprite;

        void Awake()
        {
            default_sprite = avatar.sprite;

            if (challenge_btn != null)
                challenge_btn.onClick.AddListener(OnClick);
            if (delete_btn != null)
                delete_btn.onClick.AddListener(OnDelete);
        }

        void Update()
        {

        }

        public void SetFriend(string username, string avatar, int xp, bool online)
        {
            this.username = username;
            this.title.text = username;
            AvatarData adata = AvatarData.Get(avatar);
            this.avatar.sprite = adata ? adata.avatar : default_sprite;
            this.online.sprite = online ? online_sprite : offline_sprite;
            level.text = "Lvl " + GameplayData.Get().GetPlayerLevel(xp);
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnClick()
        {
            onClick?.Invoke(username);
        }

        private void OnDelete()
        {
            onClickDelete?.Invoke(username);
        }

        public string GetUsername()
        {
            return username;
        }
    }
}