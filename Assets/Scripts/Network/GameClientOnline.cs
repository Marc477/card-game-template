﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DarkRift;
using DarkRift.Client;
using DarkRift.Client.Unity;

namespace CardGameTemplate.Client
{

    public class GameClientOnline
    {
        public int player_id = 0; //Player playing on this device;
        public UnityClient client;

        private bool connected = false;
        private bool was_connected = false;
        private Game game_data;
        private float auto_timer = 0f;

        private Queue<Message> message_queue = new Queue<Message>();

        public GameClientOnline(GameClient game_client)
        {
            client = game_client.GetComponent<UnityClient>();
            client.enabled = GameClient.online_mode;
            client.Client.MessageReceived += OnReceiveMsg;
            client.Client.Disconnected += OnDisconnect;
        }

        public void Connect()
        {
            ConnectToServer();
            ConnectToGame(GameClient.game_uid); //Connect to the game
        }

        public void ConnectToServer()
        {
            if (client.ConnectionState == ConnectionState.Connecting || client.ConnectionState == ConnectionState.Connected)
                return; // Already connected

            connected = false;

            try
            {
                //May freeze here, if so use ConnectInBackground + callback
                client.Connect(NetworkData.Get().url, NetworkData.Get().port, true);
            }
            catch (System.Exception e) { Debug.Log("Can't connect to server..." + e.Message); }
        }

        public void ConnectToGame(string uid)
        {
            if (client.ConnectionState != ConnectionState.Connected)
                return; //Not connected to server

            string username = ApiManager.Get().GetUsername();
            string token = ApiManager.Get().GetAccessToken();
            MsgPlayerConnect nplayer = new MsgPlayerConnect
            {
                username = username,
                token = token,
                game_uid = uid,
                joined = true,
                observer = TheGame.settings.play_mode == PlayMode.Observer,
            };
            Message msg = Message.Create(NetworkMsg.ConnectPlayer, nplayer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
        }

        private void AfterConnected(Game gdata, int player_id)
        {
            Debug.Log("Client connected");
            game_data = gdata;
            this.player_id = player_id;
            connected = true;
            was_connected = true;

            if (player_id < 0 || player_id >= 4)
            {
                GameClient.Get().SetObserveMode(0);
                GameClient.Get().SetObserverMode(TheGame.observe_user);
            }

            if (GameClient.Get().onConnected != null)
                GameClient.Get().onConnected.Invoke();
        }
        
        public void Update()
        {
            auto_timer += Time.deltaTime;
            if (auto_timer > 2f)
            {
                auto_timer = 0f;

                //Auto reconnect
                if (client.ConnectionState == ConnectionState.Disconnected || client.ConnectionState == ConnectionState.Interrupted)
                    ConnectToServer(); //Connect to server

                //Auto rejoin game (connected to server, but not connected to the game)
                if (!connected && client.ConnectionState == ConnectionState.Connected)
                    ConnectToGame(GameClient.game_uid); //Connect to the game
            }

            //Run messages
            if (message_queue.Count > 0) {
                Message msg = message_queue.Dequeue();
                if (msg != null)
                {
                    RunMessage(msg);
                    msg.Dispose();
                }
            }
        }

        private void OnReceiveMsg(object sender, MessageReceivedEventArgs e)
        {
            Message message = e.GetMessage();
            message_queue.Enqueue(message);
        }

        private void RunMessage(Message message) {

            DarkRiftReader reader = message.GetReader();

            if (message.Tag == NetworkMsg.ConnectPlayer)
            {
                MsgAfterConnected msg = reader.ReadSerializable<MsgAfterConnected>();
                AfterConnected(msg.game_data, msg.player_id);
            }

            //Events
            if (message.Tag == NetworkMsg.GameStart)
            {
                GameClient.Get().CallGameStart();
            }

            if (message.Tag == NetworkMsg.GameEnd)
            {
                GameClient.Get().CallGameEnd(reader.ReadInt32());
            }

            if (message.Tag == NetworkMsg.NewTurn)
            {
                GameClient.Get().CallNewTurn(reader.ReadInt32());
            }

            if (message.Tag == NetworkMsg.CardPlayed)
            {
                MsgPlayCard msg = reader.ReadSerializable<MsgPlayCard>();
                GameClient.Get().CallCardPlayed(msg.card_uid);
            }

            if (message.Tag == NetworkMsg.CardDiscarded)
            {
                MsgCard msg = reader.ReadSerializable<MsgCard>();
                GameClient.Get().CallCardDiscarded(msg.card_uid);
            }

            if (message.Tag == NetworkMsg.CardMoved)
            {
                MsgCard msg = reader.ReadSerializable<MsgCard>();
                GameClient.Get().CallCardMove(msg.card_uid);
            }

            if (message.Tag == NetworkMsg.AttackTarget)
            {
                MsgAttack msg = reader.ReadSerializable<MsgAttack>();
                GameClient.Get().CallAttackTarget(msg.attacker_uid, msg.target_uid);
            }

            if (message.Tag == NetworkMsg.AttackResolve)
            {
                MsgAttack msg = reader.ReadSerializable<MsgAttack>();
                GameClient.Get().CallAttackResolve(msg.attacker_uid, msg.target_uid);
            }

            if (message.Tag == NetworkMsg.AbilityTrigger)
            {
                MsgCastAbility msg = reader.ReadSerializable<MsgCastAbility>();
                GameClient.Get().CallAbilityTrigger(msg.ability_id, msg.caster_uid);
            }

            if (message.Tag == NetworkMsg.AbilityEffect)
            {
                MsgCastAbility msg = reader.ReadSerializable<MsgCastAbility>();
                GameClient.Get().CallAbilityEffect(msg.ability_id, msg.caster_uid, msg.target_uid);
            }

            if (message.Tag == NetworkMsg.AbilityAfter)
            {
                MsgCastAbility msg = reader.ReadSerializable<MsgCastAbility>();
                GameClient.Get().CallAbilityAfter(msg.ability_id, msg.caster_uid);
            }

            if (message.Tag == NetworkMsg.ChatMessage)
            {
                MsgChat msg = reader.ReadSerializable<MsgChat>();
                GameClient.Get().CallChatMsg(msg.player, msg.msg);
            }

            if (message.Tag == NetworkMsg.RefreshAllData)
            {
                MsgAllData msg = reader.ReadSerializable<MsgAllData>();
                game_data = msg.game_data;
                GameClient.Get().CallRefreshAll();
            }

            reader.Dispose();
        }

        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            connected = false;
            Debug.Log("Client disconnected " + e.Error);
        }

        public void Send(ushort tag)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            Message msg = Message.Create(tag, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        public void Send(ushort tag, DarkRiftWriter writer)
        {
            Message msg = Message.Create(tag, writer);
            Send(msg);
            msg.Dispose();
        }

        public void Send(ushort tag, IDarkRiftSerializable data)
        {
            Message msg = Message.Create<IDarkRiftSerializable>(tag, data);
            Send(msg);
            msg.Dispose();
        }

        public void Send(Message msg)
        {
            client.Client.SendMessage(msg, SendMode.Reliable);
        }

        //--------------------------

        public void SendPlayerSetting(string username, string deck)
        {
            MsgPlayerSettings mdata = new MsgPlayerSettings();
            mdata.username = username;
            mdata.deck = deck;
            mdata.ready = true;
            Send(NetworkMsg.PlayerSettings, mdata);
        }

        public void SendGameplaySettings(GameSettings settings)
        {
            Send(NetworkMsg.GameplaySettings, settings);
        }

        public void PlayCard(Card card, Slot slot)
        {
            MsgPlayCard mdata = new MsgPlayCard();
            mdata.card_uid = card.uid;
            mdata.slot_x = slot.x;
            mdata.slot_p = slot.p;
            Send(NetworkMsg.PlayCard, mdata);
        }

        public void AttackTarget(Card card, Card target)
        {
            MsgAttack mdata = new MsgAttack();
            mdata.attacker_uid = card.uid;
            mdata.target_uid = target.uid;
            Send(NetworkMsg.Attack, mdata);
        }

        public void Move(Card card, Slot slot)
        {
            MsgPlayCard mdata = new MsgPlayCard();
            mdata.card_uid = card.uid;
            mdata.slot_x = slot.x;
            mdata.slot_p = slot.p;
            Send(NetworkMsg.Move, mdata);
        }

        public void CastCardAbility(Card card, AbilityData ability)
        {
            MsgCastAbility mdata = new MsgCastAbility();
            mdata.caster_uid = card.uid;
            mdata.ability_id = ability.id;
            mdata.target_uid = "";
            Send(NetworkMsg.CastAbility, mdata);
        }

        public void SelectCard(Card card)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(card.uid);
            Send(NetworkMsg.SelectCard, writer);
            writer.Dispose();
        }

        public void SelectSlot(Slot slot)
        {
            MsgSlot mdata = new MsgSlot();
            mdata.slot_x = slot.x;
            mdata.slot_p = slot.p;
            Send(NetworkMsg.SelectSlot, mdata);
        }

        public void SelectChoice(int choice)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(choice);
            Send(NetworkMsg.SelectChoice, writer);
            writer.Dispose();
        }

        public void CancelSelection()
        {
            Send(NetworkMsg.CancelSelect);
        }

        public void SendChatMsg(string msg)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(msg);
            Send(NetworkMsg.ChatMessage, writer);
            writer.Dispose();
        }

        public void NextStep()
        {
            Send(NetworkMsg.NextStep);
        }

        public void Resign()
        {
            Send(NetworkMsg.Resign);
        }

        //--------------------------

        public bool IsConnected()
        {
            return connected && client.ConnectionState == ConnectionState.Connected;
        }

        public bool WasConnected()
        {
            return was_connected;
        }

        public int GetPlayerID()
        {
            return player_id;
        }

        public int GetOpponentPlayerID()
        {
            return player_id == 0 ? 1 : 0;
        }

        public Game GetGameData()
        {
            return game_data;
        }
    }

}