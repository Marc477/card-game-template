﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DarkRift;
using DarkRift.Server;
using DarkRift.Server.Unity;
using UnityEngine.Events;

namespace CardGameTemplate.Server
{
    
    public class ClientData
    {
        public ushort client_id; //index of the connection
        public string username; //Player unique id, example username
        public string game_uid; //Unique id for the game

        public ClientData(ushort id) { client_id = id; }
        public bool IsInGame() { return !string.IsNullOrEmpty(game_uid); }
    }

    public class ServerManager : MonoBehaviour
    {
        [Header("API")]
        public string api_username;
        public string api_password;

        public const string version = "0.01";

        private XmlUnityServer server;

        private Dictionary<ushort, ClientData> client_list = new Dictionary<ushort, ClientData>();  //List of clients
        private Dictionary<string, GameServer> game_list = new Dictionary<string, GameServer>(); //List of games
        private HashSet<string> connected_users = new HashSet<string>(); //List of connected player users
        private List<string> game_remove_list = new List<string>();
        private bool try_login = false;

        private static ServerManager _instance;

        void Awake()
        {
            _instance = this;
            server = GetComponent<XmlUnityServer>();
            Application.runInBackground = true;
            Application.targetFrameRate = 60; //Slow down server
        }

        private void Start()
        {
            server.Server.ClientManager.ClientConnected += ClientConnected;
            server.Server.ClientManager.ClientDisconnected += ClientDisconnected;

            Debug.Log("Server listening on " + server.Server.ClientManager.Port);

            //Connect to API
            ApiManager.Get().Login(api_username, api_password, (bool success, LoginResponse res) =>
            {
                Debug.Log("Server api login: " + success);
            });

            ApiManager.Get().onLoginAttempt += AfterLoginAttempt;
        }

        void Update()
        {
            //Update games and Destroy games with no players
            foreach (KeyValuePair<string, GameServer> pair in game_list)
            {
                GameServer gserver = pair.Value;
                gserver.Update();

                if (gserver.IsGameExpired())
                    game_remove_list.Add(pair.Key);
            }

            foreach (string key in game_remove_list)
            {
                game_list.Remove(key);

                if (ServerMatchmaker.Get())
                    ServerMatchmaker.Get().EndMatch(key);
            }
            game_remove_list.Clear();
        }

        private void AfterLoginAttempt(bool success, string error)
        {
            Debug.Log("Server api login: " + success);

            //If auto-refresh fail, login again
            if (!success && !try_login)
            {
                try_login = true;
                NetworkTool.WaitFor(5f, () =>
                {
                    ApiManager.Get().Login(api_username, api_password, (bool asuccess, LoginResponse res) =>
                    {
                        try_login = false;
                    });
                });
            }
        }

        private void ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            ClientData iclient = new ClientData(e.Client.ID);
            client_list[e.Client.ID] = iclient;
            e.Client.MessageReceived += ReceiveMessage;
        }

        private void ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            ClientData iclient = GetClient(e.Client.ID);
            client_list.Remove(e.Client.ID);
            e.Client.MessageReceived -= ReceiveMessage;
            ReceiveDisconnectPlayer(iclient);
        }

        private void ReceiveMessage(object sender, MessageReceivedEventArgs e)
        {
            ushort client_id = e.Client.ID;
            Message message = e.GetMessage();
            ClientData iclient = GetClient(client_id);
            if (iclient != null && message != null)
            {
                //Connection msg
                if (message.Tag == NetworkMsg.ConnectPlayer)
                {
                    ReceiveConnectPlayer(iclient, message);
                    message.Dispose();
                }

                //Game msg
                else if (iclient.IsInGame())
                {
                    //Call command
                    GameServer gserver = GetGame(iclient.game_uid);
                    if (gserver != null && gserver.IsPlayer(iclient.username))
                        gserver.ReceiveMsg(iclient, message);
                }
                else
                {
                    message.Dispose();
                }
            }
        }

        public void ReceiveConnectPlayer(ClientData iclient, Message message)
        {
            DarkRiftReader reader = message.GetReader();
            MsgPlayerConnect msg = reader.ReadSerializable<MsgPlayerConnect>();
            reader.Dispose();

            if (iclient != null)
            {
                if (string.IsNullOrWhiteSpace(msg.username))
                    return;

                if (string.IsNullOrWhiteSpace(msg.game_uid))
                    return;

                //Find game uid from game code
                if (msg.observer)
                    ConnectObserverToGame(iclient, msg.username, msg.game_uid);
                else
                    ConnectPlayerToGame(iclient, msg.username, msg.game_uid, msg.joined);

                GameServer gserver = GetGame(iclient.game_uid);
                if(gserver != null)
                    gserver.CallRefreshAll();
            }
        }

        private void ReceiveDisconnectPlayer(ClientData iclient)
        {
            if (iclient == null)
                return;

            connected_users.Remove(iclient.username);

            GameServer gserver = GetGame(iclient.game_uid);
            if (gserver != null)
            {
                int player_id = gserver.GetPlayerID(iclient.username);
                Player player = gserver.GetGameData().GetPlayer(player_id);
                if (player != null)
                {
                    player.connected = false;

                    DarkRiftWriter writer = DarkRiftWriter.Create();
                    writer.Write(iclient.username);
                    SendToAll(gserver.game_uid, NetworkMsg.PlayerLeft, writer);
                    writer.Dispose();

                    gserver.CallRefreshAll();
                }
            }
        }

        private void ConnectObserverToGame(ClientData iclient, string player_user, string game_uid)
        {
            GameServer gserver = GetGame(game_uid);
            if (gserver != null && iclient != null)
            {
                //Add player to game
                iclient.game_uid = gserver.game_uid;
                iclient.username = player_user;

                if (!connected_users.Contains(player_user))
                    connected_users.Add(player_user);

                //Return request
                MsgAfterConnected msg_data = new MsgAfterConnected();
                msg_data.success = true;
                msg_data.player_id = -1;
                msg_data.game_data = gserver.GetGameData();
                SendToClient(iclient.client_id, NetworkMsg.ConnectPlayer, msg_data);
            }
        }

        private void ConnectPlayerToGame(ClientData iclient, string player_user, string game_uid, bool joined)
        {
            //Get or Create game
            GameServer gserver = GetGame(game_uid);

            if (gserver == null)
                gserver = CreateGame(game_uid);

            bool can_connect = gserver.IsPlayer(player_user) || gserver.CountPlayers() < 2;
            if (gserver != null && iclient != null && can_connect)
            {
                //Add player to game
                iclient.game_uid = gserver.game_uid;
                iclient.username = player_user;

                int player_id = gserver.AddPlayer(player_user);
                if (!connected_users.Contains(player_user))
                    connected_users.Add(player_user);

                Player player = gserver.GetGameData().GetPlayer(player_id);
                if (player != null)
                {
                    player.username = player_user;
                    player.connected = true;
                }

                //Return request
                MsgAfterConnected msg_data = new MsgAfterConnected();
                msg_data.success = true;
                msg_data.player_id = player_id;
                msg_data.game_data = gserver.GetGameData();
                SendToClient(iclient.client_id, NetworkMsg.ConnectPlayer, msg_data);

                DarkRiftWriter writer = DarkRiftWriter.Create();
                writer.Write(player_user);
                SendToAll(game_uid, NetworkMsg.PlayerJoined, writer);
                writer.Dispose();
            }
        }

        public void SendToClient(ushort client_id, ushort tag, IDarkRiftSerializable data)
        {
            Message msg = Message.Create<IDarkRiftSerializable>(tag, data);
            SendToClient(client_id, msg);
            msg.Dispose();
        }

        public void SendToClient(ushort client_id, Message msg)
        {
            IClient client = server.Server.ClientManager.GetClient(client_id);
            if (client != null)
                client.SendMessage(msg, SendMode.Reliable);
        }

        public void SendToAll(string game_uid, ushort tag)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            SendToAll(game_uid, tag, writer);
            writer.Dispose();
        }

        public void SendToAll(string game_uid, ushort tag, DarkRiftWriter writer)
        {
            Message msg = Message.Create(tag, writer);
            SendToAll(game_uid, msg);
            msg.Dispose();
        }

        public void SendToAll(string game_uid, ushort tag, IDarkRiftSerializable data)
        {
            Message msg = Message.Create<IDarkRiftSerializable>(tag, data);
            SendToAll(game_uid, msg);
            msg.Dispose();
        }

        public void SendToAll(string game_uid, Message msg)
        {
            foreach (IClient client in server.Server.ClientManager.GetAllClients())
            {
                ClientData iclient = GetClient(client.ID);
                if (iclient != null)
                {
                    if (iclient.game_uid == game_uid)
                        client.SendMessage(msg, SendMode.Reliable);
                }
            }
        }

        public GameServer CreateGame(string uid)
        {
            GameServer game = new GameServer(uid);
            game_list[game.game_uid] = game;
            return game;
        }

        public void RemoveGame(string game_id)
        {
            game_list.Remove(game_id);
        }

        public GameServer GetGame(string game_uid)
        {
            if (string.IsNullOrEmpty(game_uid))
                return null;
            if (game_list.ContainsKey(game_uid))
                return game_list[game_uid];
            return null;
        }

        public ClientData GetClient(ushort client_id)
        {
            if (client_list.ContainsKey(client_id))
                return client_list[client_id];
            return null;
        }

        public ClientData GetClientByUser(string username)
        {
            foreach (KeyValuePair<ushort, ClientData> pair in client_list)
            {
                if (pair.Value.username == username)
                    return pair.Value;
            }
            return null;
        }

        public bool IsClientConnected(string username) {
            return connected_users.Contains(username);
        }

        public static ServerManager Get()
        {
            return _instance;
        }
    }

}