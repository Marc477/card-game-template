﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Client.Unity;
using DarkRift;
using DarkRift.Client;
using UnityEngine.Events;
using CardGameTemplate.UI;

namespace CardGameTemplate.Client
{
    public class GameClientMatchmaker : MonoBehaviour
    {
        public UnityAction<bool> onMatchingComplete;
        public UnityAction<MatchmakingList> onMatchmakingList;
        public UnityAction<MatchList> onMatchList;
        public UnityAction<MsgChatGlobal> onChat;
        public UnityAction onGameReady;

        private UnityClient client;
        private bool connecting = false;
        private bool matchmaking = false;
        private bool joining = false;
        private float timer = 0f;
        private float match_timer = 0f;
        private string matchmaking_group;

        private Queue<Message> message_queue = new Queue<Message>();

        private static GameClientMatchmaker _instance;

        void Awake()
        {
            _instance = this;
            client = GetComponent<UnityClient>();
        }

        private void Start()
        {
            client.Client.MessageReceived += OnReceiveMsg;
            client.Client.Disconnected += OnDisconnect;
        }

        void Update()
        {
            timer += Time.deltaTime;
            match_timer += Time.deltaTime;

            //Send periodic request
            if (IsConnected() && matchmaking)
            {
                if (timer > 2f)
                {
                    timer = 0f;
                    SendMatchRequest(true, matchmaking_group);
                }
            }

            //Disconnected, try reconnect
            if (!IsConnected() && !connecting && timer > 5f)
            {
                timer = 0f;
                Connect(NetworkData.Get().url, NetworkData.Get().port);
            }

            //Run messages
            if (message_queue.Count > 0)
            {
                Message msg = message_queue.Dequeue();
                if (msg != null)
                {
                    RunMessage(msg);
                    msg.Dispose();
                }
            }
        }

        public void StartMatchmaking(string group)
        {
            //if (GameClientLobby.Get().IsActive())
            //    return;

            Debug.Log("Start Matchmaking!");
            matchmaking_group = group;
            matchmaking = true;
            joining = false;
            match_timer = 0f;
            timer = 0f;

            Connect(NetworkData.Get().url, NetworkData.Get().port, (bool success) =>
            {
                if (success)
                {
                    SendMatchRequest(false, group);
                }
                else
                {
                    StopMatchmaking();
                }
            });
        }

        public void CancelMatchmaking()
        {
            Debug.Log("Cancel Matchmaking!");
            StopMatchmaking();
        }

        private void StopMatchmaking()
        {
            onMatchingComplete?.Invoke(false);
            matchmaking_group = "";
            matchmaking = false;
            joining = false;
            timer = 0f;
        }

        public void RefreshMatchmakingList()
        {
            Connect(NetworkData.Get().url, NetworkData.Get().port, (bool success) =>
            {
                if(success)
                    SendMatchmakingListRequest();
            });
        }

        public void RefreshMatchList(string username)
        {
            Connect(NetworkData.Get().url, NetworkData.Get().port, (bool success) =>
            {
                if (success)
                    SendMatchListRequest(username);
            });
        }

        public void Connect(string url, int port, UnityAction<bool> callback=null)
        {
            //Must be logged in to API to connect
            if(!ApiManager.Get().IsLoggedIn())
            {
                callback?.Invoke(false);
                return;
            }

            //Check if already connected
            if (IsConnected())
            {
                callback?.Invoke(true);
                return;
            }

            //Connect to new server
            connecting = true;
            client.ConnectInBackground(url, port, true, (System.Exception e) =>
            {
                bool connected = client.ConnectionState == ConnectionState.Connected;
                connecting = false;
                Debug.Log("Connected to server: " + connected);
                callback?.Invoke(connected);
            });
        }

        public void Disconnect()
        {
            message_queue.Clear();
            timer = 0f;
            if (client.ConnectionState == ConnectionState.Connected)
            {
                Debug.Log("Disconnecting...");
                client.Disconnect();
            }
        }

        public void JoinGame(string uid, string group)
        {
            if (!joining)
            {
                Debug.Log("Start Connecting!");
                joining = true;
                GameClient.online_mode = true;
                GameClient.game_uid = uid;
                SendConnectRequest(uid);
                SendGameSettings(TheGame.settings);
                StartGame();
            }
        }

        private void StartGame()
        {
			if (joining)
            {
				//If changing scene, need to receive RefreshData instead and update this
                joining = false;
				MainMenu.Get().FadeToScene(TheGame.settings.scene);
				onGameReady?.Invoke();
			}
        }

        private void SendMatchRequest(bool refresh, string group)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            MsgMatchmaking msg_match = new MsgMatchmaking();
            UserData udata = ApiManager.Get().GetUserData();
            msg_match.username = ApiManager.Get().GetUsername();
            msg_match.group = group;
            msg_match.rank = udata.rank;
            msg_match.time = match_timer;
            msg_match.refresh = refresh;
            writer.Write(msg_match);
            Message msg = Message.Create(NetworkMsg.Matchmaking, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        private void SendMatchmakingListRequest()
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            MsgMatchmakingList msg_match = new MsgMatchmakingList();
            msg_match.username = ""; //Return all users
            writer.Write(msg_match);
            Message msg = Message.Create(NetworkMsg.MatchmakingList, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        private void SendMatchListRequest(string username)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            MsgMatchmakingList msg_match = new MsgMatchmakingList();
            msg_match.username = username;
            writer.Write(msg_match);
            Message msg = Message.Create(NetworkMsg.MatchList, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        public void SendConnectRequest(string uid)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            MsgPlayerConnect msg_connect = new MsgPlayerConnect();
            msg_connect.username = ApiManager.Get().GetUsername();
            msg_connect.token = ApiManager.Get().GetAccessToken();
            msg_connect.game_uid = uid;
            msg_connect.joined = false;
            msg_connect.observer = false;
            writer.Write(msg_connect);
            Message msg = Message.Create(NetworkMsg.ConnectPlayer, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        public void SendGameSettings(GameSettings settings)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(settings);
            Message msg = Message.Create(NetworkMsg.GameplaySettings, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        public void SendPlayerSettings(string username, string deck, bool ready)
        {
            MsgPlayerSettings mdata = new MsgPlayerSettings();
            mdata.username = username;
            mdata.deck = deck;
            mdata.ready = ready;
            Message msg = Message.Create(NetworkMsg.PlayerSettings, mdata);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
        }

        private void OnReceiveMsg(object sender, MessageReceivedEventArgs e)
        {
            if (!IsConnected())
                return;

            Message message = e.GetMessage();
            if(message != null)
                message_queue.Enqueue(message);
        }

        private void RunMessage(Message message)
        {
            DarkRiftReader reader = message.GetReader();
            if (message.Tag == NetworkMsg.Matchmaking)
            {
                MsgAfterMatchmaking msg = reader.ReadSerializable<MsgAfterMatchmaking>();
                ReceiveMatchmaking(msg.success, msg.server_url, msg.game_uid);
            }

            if (message.Tag == NetworkMsg.MatchmakingList)
            {
                MatchmakingList list = reader.ReadSerializable<MatchmakingList>();
                ReceiveMatchmakingList(list);
            }

            if (message.Tag == NetworkMsg.MatchList)
            {
                MatchList list = reader.ReadSerializable<MatchList>();
                ReceiveMatchList(list);
            }

            if (message.Tag == NetworkMsg.RefreshAllData)
            {
                MsgAllData msg = reader.ReadSerializable<MsgAllData>();
                ReceiveRefreshSettings(msg.game_data);
            }

            if (message.Tag == NetworkMsg.GameReady)
            {
                ReceiveGameReady();
            }

            reader.Dispose();
        }

        private void ReceiveMatchmaking(bool success, string server_url, string game_uid)
        {
            if (IsConnected() && matchmaking)
            {
                string group = matchmaking_group;
                matchmaking = false;

                Debug.Log("Matchmaking found: " + success + " " + server_url + "/" + game_uid);
                onMatchingComplete?.Invoke(success);
                matchmaking = false;
                timer = 0f;

                //if (success)
                //    GameClientLobby.Get().JoinGame(game_uid, group);
                JoinGame(game_uid, group);
            }
        }

        private void ReceiveMatchmakingList(MatchmakingList list)
        {
            onMatchmakingList?.Invoke(list);
        }

        private void ReceiveMatchList(MatchList list)
        {
            onMatchList?.Invoke(list);
        }

        private void ReceiveRefreshSettings(Game game)
        {
            //game_data = game;
            //onReceiveSettings?.Invoke(game.settings);
            //onReceiveGameInfo?.Invoke(game);

            if (game.current_state != GameState.Connecting)
                StartGame();
        }

        private void ReceiveGameReady()
        {
            StartGame();
        }

        private void ReceiveChat(MsgChatGlobal chat)
        {
            onChat?.Invoke(chat);
        }

        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            Debug.Log("Disconnected from server!");
        }

        public bool IsMatchmaking()
        {
            return matchmaking;
        }

        public string GetGroup()
        {
            return matchmaking_group;
        }

        public float GetTimer()
        {
            return match_timer;
        }

        public bool IsConnected()
        {
            return client.ConnectionState == ConnectionState.Connected;
        }

        public static GameClientMatchmaker Get()
        {
            return _instance;
        }
    }

}