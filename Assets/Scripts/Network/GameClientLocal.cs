﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGameTemplate.Gameplay;

namespace CardGameTemplate.Client
{
    public class GameClientLocal
    {
        public int player_id = 0; //Player playing on this device;

        private bool connected = false;
        private GameplayLogic gameplay;

        public GameClientLocal(GameClient game_client)
        {
            gameplay = new GameplayLogic();
            gameplay.CreateNewGame("local", 1);

            //Events
            gameplay.onGameStart += GameClient.Get().CallGameStart;
            gameplay.onGameEnd += GameClient.Get().CallGameEnd;
            gameplay.onNewTurn += GameClient.Get().CallNewTurn;
            gameplay.onCardPlayed += GameClient.Get().CallCardPlayed;
            gameplay.onCardDiscarded += GameClient.Get().CallCardDiscarded;
            gameplay.onCardMove += GameClient.Get().CallCardMove;

            gameplay.onAttackTarget += GameClient.Get().CallAttackTarget;
            gameplay.onAttackAfter += GameClient.Get().CallAttackResolve;

            gameplay.onAbilityTrigger += GameClient.Get().CallAbilityTrigger;
            gameplay.onAbilityEffect += GameClient.Get().CallAbilityEffect;
            gameplay.onAbilityAfter += GameClient.Get().CallAbilityAfter;

            gameplay.onRefreshAllData += GameClient.Get().CallRefreshAll;
        }

        public void Connect()
        {
            GameTool.StartCoroutine(ConnectRoutine()); //Wait one sec to simulate connection time and let the server initialize
        }

        private IEnumerator ConnectRoutine()
        {
            yield return new WaitForSeconds(1f);
            AfterConnect();
        }

        public void AfterConnect()
        {
            connected = true;

            if (GameClient.Get().onConnected != null)
                GameClient.Get().onConnected.Invoke();
        }

        public void Update()
        {
            gameplay.Update();
        }

        //--------------------------

        public void SendPlayerSettings(int player_id, string username, string deck)
        {
            gameplay.SetPlayerName(player_id, username);
            gameplay.SetPlayerDeck(player_id, username, deck);
            gameplay.SetPlayerReady(player_id, true);
            gameplay.SetPlayerConnected(player_id, true);
        }

        public void SendPlayerSettings(int player_id, string name, string deck, CardData[] cards)
        {
            gameplay.SetPlayerName(player_id, name);
            gameplay.SetPlayerDeck(player_id, deck, cards);
            gameplay.SetPlayerReady(player_id, true);
            gameplay.SetPlayerConnected(player_id, true);
        }

        public void SendAIPlayerSettings(int player_id, string name, string deck, CardData[] cards, int ai_level)
        {
            gameplay.SetPlayerName(player_id, name);
            gameplay.SetPlayerDeck(player_id, deck, cards);
            gameplay.SetAiLevel(player_id, ai_level);
            gameplay.SetPlayerReady(player_id, true);
            gameplay.SetPlayerConnected(player_id, true);
        }

        public void SendGameplaySettings(GameSettings settings)
        {
            gameplay.SetGameplaySettings(settings);
        }

        public void SelectDraft(Card card)
        {
            //gameplay.SelectDraft(player_id, card.uid);
        }

        public void PlayCard(Card card, Slot slot)
        {
            gameplay.TryPlayCard(player_id, card.uid, slot.x, slot.p);
        }

        public void AttackTarget(Card card, Card target)
        {
            gameplay.AttackTarget(player_id, card.uid, target.uid);
        }

        public void Move(Card card, Slot slot)
        {
            gameplay.MoveCard(player_id, card.uid, slot.x, slot.p);
        }

        public void CastCardAbility(Card card, AbilityData ability)
        {
            if(card != null && ability != null)
                gameplay.CastCardAbility(player_id, card.uid, ability.id);
        }

        public void SelectCard(Card card)
        {
            gameplay.SelectCard(player_id, card.uid);
        }

        public void SelectSlot(Slot slot)
        {
            gameplay.SelectSlot(player_id, slot.x, slot.p);
        }

        public void SelectChoice(int choice)
        {
            gameplay.SelectChoice(player_id, choice);
        }

        public void CancelSelection()
        {
            gameplay.CancelSelection(player_id);
        }

        public void NextStep()
        {
            gameplay.NextStep(player_id);
        }

        public void Resign()
        {
            gameplay.Resign(player_id);
        }

        //--------------------------

        public bool IsConnected()
        {
            return connected;
        }

        public int GetPlayerID()
        {
            return player_id;
        }

        public int GetOpponentPlayerID()
        {
            return player_id == 0 ? 1 : 0;
        }

        public Game GetGameData()
        {
            return gameplay.GetGameData();
        }
    }

}