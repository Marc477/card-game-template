﻿using DarkRift;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using CardGameTemplate.Gameplay;


namespace CardGameTemplate.Server
{
    public class CommandEvent
    {
        public ushort tag;
        public UnityAction<ClientData, DarkRiftReader> callback;
    }

    public struct ReceivedMessage
    {
        public ClientData iclient;
        public Message message;
    }

    public class GameServer
    {
        public string game_uid; //Game unique ID
        public List<string> players = new List<string>();

        private GameplayLogic gameplay;
        private int nb_players = 2;
        private float expiration = 0f;

        private Dictionary<ushort, CommandEvent> registered_commands = new Dictionary<ushort, CommandEvent>();

        private Queue<ReceivedMessage> message_queue = new Queue<ReceivedMessage>();

        public GameServer(string uid)
        {
            game_uid = uid;
            gameplay = new GameplayLogic();
            gameplay.CreateNewGame(game_uid, nb_players);

            //Commands
            RegisterCommand(NetworkMsg.PlayerSettings, ReceivePlayerSettings);
            RegisterCommand(NetworkMsg.GameplaySettings, ReceiveGameplaySettings);
            RegisterCommand(NetworkMsg.PlayCard, ReceivePlayCard);
            RegisterCommand(NetworkMsg.Attack, ReceiveAttackTarget);
            RegisterCommand(NetworkMsg.Move, ReceiveMove);
            RegisterCommand(NetworkMsg.CastAbility, ReceiveCastCardAbility);
            RegisterCommand(NetworkMsg.SelectCard, ReceiveSelectCard);
            RegisterCommand(NetworkMsg.SelectSlot, ReceiveSelectSlot);
            RegisterCommand(NetworkMsg.SelectChoice, ReceiveSelectChoice);
            RegisterCommand(NetworkMsg.CancelSelect, ReceiveCancelSelection);
            RegisterCommand(NetworkMsg.NextStep, ReceiveEndTurn);
            RegisterCommand(NetworkMsg.Resign, ReceiveResign);
            RegisterCommand(NetworkMsg.ChatMessage, ReceiveChat);

            //Events
            gameplay.onReady += CallGameReady;
            gameplay.onGameStart += CallGameStart;
            gameplay.onGameEnd += CallGameEnd;
            gameplay.onNewTurn += CallNewTurn;

            gameplay.onCardPlayed += CallCardPlayed;
            gameplay.onCardDiscarded += CallCardDiscarded;
            gameplay.onCardMove += CallCardMoved;

            gameplay.onAbilityTrigger += CallAbilityTrigger;
            gameplay.onAbilityEffect += CallAbilityEffect;
            gameplay.onAbilityAfter += CallAfterAbility;
            
            gameplay.onAttackTarget += CallAttackTarget;
            gameplay.onAttackAfter += CallAttackResolve;
            
            gameplay.onRefreshAllData += CallRefreshAll;
        }

        ~GameServer()
        {
            gameplay.onReady -= CallGameReady;
            gameplay.onGameStart -= CallGameStart;
            gameplay.onGameEnd -= CallGameEnd;
            gameplay.onNewTurn -= CallNewTurn;

            gameplay.onCardPlayed -= CallCardPlayed;
            gameplay.onCardDiscarded -= CallCardDiscarded;
            gameplay.onCardMove -= CallCardMoved;

            gameplay.onAbilityTrigger -= CallAbilityTrigger;
            gameplay.onAbilityEffect -= CallAbilityEffect;
            gameplay.onAbilityAfter -= CallAfterAbility;

            gameplay.onAttackTarget -= CallAttackTarget;
            gameplay.onAttackAfter -= CallAttackResolve;

            gameplay.onRefreshAllData -= CallRefreshAll;
        }

        public void Update()
        {
            gameplay.Update();

            if (HasGameEnded() || CountConnectedPlayers() == 0)
                expiration += Time.deltaTime;

            //Run messages
            if (message_queue.Count > 0)
            {
                ReceivedMessage msg = message_queue.Dequeue();
                RunMessage(msg.iclient, msg.message);
                msg.message.Dispose();
            }
        }

        private void RegisterCommand(ushort tag, UnityAction<ClientData, DarkRiftReader> callback)
        {
            CommandEvent cmdevt = new CommandEvent();
            cmdevt.tag = tag;
            cmdevt.callback = callback;
            registered_commands.Add(tag, cmdevt);
        }

        public void ReceiveMsg(ClientData iclient, Message message)
        {
            ReceivedMessage msg = new ReceivedMessage();
            msg.iclient = iclient;
            msg.message = message;
            message_queue.Enqueue(msg);
            expiration = 0f;
        }

        private void RunMessage(ClientData iclient, Message message)
        {
            CommandEvent command;
            bool found = registered_commands.TryGetValue(message.Tag, out command);
            if (found)
            {
                DarkRiftReader reader = message.GetReader();
                command.callback.Invoke(iclient, reader);
                reader.Dispose();
            }
        }

        public void ReceivePlayerSettings(ClientData iclient, DarkRiftReader reader)
        {
            MsgPlayerSettings msg = reader.ReadSerializable<MsgPlayerSettings>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
            {
                gameplay.SetPlayerName(player_id, msg.username);
                gameplay.SetPlayerDeck(player_id, msg.username, msg.deck);
                gameplay.SetPlayerReady(player_id, msg.ready);
                gameplay.RefreshData();
            }
        }

        public void ReceiveGameplaySettings(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
            {
                GameSettings settings = reader.ReadSerializable<GameSettings>();
                gameplay.SetGameplaySettings(settings);
                gameplay.RefreshData();
            }
        }

        public void ReceivePlayCard(ClientData iclient, DarkRiftReader reader)
        {
            MsgPlayCard msg = reader.ReadSerializable<MsgPlayCard>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.TryPlayCard(player_id, msg.card_uid, msg.slot_x, msg.slot_p);
        }

        public void ReceiveAttackTarget(ClientData iclient, DarkRiftReader reader)
        {
            MsgAttack msg = reader.ReadSerializable<MsgAttack>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.AttackTarget(player_id, msg.attacker_uid, msg.target_uid);
        }

        public void ReceiveMove(ClientData iclient, DarkRiftReader reader)
        {
            MsgPlayCard msg = reader.ReadSerializable<MsgPlayCard>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.MoveCard(player_id, msg.card_uid, msg.slot_x, msg.slot_p);
        }

        public void ReceiveCastCardAbility(ClientData iclient, DarkRiftReader reader)
        {
            MsgCastAbility msg = reader.ReadSerializable<MsgCastAbility>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.CastCardAbility(player_id, msg.caster_uid, msg.ability_id);
        }

        public void ReceiveSelectCard(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.SelectCard(player_id, reader.ReadString());
        }

        public void ReceiveSelectSlot(ClientData iclient, DarkRiftReader reader)
        {
            MsgSlot msg = reader.ReadSerializable<MsgSlot>();
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.SelectSlot(player_id, msg.slot_x, msg.slot_p);
        }

        public void ReceiveSelectChoice(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.SelectChoice(player_id, reader.ReadInt32());
        }

        public void ReceiveCancelSelection(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.CancelSelection(player_id);
        }

        public void ReceiveEndTurn(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if(player_id >= 0)
                gameplay.NextStep(player_id);
        }

        public void ReceiveResign(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
                gameplay.Resign(player_id);
        }

        public void ReceiveChat(ClientData iclient, DarkRiftReader reader)
        {
            int player_id = GetPlayerID(iclient.username);
            if (player_id >= 0)
            {
                MsgChat msg = new MsgChat();
                msg.msg = reader.ReadString();
                msg.player = GetPlayerID(iclient.username);
                ServerManager.Get().SendToAll(game_uid, NetworkMsg.ChatMessage, msg);
            }
        }

        public int AddPlayer(string username)
        {
            if(!players.Contains(username))
                players.Add(username);
            return GetPlayerID(username);
        }

        public int GetPlayerID(string username)
        {
            int index = 0;
            foreach (string player in players)
            {
                if (username == player)
                    return index;
                index++;
            }
            return -1;
        }

        public bool IsPlayer(string username)
        {
            int id = GetPlayerID(username);
            return id >= 0 && id < nb_players;
        }

        public int CountPlayers()
        {
            return players.Count;
        }

        public int CountConnectedPlayers()
        {
            int nb = 0;
            foreach (string user in players)
            {
                if (ServerManager.Get().IsClientConnected(user))
                    nb++;
            }
            return nb;
        }

        public bool HasJoined(string username)
        {
            return players.Contains(username);
        }

        public Game GetGameData()
        {
            return gameplay.GetGameData();
        }

        public bool HasGameEnded()
        {
            return gameplay.HasGameEnded();
        }

        public bool IsGameExpired()
        {
            return expiration > 30f;
        }

        public void CallGameReady() 
        {
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.GameReady);
        }

        public void CallGameStart()
        {
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.GameStart);

            //Create Match
            /*Game game_data = gameplay.GetGameData();
            string url = ApiManager.server_url + "/matches/add";
            AddMatchRequest req = new AddMatchRequest();
            req.players = new string[2];
            req.players[0] = game_data.players[0].username;
            req.players[1] = game_data.players[1].username;
            req.decks = new string[2];
            req.decks[0] = game_data.players[0].deck;
            req.decks[1] = game_data.players[1].deck;
            req.tid = game_data.game_uid;
            req.ranked = game_data.IsRanked();
            req.mode = Settings.GetRankModeId(game_data.settings.rank_mode);
            string json = ApiTool.ToJson(req);
            ApiManager.Get().SendPostJsonRequest(url, json, (WebResponse res) =>
            {
                Debug.Log("Match Started! " + res.success);
            });*/
        }

        public void CallGameEnd(int winner_id)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(winner_id);
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.GameEnd, writer);
            writer.Dispose();

            Game game_data = gameplay.GetGameData();
            if (game_data.turn_count <= 1)
                return; //Dont complete match if 1 full turn not played

            //End Match and give rewards
            /*Player player = game_data.GetPlayer(winner_id);
            Player fplayer = game_data.GetPlayer(game_data.first_player);
            string url = ApiManager.server_url + "/matches/complete";
            CompleteMatchRequest req = new CompleteMatchRequest();
            req.tid = game_data.game_uid;
            req.winner = player != null ? player.username : "";
            req.first = fplayer != null ? fplayer.username : "";
            req.played_cards1 = gameplay.GetPlayedCards(0).ToArray();
            req.played_cards2 = gameplay.GetPlayedCards(1).ToArray();
            string json = ApiTool.ToJson(req);
            ApiManager.Get().SendPostJsonRequest(url, json, (WebResponse res) =>
            {
                Debug.Log("Match Completed! " + res.success);
            });*/
        }

        public void CallNewRound()
        {
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.NewRound);
        }

        public void CallNewTurn(int player_id)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(player_id);
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.NewTurn, writer);
            writer.Dispose();
        }

        public void CallCardPlayed(string uid)
        {
            MsgPlayCard mdata = new MsgPlayCard();
            mdata.card_uid = uid;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.CardPlayed, mdata);
        }

        public void CallCardDiscarded(string uid)
        {
            MsgCard mdata = new MsgCard();
            mdata.card_uid = uid;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.CardDiscarded, mdata);
        }

        public void CallCardMoved(string uid)
        {
            MsgCard mdata = new MsgCard();
            mdata.card_uid = uid;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.CardMoved, mdata);
        }

        public void CallAttackTarget(string attacker, string target)
        {
            MsgAttack mdata = new MsgAttack();
            mdata.attacker_uid = attacker;
            mdata.target_uid = target;
            mdata.damage = 0;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.AttackTarget, mdata);
        }

        public void CallAttackResolve(string attacker, string target)
        {
            MsgAttack mdata = new MsgAttack();
            mdata.attacker_uid = attacker;
            mdata.target_uid = target;
            mdata.damage = 0;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.AttackResolve, mdata);
        }

        public void CallAbilityTrigger(string ability_id, string caster_uid)
        {
            MsgCastAbility mdata = new MsgCastAbility();
            mdata.ability_id = ability_id;
            mdata.caster_uid = caster_uid;
            mdata.target_uid = "";
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.AbilityTrigger, mdata);
        }

        public void CallAbilityEffect(string ability_id, string caster_uid, string target_uid)
        {
            MsgCastAbility mdata = new MsgCastAbility();
            mdata.ability_id = ability_id;
            mdata.caster_uid = caster_uid;
            mdata.target_uid = target_uid;
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.AbilityEffect, mdata);
        }

        public void CallAfterAbility(string ability_id, string caster_uid)
        {
            MsgCastAbility mdata = new MsgCastAbility();
            mdata.ability_id = ability_id;
            mdata.caster_uid = caster_uid;
            mdata.target_uid = "";
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.AbilityAfter, mdata);
        }

        public void CallRefreshAll()
        {
            MsgAllData mdata = new MsgAllData();
            mdata.game_data = GetGameData();
            ServerManager.Get().SendToAll(game_uid, NetworkMsg.RefreshAllData, mdata);
        }
    }

}
