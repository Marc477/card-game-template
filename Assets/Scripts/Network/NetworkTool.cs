﻿using System.Collections;
using UnityEngine;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

namespace CardGameTemplate
{

    public class NetworkTool
    {
        public static void WaitFor(float time, Action callback)
        {
            StartCoroutine(WaitForRun(time, callback));
        }

        public static void WaitUntil(Func<bool> condition, Action callback)
        {
            StartCoroutine(WaitUntilRun(condition, callback));
        }

        private static IEnumerator WaitForRun(float time, Action callback) { yield return new WaitForSeconds(time); callback?.Invoke(); }
        private static IEnumerator WaitUntilRun(Func<bool> condition, Action callback) { yield return new WaitUntil(condition); callback?.Invoke(); }


        public static Coroutine StartCoroutine(IEnumerator routine)
        {
            return NetworkToolMono.Inst.StartCoroutine(routine);
        }

        public static void StopCoroutine(Coroutine routine)
        {
            NetworkToolMono.Inst.StopCoroutine(routine);
        }

        public static T Deserialize<T>(byte[] bytes) where T : class
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            ms.Seek(0, SeekOrigin.Begin);
            T obj = (T)bf.Deserialize(ms);
            ms.Close();
            return obj;
        }

        public static byte[] Serialize<T>(T obj) where T : class
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            byte[] bytes = ms.ToArray();
            ms.Close();
            return bytes;
        }

        public static IPAddress GetIPAddress(string url, IPAddress default_ip = null)
        {
            IPHostEntry ip_address = Dns.GetHostEntry(url);
            IPAddress ip = ip_address != null && ip_address.AddressList.Length > 0 ? ip_address.AddressList[0] : default_ip;
            return ip;
        }
    }
}