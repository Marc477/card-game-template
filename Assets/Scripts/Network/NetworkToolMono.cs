﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGameTemplate
{

    public class NetworkToolMono : MonoBehaviour
    {
        private static NetworkToolMono _instance;

        public Coroutine StartRoutine(IEnumerator routine)
        {
            return StartCoroutine(routine);
        }

        public void StopRoutine(Coroutine routine)
        {
            StopCoroutine(routine);
        }

        public static NetworkToolMono Inst
        {
            get
            {
                if (_instance == null)
                {
                    GameObject ntool = new GameObject("NetworkTool Monobehavior");
                    _instance = ntool.AddComponent<NetworkToolMono>();
                }
                return _instance;
            }
        }
    }
}