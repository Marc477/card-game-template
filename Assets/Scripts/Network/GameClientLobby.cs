﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Client.Unity;
using DarkRift;
using DarkRift.Client;
using UnityEngine.Events;

namespace CardGameTemplate.Client
{
    public class GameClientLobby : MonoBehaviour
    {
        public UnityAction<bool> onJoinGame;
        public UnityAction<string> onPlayerConnect;
        public UnityAction<string> onPlayerDisconnect;
        public UnityAction<Game> onReceiveGameInfo;
        public UnityAction<GameSettings> onReceiveSettings;
        public UnityAction<int, string> onReceiveChat;
        public UnityAction onGameReady;

        private UnityClient client;
        private bool connecting = false;
        private bool joining = false;
        private bool in_match = false;
        private float match_timer = 0f;
        private float timer = 0f;

        private string server_url;
        private string game_uid;
        private string game_group;

        private int player_id = -1;
        private Game game_data;

        private Queue<Message> message_queue = new Queue<Message>();

        private static GameClientLobby _instance;

        void Awake()
        {
            _instance = this;
            client = GetComponent<UnityClient>();
        }

        private void Start()
        {
            client.Client.MessageReceived += OnReceiveMsg;
            client.Client.Disconnected += OnDisconnect;
        }

        void Update()
        {
            timer += Time.deltaTime;
            match_timer += Time.deltaTime;

            //Failed to connect
            if (IsConnected() && joining)
            {
                if (timer > 10f)
                {
                    timer = 0f;
                    CancelGame();
                }
            }

            //Disconnected, try reconnect
            if (!IsConnected() && !connecting && timer > 5f)
            {
                timer = 0f;
                Connect(NetworkData.Get().url, NetworkData.Get().port);
            }

            //Run messages
            if (message_queue.Count > 0)
            {
                Message msg = message_queue.Dequeue();
                if (msg != null)
                {
                    RunMessage(msg);
                    msg.Dispose();
                }
            }
        }

        public void JoinGame(string uid, string group)
        {
            Debug.Log("Start Connecting!");
            game_uid = uid;
            game_group = group;
            joining = true;
            in_match = false;
            player_id = -1;
            game_data = null;
            timer = 0f;

            //Disconnect from matchmaker and connect to game server
            Disconnect();

            //Connect to game server, which may be at different url
            Connect(NetworkData.Get().url, NetworkData.Get().port, (bool success) =>
            {
                if (success)
                {
                    SendConnectRequest(uid);
                }
                else
                {
                    CancelGame();
                }
            });
        }

        public void CancelGame()
        {
            onJoinGame?.Invoke(false);
            joining = false;
            in_match = false;
            game_uid = "";
            player_id = -1;
            game_data = null;
            timer = 0f;
            Disconnect();
        }

        public void Connect(string url, int port, UnityAction<bool> callback=null)
        {
            //Must be logged in to API to connect
            if(!ApiManager.Get().IsLoggedIn())
            {
                callback?.Invoke(false);
                return;
            }

            //Check if already connected
            if (IsConnected() && url == server_url)
            {
                callback?.Invoke(true);
                return;
            }

            //Change server if connected to another
            if (IsConnected())
                Disconnect();

            //Connect to new server
            connecting = true;
            server_url = url;
            client.ConnectInBackground(url, port, true, (System.Exception e) =>
            {
                bool connected = client.ConnectionState == ConnectionState.Connected;
                connecting = false;
                Debug.Log("Connected to server: " + connected);
                callback?.Invoke(connected);
            });
        }

        public void Disconnect()
        {
            message_queue.Clear();
            server_url = "";
            timer = 0f;
            if (client.ConnectionState == ConnectionState.Connected)
            {
                Debug.Log("Disconnecting...");
                client.Disconnect();
            }
        }

        public void SendConnectRequest(string uid)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            MsgPlayerConnect msg_connect = new MsgPlayerConnect();
            msg_connect.username = ApiManager.Get().GetUsername();
            msg_connect.token = ApiManager.Get().GetAccessToken();
            msg_connect.game_uid = uid;
            msg_connect.joined = false;
            msg_connect.observer = false;
            writer.Write(msg_connect);
            Message msg = Message.Create(NetworkMsg.ConnectPlayer, writer);
            client.Client.SendMessage(msg, SendMode.Reliable);
            msg.Dispose();
            writer.Dispose();
        }

        public void SendGameSettings(GameSettings settings)
        {
            if (in_match)
            {
                DarkRiftWriter writer = DarkRiftWriter.Create();
                writer.Write(settings);
                Message msg = Message.Create(NetworkMsg.GameplaySettings, writer);
                client.Client.SendMessage(msg, SendMode.Reliable);
                msg.Dispose();
                writer.Dispose();
            }
        }

        public void SendPlayerSettings(string username, string deck, bool ready)
        {
            if (in_match)
            {
                MsgPlayerSettings mdata = new MsgPlayerSettings();
                mdata.username = username;
                mdata.deck = deck;
                mdata.ready = ready;
                Message msg = Message.Create(NetworkMsg.PlayerSettings, mdata);
                client.Client.SendMessage(msg, SendMode.Reliable);
                msg.Dispose();
            }
        }

        public void SendChatMsg(string msg)
        {
            DarkRiftWriter writer = DarkRiftWriter.Create();
            writer.Write(msg);
            Message nmsg = Message.Create(NetworkMsg.ChatMessage, writer);
            client.Client.SendMessage(nmsg, SendMode.Reliable);
            nmsg.Dispose();
            writer.Dispose();
        }

        private void OnReceiveMsg(object sender, MessageReceivedEventArgs e)
        {
            if (!IsConnected())
                return;

            Message message = e.GetMessage();
            if(message != null)
                message_queue.Enqueue(message);
        }

        private void RunMessage(Message message)
        {
            DarkRiftReader reader = message.GetReader();
            if (message.Tag == NetworkMsg.ConnectPlayer)
            {
                MsgAfterConnected msg = reader.ReadSerializable<MsgAfterConnected>();
                ReceiveConnectPlayer(msg.success, msg.player_id, msg.game_data);
            }

            if (message.Tag == NetworkMsg.PlayerJoined)
            {
                string username = reader.ReadString();
                onPlayerConnect?.Invoke(username);
            }

            if (message.Tag == NetworkMsg.PlayerLeft)
            {
                string username = reader.ReadString();
                onPlayerDisconnect?.Invoke(username);
            }

            if (message.Tag == NetworkMsg.RefreshAllData)
            {
                MsgAllData msg = reader.ReadSerializable<MsgAllData>();
                ReceiveRefreshSettings(msg.game_data);
            }

            if (message.Tag == NetworkMsg.ChatMessage)
            {
                MsgChat msg = reader.ReadSerializable<MsgChat>();
                onReceiveChat?.Invoke(msg.player, msg.msg);
            }

            if (message.Tag == NetworkMsg.GameReady)
            {
                ReceiveGameReady();
            }
            reader.Dispose();
        }

        private void ReceiveConnectPlayer(bool success, int player_id, Game gdata)
        {
            if (IsConnected() && joining)
            {
                this.player_id = player_id;
                this.game_data = gdata;

                joining = false;
                in_match = success;

                GameClient.online_mode = true;
                GameClient.game_uid = gdata.game_uid;
                onJoinGame?.Invoke(success);
                onReceiveGameInfo?.Invoke(game_data);

                Debug.Log("Matchmaking complete: " + success);
            }
        }

        private void ReceiveRefreshSettings(Game game)
        {
            game_data = game;
            onReceiveSettings?.Invoke(game.settings);
            onReceiveGameInfo?.Invoke(game);

            if (game.current_state != GameState.Connecting)
                onGameReady?.Invoke();
        }

        private void ReceiveGameReady()
        {
            onGameReady?.Invoke();
        }

        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            Debug.Log("Disconnected from server!");
        }

        public bool IsConnected()
        {
            return client.ConnectionState == ConnectionState.Connected;
        }

        public bool IsInMatch()
        {
            return in_match;
        }

        public bool IsJoining()
        {
            return joining;
        }

        public bool IsActive()
        {
            return in_match || joining;
        }

        public string GetServerURL()
        {
            return server_url;
        }

        public string GetGameUID()
        {
            return game_uid;
        }

        public string GetGroup()
        {
            return game_group;
        }

        public int GetPlayerID()
        {
            return player_id;
        }

        public string GetPlayerUser(int player_id)
        {
            Player player = game_data.GetPlayer(player_id);
            return player.username;
        }

        public Game GetGameData()
        {
            return game_data;
        }

        public static GameClientLobby Get()
        {
            return _instance;
        }
    }

}