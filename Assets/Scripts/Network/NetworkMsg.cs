﻿
using DarkRift;

namespace CardGameTemplate
{

    public static class NetworkMsg
    {
        //Connections
        public const ushort ConnectPlayer = 1001; //Connect itself
        public const ushort PlayerJoined = 1002; //Alert others of connection
        public const ushort PlayerLeft = 1003; //Alert others of disconnection
        public const ushort Matchmaking = 1005; //Start matchmaking
        public const ushort MatchList = 1006; //Get matchmaking list
        public const ushort MatchmakingList = 1007; //Get matchmaking list
        public const ushort PlayerSettings = 1010; //After connect, send player data
        public const ushort GameplaySettings = 1015; //After connect, send gameplay settings
        public const ushort GameReady = 1020; // Game is ready to switch to next scene
        public const ushort Resign = 1100; //Send resignation
        public const ushort ChatMessage = 1500; //Player chat msg

        //Commands (client to server)
        public const ushort PlayCard = 2000;
        public const ushort Attack = 2010;
        public const ushort Move = 2015;
        public const ushort CastAbility = 2020;
        public const ushort SelectCard = 2030;
        public const ushort SelectSlot = 2032;
        public const ushort SelectChoice = 2033;
        public const ushort CancelSelect = 2035;
        public const ushort NextStep = 2040;

        //Events (server to client)
        public const ushort GameStart = 3000;
        public const ushort GameEnd = 3001;
        public const ushort NewRound = 3010;
        public const ushort NewTurn = 3011;

        public const ushort CardPlayed = 3020;
        public const ushort CardTransform = 3022;
        public const ushort CardDiscarded = 3025;
        public const ushort CardMoved = 3027;

        public const ushort AttackTarget = 3030;
        public const ushort AttackResolve = 3032;

        public const ushort AbilityTrigger = 3040;
        public const ushort AbilityEffect = 3042;
        public const ushort AbilityAfter = 3048;

        public const ushort RefreshAllData = 3100; //Slow but used in some cases until optimized
    }

    public class MsgChat : IDarkRiftSerializable
    {
        public int player;
        public string msg;

        public void Deserialize(DeserializeEvent e)
        {
            player = e.Reader.ReadInt32();
            msg = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(player);
            e.Writer.Write(msg);
        }
    }

    public class MsgChatGlobal : IDarkRiftSerializable
    {
        public string username;
        public string channel;
        public string msg;

        public void Deserialize(DeserializeEvent e)
        {
            username = e.Reader.ReadString();
            channel = e.Reader.ReadString();
            msg = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(username);
            e.Writer.Write(channel);
            e.Writer.Write(msg);
        }
    }

    public class MsgPlayerConnect : IDarkRiftSerializable­
    {
        public string username;
        public string token;
        public string game_uid;
        public bool joined; //Is the player joining the game scene? Or still in settings
        public bool observer; //join as observer

        public void Deserialize(DeserializeEvent e)
        {
            username = e.Reader.ReadString();
            token = e.Reader.ReadString();
            game_uid = e.Reader.ReadString();
            joined = e.Reader.ReadBoolean();
            observer = e.Reader.ReadBoolean();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(username);
            e.Writer.Write(token);
            e.Writer.Write(game_uid);
            e.Writer.Write(joined);
            e.Writer.Write(observer);
        }
    }

    public class MsgAfterConnected : IDarkRiftSerializable
    {
        public bool success;
        public int player_id;
        public Game game_data;

        public void Deserialize(DeserializeEvent e)
        {
            success = e.Reader.ReadBoolean();
            player_id = e.Reader.ReadInt32();
            if (e.Reader.Position < e.Reader.Length)
                game_data = NetworkTool.Deserialize<Game>(e.Reader.ReadBytes());
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(success);
            e.Writer.Write(player_id);
            if (game_data != null)
                e.Writer.Write(NetworkTool.Serialize(game_data));
        }
    }

    public class MsgMatchmaking : IDarkRiftSerializable
    {
        public string username;
        public string group;
        public int rank;
        public bool refresh;
        public float time;

        public void Deserialize(DeserializeEvent e)
        {
            username = e.Reader.ReadString();
            group = e.Reader.ReadString();
            rank = e.Reader.ReadInt32();
            refresh = e.Reader.ReadBoolean();
            time = e.Reader.ReadSingle();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(username);
            e.Writer.Write(group);
            e.Writer.Write(rank);
            e.Writer.Write(refresh);
            e.Writer.Write(time);
        }
    }

    public class MsgAfterMatchmaking : IDarkRiftSerializable
    {
        public bool success;
        public string server_url;
        public string game_uid;

        public void Deserialize(DeserializeEvent e)
        {
            success = e.Reader.ReadBoolean();
            server_url = e.Reader.ReadString();
            game_uid = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(success);
            e.Writer.Write(server_url);
            e.Writer.Write(game_uid);
        }
    }

    public class MsgMatchmakingList : IDarkRiftSerializable
    {
        public string username;

        public void Deserialize(DeserializeEvent e)
        {
            username = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(username);
        }
    }

    [System.Serializable]
    public struct MatchmakingListItem
    {
        public string group;
        public string username;
    }

    [System.Serializable]
    public class MatchListItem
    {
        public string group;
        public string username;
        public string opponent;
        public string game_uid;
        public string game_url;
    }


    public class MatchmakingList : IDarkRiftSerializable
    {
        public MatchmakingListItem[] items;

        public void Deserialize(DeserializeEvent e)
        {
            items = NetworkTool.Deserialize<MatchmakingListItem[]>(e.Reader.ReadBytes());
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(NetworkTool.Serialize(items));
        }
    }

    public class MatchList : IDarkRiftSerializable
    {
        public MatchListItem[] items;

        public void Deserialize(DeserializeEvent e)
        {
            items = NetworkTool.Deserialize<MatchListItem[]>(e.Reader.ReadBytes());
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(NetworkTool.Serialize(items));
        }
    }

    public class MsgAllData : IDarkRiftSerializable
    {
        public Game game_data;

        public void Deserialize(DeserializeEvent e)
        {
            game_data = NetworkTool.Deserialize<Game>(e.Reader.ReadBytes());
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(NetworkTool.Serialize(game_data));
        }
    }

    public class MsgPlayerSettings : IDarkRiftSerializable
    {
        public string username;
        public string deck;
        public bool ready;

        public void Deserialize(DeserializeEvent e)
        {
            username = e.Reader.ReadString();
            deck = e.Reader.ReadString();
            ready = e.Reader.ReadBoolean();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(username);
            e.Writer.Write(deck);
            e.Writer.Write(ready);
        }
    }

    public class MsgPlayCard : IDarkRiftSerializable
    {
        public string card_uid;
        public int slot_x;
        public int slot_p;

        public void Deserialize(DeserializeEvent e)
        {
            card_uid = e.Reader.ReadString();
            slot_x = e.Reader.ReadInt32();
            slot_p = e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(card_uid);
            e.Writer.Write(slot_x);
            e.Writer.Write(slot_p);
        }
    }

    public class MsgCard : IDarkRiftSerializable
    {
        public string card_uid;

        public void Deserialize(DeserializeEvent e)
        {
            card_uid = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(card_uid);
        }
    }

    public class MsgSlot : IDarkRiftSerializable
    {
        public int slot_x;
        public int slot_p;

        public void Deserialize(DeserializeEvent e)
        {
            slot_x = e.Reader.ReadInt32();
            slot_p = e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(slot_x);
            e.Writer.Write(slot_p);
        }
    }

    public class MsgAttack : IDarkRiftSerializable
    {
        public string attacker_uid;
        public string target_uid;
        public int damage;

        public void Deserialize(DeserializeEvent e)
        {
            attacker_uid = e.Reader.ReadString();
            target_uid = e.Reader.ReadString();
            damage = e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(attacker_uid);
            e.Writer.Write(target_uid);
            e.Writer.Write(damage);
        }
    }

    public class MsgCastAbility : IDarkRiftSerializable
    {
        public string ability_id;
        public string caster_uid;
        public string target_uid;

        public void Deserialize(DeserializeEvent e)
        {
            ability_id = e.Reader.ReadString();
            caster_uid = e.Reader.ReadString();
            target_uid = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(ability_id);
            e.Writer.Write(caster_uid);
            e.Writer.Write(target_uid);
        }
    }

    public class MsgTriggerItem : IDarkRiftSerializable
    {
        public int type;
        public int slot_x;
        public int slot_p;

        public void Deserialize(DeserializeEvent e)
        {
            type = e.Reader.ReadInt32();
            slot_x = e.Reader.ReadInt32();
            slot_p = e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(type);
            e.Writer.Write(slot_x);
            e.Writer.Write(slot_p);
        }
    }
}