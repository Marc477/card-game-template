﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CardGameTemplate.Client
{

    public class GameClient : MonoBehaviour
    {
        public static bool online_mode = false;
        public static string game_uid = "ABC";

        public UnityAction onConnected;
        public UnityAction onGameStart;
        public UnityAction<int> onGameEnd;
        public UnityAction<int> onNewTurn;
        public UnityAction<string> onCardPlayed;
        public UnityAction<string> onCardDiscarded;
        public UnityAction<string> onCardMove;

        public UnityAction<string, string> onAbilityTrigger;
        public UnityAction<string, string, string> onAbilityEffect;
        public UnityAction<string, string> onAbilityAfter;

        public UnityAction<string, string> onAttackTarget;
        public UnityAction<string, string> onAttackAfter;

        public UnityAction<string> onAddItem;

        public UnityAction<int, string> onChatMsg;
        public UnityAction onRefreshAllData;

        private GameClientLocal client_local;
        private GameClientOnline client_online;

        private bool observe_mode = false;
        private int observe_player_id = 0;

        private static GameClient _instance;

        void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            if (!online_mode)
            {
                client_local = new GameClientLocal(this);
                client_local.Connect();
            }
            else
            {
                client_online = new GameClientOnline(this);
                client_online.Connect();
            }

            ApiManager api = ApiManager.Get();
            Debug.Log("Api Connection: " + api.IsLoggedIn());
        }

        void Update()
        {
            if (client_local == null && client_online == null)
                return;

            if (!online_mode)
                client_local.Update();
            else
                client_online.Update();
        }

        //--------------------------

        public void SendPlayerSettings(string username, string deck)
        {
            //Deck will be loaded from the server
            if (online_mode)
                client_online.SendPlayerSetting(username, deck);
            else
                client_local.SendPlayerSettings(client_local.player_id, username, deck);
        }

        public void SendPlayerSettings(int player_id, string name, DeckData deck)
        {
            //Deck is sent directly
            if (!online_mode)
            {
                List<CardData> cards = new List<CardData>();
                cards.AddRange(deck.cards);
                client_local.SendPlayerSettings(player_id, name, deck.id, cards.ToArray());
            }
        }

        public void SendAIPlayerSettings(int player_id, string name, DeckData deck, int ai_level)
        {
            //Deck is sent directly
            if (!online_mode)
            {
                List<CardData> cards = new List<CardData>();
                cards.AddRange(deck.cards);
                client_local.SendAIPlayerSettings(player_id, name, deck.id, cards.ToArray(), ai_level);
            }
        }

        public void SendGameplaySettings(GameSettings settings)
        {
            if (!online_mode)
                client_local.SendGameplaySettings(settings);
            else
                client_online.SendGameplaySettings(settings);
        }

        /*public void SelectDraft(Card card)
        {
            if (!online_mode)
                client_local.SelectDraft(card);
            else
                client_online.SelectDraft(card);
        }*/

        public void PlayCard(Card card, Slot slot)
        {
            if (!online_mode)
                client_local.PlayCard(card, slot);
            else
                client_online.PlayCard(card, slot);
        }

        public void AttackTarget(Card card, Card target)
        {
            if (!online_mode)
                client_local.AttackTarget(card, target);
            else
                client_online.AttackTarget(card, target);
        }

        public void Move(Card card, Slot slot)
        {
            if (!online_mode)
                client_local.Move(card, slot);
            else
                client_online.Move(card, slot);
        }

        public void CastCardAbility(Card card, AbilityData ability)
        {
            if (!online_mode)
                client_local.CastCardAbility(card, ability);
            else
                client_online.CastCardAbility(card, ability);
        }

        public void SelectCard(Card card)
        {
            if (!online_mode)
                client_local.SelectCard(card);
            else
                client_online.SelectCard(card);
        }

        public void SelectSlot(Slot slot)
        {
            if (!online_mode)
                client_local.SelectSlot(slot);
            else
                client_online.SelectSlot(slot);
        }

        public void SelectChoice(int choice)
        {
            if (!online_mode)
                client_local.SelectChoice(choice);
            else
                client_online.SelectChoice(choice);
        }

        public void CancelSelection()
        {
            if (!online_mode)
                client_local.CancelSelection();
            else
                client_online.CancelSelection();
        }

        public void NextStep()
        {
            if (!online_mode)
                client_local.NextStep();
            else
                client_online.NextStep();
        }

        public void Resign()
        {
            if (online_mode)
                client_online.Resign();
            else
                client_local.Resign();
        }

        public void SendChatMsg(string msg)
        {
            if (online_mode)
                client_online.SendChatMsg(msg);
        }

        public void SetObserveMode(int player_id)
        {
            observe_mode = true;
            observe_player_id = player_id;
        }

        public void SetObserverMode(string username)
        {
            Game data = GetGameData();
            foreach (Player player in data.players)
            {
                if (player.username == username)
                    SetObserveMode(player.player_id);
            }
        }

        //--------------------------

        public bool IsConnected()
        {
            if (!online_mode && client_local != null)
                return client_local.IsConnected();
            if(online_mode && client_online != null)
                return client_online.IsConnected();
            return false;
        }

        public bool WasConnected()
        {
            if (!online_mode && client_local != null)
                return client_local.IsConnected();
            if (online_mode && client_online != null)
                return client_online.WasConnected();
            return false;
        }

        public Player GetPlayer()
        {
            Game gdata = GetGameData();
            return gdata.GetPlayer(GetPlayerID());
        }

        public Player GetOpponentPlayer()
        {
            Game gdata = GetGameData();
            return gdata.GetPlayer(GetOpponentPlayerID());
        }

        public int GetPlayerID()
        {
            if (observe_mode)
                return observe_player_id;
            return online_mode ? client_online.GetPlayerID() : client_local.GetPlayerID();
            //return client_local.GetPlayerID();
        }

        public int GetOpponentPlayerID()
        {
            return GetPlayerID() == 0 ? 1 : 0;
        }

        public bool IsYourTurn()
        {
            int player_id = GetPlayerID();
            Game game_data = GetGameData();

            if (!IsConnected())
                return false;
            return player_id == game_data.current_player;
        }

        public bool IsObserveMode()
        {
            return observe_mode;
        }

        public Game GetGameData()
        {
            return online_mode ? client_online.GetGameData() : client_local.GetGameData();
            //return client_local.GetGameData();
        }

        public static GameClient Get()
        {
            return _instance;
        }

        public void CallGameStart(){ onGameStart?.Invoke(); }
        public void CallGameEnd(int winner_id){ onGameEnd?.Invoke(winner_id); }
        public void CallNewTurn(int p){ onNewTurn?.Invoke(p); }
        public void CallCardPlayed(string uid) { onCardPlayed?.Invoke(uid); }
        public void CallCardDiscarded(string uid) { onCardDiscarded?.Invoke(uid); }
        public void CallCardMove(string uid) { onCardMove?.Invoke(uid); }

        public void CallAttackTarget(string attacker, string target){ onAttackTarget?.Invoke(attacker, target);}
        public void CallAttackResolve(string attacker, string target){ onAttackAfter?.Invoke(attacker, target);}
        
        public void CallAbilityTrigger(string ability_id, string caster_uid){ onAbilityTrigger?.Invoke(ability_id, caster_uid);}
        public void CallAbilityEffect(string ability_id, string caster_uid, string target_uid) { onAbilityEffect?.Invoke(ability_id, caster_uid, target_uid);}
        public void CallAbilityAfter(string ability_id, string caster_uid){ onAbilityAfter?.Invoke(ability_id, caster_uid);}

        public void CallChatMsg(int player_id, string msg) { onChatMsg?.Invoke(player_id, msg); }
        public void CallRefreshAll(){ onRefreshAllData?.Invoke();}

    }

}