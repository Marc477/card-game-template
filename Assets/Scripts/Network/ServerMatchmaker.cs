﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift;
using DarkRift.Server;
using DarkRift.Server.Unity;

namespace CardGameTemplate.Server
{
    public class MatchPlayerData
    {
        public string username;
        public string group;
        public int rank;
    }

    public class MatchData
    {
        public string group;
        public string opponent;
        public string game_uid;
        public string server_url;
        public bool ended = false;

        public MatchData(string grp, string oplayer, string uid, string url) { group = grp; opponent = oplayer; game_uid = uid; server_url = url; }
    }

    public class ServerMatchmaker : MonoBehaviour
    {
        [Header("Matchmaker")]
        public string[] servers;

        private Dictionary<ushort, ClientData> client_list = new Dictionary<ushort, ClientData>();  //List of clients
        private Dictionary<string, MatchPlayerData> matchmaking_players = new Dictionary<string, MatchPlayerData>(); //Get deleted every 20 sec
        private float matchmake_timer = 0f;
        //private float player_list_timer = 0f;

        private Dictionary<string, MatchData> previous_matches = new Dictionary<string, MatchData>(); //username -> MatchData
        private Dictionary<string, string> matched_players = new Dictionary<string, string>(); //username -> game_uid
        private Dictionary<string, string> game_servers = new Dictionary<string, string>(); //game_uid -> server_url
        
        private XmlUnityServer server;

        private static ServerMatchmaker _instance;

        void Awake()
        {
            _instance = this;
            server = GetComponent<XmlUnityServer>();
            Application.runInBackground = true;
        }

        void Start()
        {
            server.Server.ClientManager.ClientConnected += ClientConnected;
            server.Server.ClientManager.ClientDisconnected += ClientDisconnected;

            Debug.Log("Matchmaker listening on " + server.Server.ClientManager.Port);
        }

        void Update()
        {
            //Matchmaking
            matchmake_timer += Time.deltaTime;
            if (matchmake_timer > 20f)
            {
                matchmake_timer = 0f;
                matchmaking_players.Clear(); //Delete and restart, to make sure you only keep recent players
            }

            /*player_list_timer += Time.deltaTime;
            if (player_list_timer > (12f * 3600f))
            {
                player_list_timer = 0f;
                previous_matches.Clear(); //Delete and restart, so the list dont get too big
            }*/
        }

        private void ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            ClientData iclient = new ClientData(e.Client.ID);
            client_list[e.Client.ID] = iclient;
            e.Client.MessageReceived += ReceiveMessage;
        }

        private void ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            ClientData iclient = GetClient(e.Client.ID);
            client_list.Remove(e.Client.ID);
            e.Client.MessageReceived -= ReceiveMessage;
        }

        private void ReceiveMessage(object sender, MessageReceivedEventArgs e)
        {
            ushort client_id = e.Client.ID;
            Message message = e.GetMessage();
            ClientData iclient = GetClient(client_id);
            if (iclient != null && message != null)
            {
                //Connection msg
                if (message.Tag == NetworkMsg.Matchmaking)
                {
                    ReceiveMatchmaking(iclient, message);
                    message.Dispose();
                }
                else if (message.Tag == NetworkMsg.MatchmakingList)
                {
                    ReceiveMatchmakingList(iclient, message);
                    message.Dispose();
                }
                else if (message.Tag == NetworkMsg.MatchList)
                {
                    ReceiveMatchList(iclient, message);
                    message.Dispose();
                }
                else
                {
                    message.Dispose();
                }
            }
        }

        private void ReceiveMatchmaking(ClientData iclient, Message message)
        {
            DarkRiftReader reader = message.GetReader();
            MsgMatchmaking msg = reader.ReadSerializable<MsgMatchmaking>();
            reader.Dispose();

            if (string.IsNullOrWhiteSpace(msg.username))
                return;

            string user = msg.username;
            bool is_refresh = msg.refresh;

            iclient.username = msg.username;

            //Restart matching
            if (!is_refresh)
                matched_players.Remove(user);

            //Check if already matched
            if (matched_players.ContainsKey(user))
            {
                string game_uid = matched_players[user];
                if (game_servers.ContainsKey(game_uid))
                {
                    string url = game_servers[game_uid];
                    SendMatchmakingResponse(iclient, true, url, game_uid); //Was already matched, return saved result!
                }

                //Clear after sending
                matched_players.Remove(user);
                game_servers.Remove(game_uid);
                return;
            }

            //Data
            MatchPlayerData pdata = new MatchPlayerData();
            pdata.username = user;
            pdata.group = msg.group;
            pdata.rank = msg.rank;

            //Add to matchking players
            if (!string.IsNullOrEmpty(user) && !matchmaking_players.ContainsKey(user))
                matchmaking_players.Add(user, pdata);

            float wait_max = 20f;
            int variance_max = 2000;

            bool friendly = msg.group.StartsWith("u_");
            float wait_timer = msg.time;
            float wait_value = Mathf.Clamp01(wait_timer / wait_max);
            int elo_variance = Mathf.RoundToInt(wait_value * variance_max);

            string other_user = "";
            foreach (KeyValuePair<string, MatchPlayerData> opair in matchmaking_players)
            {
                string auser = opair.Key;
                MatchPlayerData adata = opair.Value;
                int diff = Mathf.Abs(adata.rank - msg.rank);
                bool same_group = adata.group == msg.group;
                bool valid_matching = !previous_matches.ContainsKey(user) || previous_matches[user].opponent != auser || msg.time > 5f;
                bool valid_elo = friendly || diff < elo_variance;
                if (auser != user && valid_elo && same_group && valid_matching)
                {
                    other_user = auser;
                    break;
                }
            }

            //Match success
            if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(other_user))
            {
                string prefix = msg.group.Length >= 2 ? msg.group.Substring(0, 2) : "";
                string game_code = prefix + GameTool.GenerateUID(12, 15);
                string game_url = NetworkData.Get().url;
                if(servers.Length > 0)
                    game_url = servers[UnityEngine.Random.Range(0, servers.Length)];

                matchmaking_players.Remove(user);
                matchmaking_players.Remove(other_user);
                matched_players[user] = game_code;
                matched_players[other_user] = game_code;
                previous_matches[user] = new MatchData(msg.group, other_user, game_code, game_url);
                previous_matches[other_user] = new MatchData(msg.group, user, game_code, game_url);
                game_servers[game_code] = game_url;
                SendMatchmakingResponse(iclient, true, game_url, game_code); //Just matched to new player!
                return;
            }
        }

        private void ReceiveMatchmakingList(ClientData iclient, Message message)
        {
            DarkRiftReader reader = message.GetReader();
            MsgMatchmakingList msg = reader.ReadSerializable<MsgMatchmakingList>();
            reader.Dispose();

            List<MatchmakingListItem> items = new List<MatchmakingListItem>();

            foreach (KeyValuePair<string, MatchPlayerData> pair in matchmaking_players)
            {
                if (string.IsNullOrEmpty(msg.username) || pair.Key == msg.username)
                {
                    MatchPlayerData pdata = pair.Value;
                    MatchmakingListItem item = new MatchmakingListItem();
                    item.group = pdata.group;
                    item.username = pdata.username;
                    items.Add(item);
                }
            }

            MatchmakingList msg_list = new MatchmakingList();
            msg_list.items = items.ToArray();
            SendToClient(iclient.client_id, NetworkMsg.MatchmakingList, msg_list);
        }

        private void ReceiveMatchList(ClientData iclient, Message message)
        {
            DarkRiftReader reader = message.GetReader();
            MsgMatchmakingList msg = reader.ReadSerializable<MsgMatchmakingList>();
            reader.Dispose();

            List<MatchListItem> items = new List<MatchListItem>();

            foreach (KeyValuePair<string, MatchData> pair in previous_matches)
            {
                if (!pair.Value.ended)
                {
                    if (string.IsNullOrEmpty(msg.username) || pair.Key == msg.username)
                    {
                        MatchData pdata = pair.Value;
                        MatchListItem item = new MatchListItem();
                        item.group = pdata.opponent;
                        item.username = pair.Key;
                        item.opponent = pair.Key;
                        item.game_uid = pdata.game_uid;
                        item.game_url = pdata.server_url;
                        items.Add(item);
                    }
                }
            }

            MatchList msg_list = new MatchList();
            msg_list.items = items.ToArray();
            SendToClient(iclient.client_id, NetworkMsg.MatchList, msg_list);
        }

        private void SendMatchmakingResponse(ClientData iclient, bool success, string server_url, string game_uid)
        {
            MsgAfterMatchmaking msg_match = new MsgAfterMatchmaking();
            msg_match.success = success;
            msg_match.game_uid = game_uid;
            msg_match.server_url = server_url;
            SendToClient(iclient.client_id, NetworkMsg.Matchmaking, msg_match);
        }

        public void EndMatch(string uid)
        {
            foreach (KeyValuePair<string, MatchData> pair in previous_matches)
            {
                if (pair.Value.game_uid == uid)
                    pair.Value.ended = true;
            }
        }

        public void SendToClient(ushort client_id, ushort tag, IDarkRiftSerializable data)
        {
            Message msg = Message.Create<IDarkRiftSerializable>(tag, data);
            SendToClient(client_id, msg);
            msg.Dispose();
        }

        public void SendToClient(ushort client_id, Message msg)
        {
            IClient client = server.Server.ClientManager.GetClient(client_id);
            if (client != null)
                client.SendMessage(msg, SendMode.Reliable);
        }

        public void SendToAll(ushort tag, IDarkRiftSerializable data)
        {
            Message msg = Message.Create<IDarkRiftSerializable>(tag, data);
            SendToAll(msg);
            msg.Dispose();
        }

        public void SendToAll(Message msg)
        {
            foreach (IClient client in server.Server.ClientManager.GetAllClients())
            {
                ClientData iclient = GetClient(client.ID);
                if (iclient != null)
                {
                    client.SendMessage(msg, SendMode.Reliable);
                }
            }
        }

        public ClientData GetClient(ushort client_id)
        {
            if (client_list.ContainsKey(client_id))
                return client_list[client_id];
            return null;
        }

        public ClientData GetClientByUser(string username)
        {
            foreach (KeyValuePair<ushort, ClientData> pair in client_list)
            {
                if (pair.Value.username == username)
                    return pair.Value;
            }
            return null;
        }

        public static ServerMatchmaker Get()
        {
            return _instance;
        }
    }
}
